Version 0.69:
- changed minSdkVersion to 19 (android 4.4).
- fixed app crash when sharing files on devices running android versions 7.0 or later