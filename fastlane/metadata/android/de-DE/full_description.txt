<p>Das OK Lab Stuttgart hat ein Projekt, um selbst Feinstaub-Sensoren zu bauen. Diese Feinstaub-Daten können da an die Datenbank von https://luftdaten.info gesendet werden. Diese App zeigt Daten aus diesem Netzwerk. Es ist nicht nötig, einen Feinstaub-Sensor selbst zu betreiben, um diese App zu nutzen.</p>

<p>Aktuell unterstützte Sensortypen sind: SDS011, SDS021, HPM, DHT11, DHT22, HTU21D, BME280 (nur Temperatur & Luftfeuchtigkeit)</p>
<p>Neben der Hauptanwendung bietet diese App auch drei Widgets:</p>
<ul>
<li>Ein kleines Widget, das ein Minimum an Platz verbraucht und die aktuellen Daten als Text anzeigt.</li>
<li>Ein großes Widget, das die aktuellen Daten anzeigt und Diagramme zeichnet für Feinstaub, Temperatur und Luftfeuchtigkeit.</li>
<li>Ein großes Widget, das die Feinstaub-Werte der Sensoren in der Nähe anzeigt.</li>
</ul>
<p>Eine kurze Anleitung und die Datenschutzerklärung findet sich hier: https://codeberg.org/Starfish/Feinstaubwidget/wiki</p>