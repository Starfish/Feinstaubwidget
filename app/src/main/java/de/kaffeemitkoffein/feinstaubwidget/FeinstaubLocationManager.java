/**

 This file is part of FeinstaubWidget.

 Copyright (c) 2018, 2019, 2020, 2021, 2022, 2023, 2024 Pawel Dube

 FeinstaubWidget is free software: you can redistribute it and/or modify it
 under the terms of the GNU General Public License as published by the
 Free Software Foundation, either version 3 of the License, or (at
 your option) any later version.

 FeinstaubWidget is distributed in the hope that it will be useful, but
 WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with FeinstaubWidget. If not, see <http://www.gnu.org/licenses/>.

 */

package de.kaffeemitkoffein.feinstaubwidget;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.PendingIntent;
import android.content.Context;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationManager;
import android.os.Looper;

import java.util.Calendar;
import java.util.concurrent.Executor;
import java.util.function.Consumer;

/**
 * This class is used to access location information throughout the app.
 *
 * The methods used to retrieve a location depend on the settings. This class uses
 * the providers enabled in the settings.
 *
 * The methods in this class are similar to the ones presented by the
 * LocationManager. Use them accordingly.
 *
 */

public class FeinstaubLocationManager {

    public static final String KEY_LOCATION_CHANGED = LocationManager.KEY_LOCATION_CHANGED;

    private Context context;
    private LocationManager locationManager;

    /**
     * Public constructor.
     *
     * @param context
     */

    public FeinstaubLocationManager(Context context){
        this.context =  context;
        locationManager = (LocationManager) context.getSystemService(Context.LOCATION_SERVICE);
    }

    /**
     * Checks if the app was granted the location permission.
     *
     * @return
     */

    public Boolean hasLocationPermission(){
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.M) {
            if (context.checkSelfPermission(Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED){
                return true;
            } else {
                return false;
            }
        } else {
            return true;
        }
    }

    /**
     * Gets the last known location from the specified provider.
     *
     * @param locationProvider
     * @return
     */

    @SuppressLint("MissingPermission")
    public Location getLastKnownLocation(String locationProvider){
        if (hasLocationPermission(context)){
            return locationManager.getLastKnownLocation(locationProvider);
        } else return null;
    }

    /**
     * Gets the last known location.
     *
     * @param active specifies if only the passive provider should be used.
     * @param feinstaubPreferences an instance of the preferences to use, may be null.
     * @return
     */

    public Location getLastKnownLocation(Boolean active, FeinstaubPreferences feinstaubPreferences){
        if (active){
            return getLastKnownLocation(feinstaubPreferences);
        } else {
            return getLastKnownLocation(LocationManager.PASSIVE_PROVIDER);
        }
    }

    /**
     * Gets the last known location.
     *
     * @return
     */

    public Location getLastKnownLocation(){
    FeinstaubPreferences feinstaubPreferences = new FeinstaubPreferences(context);
    return getLastKnownLocation(feinstaubPreferences);
    }

    /**
     * Gets the last known location.
     *
     * The last known location is queried from the best available provider, respecting
     * the user's choice in the settings.
     *
     * The preferred providers are (in order of use, if available and desired):
     * 1) satellites ("GPS")
     * 2) network
     * 3) passive provider
     * 4) 1st local sensor location
     *
     * @param feinstaubPreferences an instance of the preferences to use, may be null.
     * @return
     */

    public Location getLastKnownLocation(FeinstaubPreferences feinstaubPreferences){
        if (feinstaubPreferences == null){
            feinstaubPreferences = new FeinstaubPreferences(context);
        }
        Location location = null;
        if (hasLocationPermission()){
            if (feinstaubPreferences.use_location_provider_GPS){
                if (locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER))
                    location = getLastKnownLocation(LocationManager.GPS_PROVIDER);
            }
            if ((location == null) && (feinstaubPreferences.use_location_provider_NETWORK)){
                if (locationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER))
                    location = getLastKnownLocation(LocationManager.NETWORK_PROVIDER);
            }
            if ((location == null) && (feinstaubPreferences.use_location_provider_PASSIVE)){
                if (locationManager.isProviderEnabled(LocationManager.PASSIVE_PROVIDER))
                    location = getLastKnownLocation(LocationManager.PASSIVE_PROVIDER);
            }
        }
        if ((location == null) && (feinstaubPreferences.use_local_sensor_location_as_fallback)){
            SensorlocationHandler sensorlocationHandler = new SensorlocationHandler(context);
            location = sensorlocationHandler.getLocationFromSensor(feinstaubPreferences.sensor1_number);
        }
        return location;
    }

    /**
     * Checks if at least one provider is available that meets the criteria set up
     * in the preferences.
     *
     * @param feinstaubPreferences  instance of FeinstaubPreferences, if available. May be null.
     * @return
     */

    public Boolean isProviderEnabled(FeinstaubPreferences feinstaubPreferences){
        if (feinstaubPreferences == null) {
            feinstaubPreferences = new FeinstaubPreferences(context);
        }
        /*
         * Checks if the GPS provider is available.
         */
        Boolean enabled = false;
        if (feinstaubPreferences.use_location_provider_GPS) {
            if (locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER))
                enabled = true;
        }
        /*
         * Checks if the network provider is available.
         */
        if (!enabled){
            if (feinstaubPreferences.use_location_provider_NETWORK){
                if (locationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER)){
                    enabled = true;
                }
            }
        }
        if (!enabled){
            /*
             * Checks if 1st sensor should be used as a location provider.
             * Further checks if this works at all.
             */
            if ((feinstaubPreferences.use_local_sensor_location_as_fallback) && (feinstaubPreferences.lastlocationlistupdatetime != 0)){
                SensorlocationHandler sensorlocationHandler = new SensorlocationHandler(context);
                Location location = sensorlocationHandler.getLocationFromSensor(feinstaubPreferences.sensor1_number);
                if (location != null){
                    enabled = true;
                }
            }
        }
        return enabled;
    }

    /**
     * Convenience method to call isProviderEnabled without passing a FinestaubPreferences instance. See
     * above.
     *
     * @return
     */

    public Boolean isProviderEnabled(){
        return isProviderEnabled(null);
    }

    /**
     * Request a single location update passing a pendingIntent that is called once the location fix
     * was done successfully.
     *
     * Mainly designed to work in widgets.
     *
     * It will ignore any conditions that lead to failure to obtain a location fix, especially
     * missing location permission.
     *
     * @param pendingIntent
     * @param feinstaubPreferences  instance of FeinstaubPreferences, if available. May be null.
     */

    @SuppressLint("MissingPermission")
    public void requestSingleUpdate(PendingIntent pendingIntent, FeinstaubPreferences feinstaubPreferences){
        if (feinstaubPreferences == null) {
            feinstaubPreferences = new FeinstaubPreferences(context);
        }
        if ((feinstaubPreferences.use_location_provider_GPS) && (hasLocationPermission(context))){
            try {
                locationManager.requestSingleUpdate(LocationManager.GPS_PROVIDER,pendingIntent);
            } catch (Exception e){
                // failing to register this pendingIntent is ignored.
            }
        } else {
            if ((feinstaubPreferences.use_location_provider_NETWORK) && (hasLocationPermission(context))){
                try {
                    locationManager.requestSingleUpdate(LocationManager.NETWORK_PROVIDER,pendingIntent);
                } catch (Exception e){
                    // failing to register this pendingIntent is ignored.
                }
            }
        }
    }

    /**
     * Request a single location update passing a listener that is notified once the location fix
     * was done successfully.
     *
     * Mainly designed to work in the application.
     *
     * It will ignore any conditions that lead to failure to obtain a location fix, especially
     * missing location permission.
     *
     * @param feinstaubLocationListener
     * @param executor
     * @param feinstaubPreferences  instance of FeinstaubPreferences, if available. May be null.
     */

    @SuppressLint("MissingPermission")
    public void requestSingleUpdate(final FeinstaubLocationListener feinstaubLocationListener, Executor executor, FeinstaubPreferences feinstaubPreferences){
        if (feinstaubPreferences == null) {
            feinstaubPreferences = new FeinstaubPreferences(context);
        }
        if (hasLocationPermission(context)){
            if (android.os.Build.VERSION.SDK_INT<=30){
                if (feinstaubPreferences.use_location_provider_GPS){
                    locationManager.requestSingleUpdate(LocationManager.GPS_PROVIDER,feinstaubLocationListener,null);
                } else {
                    if (feinstaubPreferences.use_location_provider_NETWORK){
                        locationManager.requestSingleUpdate(LocationManager.NETWORK_PROVIDER,feinstaubLocationListener,null);
                    }
                }
            } else {
                if ((feinstaubPreferences.use_location_provider_GPS) || (feinstaubPreferences.use_location_provider_NETWORK)){
                    String provider = LocationManager.NETWORK_PROVIDER;
                    if (feinstaubPreferences.use_location_provider_GPS){
                        provider=LocationManager.GPS_PROVIDER;
                    }
                    locationManager.getCurrentLocation(provider, null, executor, new Consumer<Location>() {
                        @Override
                        public void accept(Location location) {
                            feinstaubLocationListener.onLocationChanged(location);
                        }
                    });
                }
            }
        }
    }

    @SuppressLint("MissingPermission")
    public void requestLocationUpdates(PendingIntent pendingIntent, FeinstaubPreferences feinstaubPreferences){
        if (feinstaubPreferences == null) {
            feinstaubPreferences = new FeinstaubPreferences(context);
        }
        String bestProvider = null;
        if (feinstaubPreferences.use_location_provider_PASSIVE)
            bestProvider = LocationManager.PASSIVE_PROVIDER;
        if (feinstaubPreferences.use_location_provider_NETWORK)
            bestProvider = LocationManager.NETWORK_PROVIDER;
        if (feinstaubPreferences.use_location_provider_GPS)
            bestProvider = LocationManager.GPS_PROVIDER;
        if ((bestProvider != null) && (hasLocationPermission(context))){
            try {
                locationManager.requestLocationUpdates(bestProvider, feinstaubPreferences.getLocationUpdateIntervalInMillis(), feinstaubPreferences.location_update_distance, pendingIntent);
            } catch (Exception e){
                // failing to register this pendingIntent is ignored.
            }
        }
    }

    @SuppressLint("MissingPermission")
    public void requestLocationUpdates(FeinstaubLocationListener feinstaubLocationListener, FeinstaubPreferences feinstaubPreferences){
        if (feinstaubPreferences == null) {
            feinstaubPreferences = new FeinstaubPreferences(context);
        }
        String bestProvider = null;
        if (feinstaubPreferences.use_location_provider_PASSIVE)
            bestProvider = LocationManager.PASSIVE_PROVIDER;
        if (feinstaubPreferences.use_location_provider_NETWORK)
            bestProvider = LocationManager.NETWORK_PROVIDER;
        if (feinstaubPreferences.use_location_provider_GPS)
            bestProvider = LocationManager.GPS_PROVIDER;
        if ((bestProvider != null) && (hasLocationPermission(context))){
            try {
                locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, feinstaubPreferences.getLocationUpdateIntervalInMillis(), feinstaubPreferences.location_update_distance, feinstaubLocationListener);
            } catch (Exception e){
                // failing to register this listener is ignored.
            }
        }
    }

    /**
     * Based on the location l, this method returns a char indicating the source of the location:
     * "G" = GPS / satellites
     * "N" = network
     * "P" = passive provider
     * "S" = location of 1st local sensor
     *
     * Returns an empty string when no provider was set in the location.
     *
     * @param l Location
     * @return char
     */

    public String getLocationSourceChar(Location l){
        String location_source = "";
        String provider = l.getProvider();
        if (provider != null){
            if (l.getProvider().equals(LocationManager.GPS_PROVIDER)){
                location_source="G";
            }
            if (l.getProvider().equals(LocationManager.NETWORK_PROVIDER)){
                location_source="N";
            }
            if (l.getProvider().equals(LocationManager.PASSIVE_PROVIDER)){
                location_source="P";
            }
            if (l.getProvider().equals(LuftDatenContentProvider.SensorLocationsDatabaseHelper.LOCATION_PROVIDER)){
                location_source="S";
            }
        }
        return location_source;
    }

    public static boolean hasLocationPermission(Context context){
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.M) {
            if ((context.checkSelfPermission(Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) ||
                    (context.checkSelfPermission(Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED))
            {
                // permission not granted
                return false;
            }
            else
            {
                // permission is granted, ok
                return true;
            }
        }
        else
        {
            // before api 23, permissions are always granted, so everything is ok
            return true;
        }
    }

    public static boolean hasBackgroundLocationPermission(Context context){
        // always present below 23
        if (android.os.Build.VERSION.SDK_INT < 23){
            return true;
        } else {
            if (android.os.Build.VERSION.SDK_INT < 29) {
                // below 29, background permission is always granted with normal "foreground" permission
                return hasLocationPermission(context);
            } else {
                if (context.checkSelfPermission(Manifest.permission.ACCESS_BACKGROUND_LOCATION) != PackageManager.PERMISSION_GRANTED){
                    return false;
                }
                return true;
            }
        }
    }

}
