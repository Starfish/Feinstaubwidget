/*

 This file is part of FeinstaubWidget.

 Copyright (c) 2018, 2019, 2020, 2021, 2022, 2023, 2024 Pawel Dube

 FeinstaubWidget is free software: you can redistribute it and/or modify it
 under the terms of the GNU General Public License as published by the
 Free Software Foundation, either version 3 of the License, or (at
 your option) any later version.

 FeinstaubWidget is distributed in the hope that it will be useful, but
 WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with FeinstaubWidget. If not, see <http://www.gnu.org/licenses/>.

 */

package de.kaffeemitkoffein.feinstaubwidget;

import android.app.PendingIntent;
import android.appwidget.AppWidgetManager;
import android.appwidget.AppWidgetProvider;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.os.Handler;
import android.widget.RemoteViews;
import java.net.URL;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.concurrent.Executor;
import java.util.concurrent.Executors;

import android.os.Bundle;

/**
 * This class defines the small widget with all the needed routines.
 *
 * The other two widget types extend this class.
 *
 */

public class StaubWidget extends AppWidgetProvider {

    /**
     * Constants to define the update action. To limit the API requests, two types of action are
     * defined:
     * - UPDATE: means new data should be read from the API (via the internet),
     *           if successful:
     *            - the widget displays the new data,
     *            - a REFRESH action is sent to the other widgets, so that they also display
     *              the new data.
     * - REFRESH: means the widget display is updated with present (local) data. This is used
     *            to keep the widget display up to date when the main app or other widgets got
     *            new data.
     *            This only refreshes the display, but does not trigger a call of the API.
     */

    public static final String WIDGET_CUSTOM_UPDATE_ACTION      = "de.kaffeemitkoffein.feinstaubwidget.WIDGET_CUSTOM_ACTION_UPDATE";
    public static final String WIDGET_CUSTOM_REFRESH_ACTION     = "de.kaffeemitkoffein.feinstaubwidget.WIDGET_CUSTOM_ACTION_REFRESH";
    public static final String WIDGET_CUSTOM_NEWLOCATION_ACTION = "de.kaffeemitkoffein.feinstaubwidget.WIDGET_CUSTOM_ACTION_NEWLOCATION";

    public static int WIDGET_CONN_RETRY_DELAY = 300000; // defines when to retry in milliseconds.
    public static int DATABASE_TIMESPAN = 86400000;     // defines the timespan of the database.

    private final static String VERBOSEID = "WIDGET-SMALL";

    FeinstaubPreferences preferences;

    Context context;

    /**
     * This is called when the widget gets enabled.
     */

    @Override
    public void onEnabled(Context c){
        super.onEnabled(c);
        context = c;
    }

    /**
     * This is called when the widget gets disabled. In the standard widget, there is nothing
     * special to do here.
     *
     * @param c
     */

    @Override
    public void onDisabled(Context c){
        super.onDisabled(c);
    }

    /**
     * Here, by overriding this class, the widget responds to the system call to update
     * all widgets of this type.
     *
     * Actions:
     * - New data is queried from the API.
     * - The local data base is cleaned up.
     *
     * @param c
     * @param awm
     * @param widget_instances
     */

    @Override
    public void onUpdate(Context c, AppWidgetManager awm, int[] widget_instances){
        context = c;
        super.onUpdate(c, awm, widget_instances);
        fetchSensorData(c,awm,widget_instances);
        cleanDatabase();
    }

    /**
     * Displays known, local data in the widgets. This is used to refresh the widget display when
     * new data arrives.
     *
     * @param c Context
     * @param awm AppWidgetManager
     * @param widget_instances widget instances
     */

    public void updateWidgetDisplay(Context c, AppWidgetManager awm, int[] widget_instances){
        final int instances = widget_instances.length;
        String time ="--:--";
        readPreferences();
        CardHandler ch = new CardHandler(c);
        SensorDataSet data1 = ch.getLatestDataSet(preferences.sensor1_number);
        SensorDataSet data2 = ch.getLatestDataSet(preferences.sensor2_number);
        for (int i=0; i<instances; i++) {
            // Register the widget view as a "button" for refresh
            RemoteViews rv = new RemoteViews(c.getPackageName(), R.layout.widget_layout);
            Intent intent = new Intent();
            intent.setClass(c,getClass());
            intent.setAction(WIDGET_CUSTOM_UPDATE_ACTION);
            PendingIntent pi;
            if (android.os.Build.VERSION.SDK_INT >= 23){
                pi = PendingIntent.getBroadcast(c,0,intent,PendingIntent.FLAG_UPDATE_CURRENT|PendingIntent.FLAG_IMMUTABLE);
            } else {
                pi = PendingIntent.getBroadcast(c,0,intent,PendingIntent.FLAG_UPDATE_CURRENT);
            }
            rv.setOnClickPendingIntent(R.id.widget_maincontainer,pi);
            DisplayManager dsm = new DisplayManager();
            WidgetDimensionManager wdm = new WidgetDimensionManager(c,awm,widget_instances[i]);
            dsm.setTheme(preferences.widget_theme);
            dsm.displayCurrentDataSet(wdm,
                                      rv,
                                      R.id.widget_time,
                                      R.id.widget_sensor1_data_1,
                                      R.id.widget_sensor1_data_2,
                                      R.id.widget_sensor2_data_1,
                                      R.id.widget_sensor2_data_2,
                                      data1,data2,preferences.display_reference,preferences.widget_opacity);
            awm.updateAppWidget(widget_instances[i], rv);
        }
    }

    /**
     * Responds to intent calls from other widgets and/or the main app.
     *
     * Possible actions are:
     * WIDGET_REFRESH_ACTION: the widget should refresh the displayed content
     * using known, locally available data.
     *
     * WIDGET_UPDATE_ACTION: all instances of this widget type are requested to
     * update, means: read new data from the API (via the internet) and display it.
     *
     * WIDGET_NEWLOCATION_ACTION: this is received when new location information
     * is available.
     *
     * You may override the following subs to define the three action.
     *
     * @param c
     * @param i
     */

    @Override
    public void onReceive(Context c, Intent i){
        context = c;
        super.onReceive(c, i);
        if (i != null){
            String action = i.getAction();
            if (action.equals(WIDGET_CUSTOM_REFRESH_ACTION)){
                widgetRefreshAction(c,i);
            }
            if (action.equals(WIDGET_CUSTOM_UPDATE_ACTION)){
                widgetUpdateAction(c,i);
            }
            if (action.equals(WIDGET_CUSTOM_NEWLOCATION_ACTION)){
                widgetNewLocationAction(c,i);
            }
        }
    }

    /**
     * Refreshes the widget with known data. This sub is called to execute the
     * WIDGET_REFRESH_ACTION.
     *
     * @param c Context
     */

    public void widgetRefreshAction(Context c, Intent i){
        AppWidgetManager awm = AppWidgetManager.getInstance(c);
        int[] wi = awm.getAppWidgetIds(new ComponentName(c,this.getClass().getName()));
        if (wi.length>0){
            updateWidgetDisplay(c,awm,wi);
        }
    }

    /**
     * This sub is called to execute the request to update
     * all instances of this widget with new data from the API.
     *
     * @param c Context
     */

    public void widgetUpdateAction(Context c, Intent i){
        AppWidgetManager awm = AppWidgetManager.getInstance(c);
        int[] wi = awm.getAppWidgetIds(new ComponentName(c,this.getClass().getName()));
        onUpdate(c,awm,wi);
    }

    /**
     * This sub is called to execute all tasks needed when a new location
     * is available.
     *
     * @param c Context
     */

    public void widgetNewLocationAction(Context c, Intent i){
        /*
         * Nothing to do in this widget, as it does not depend on location information.
         * Override this to define any action needed upon availability of new location data.
         */
    }

    /**
     * This is called when the widget gets resized.
     *
     * Upon resize, the displayed content is updated. Please note that in this case, only the one
     * affected widget gets updated. Other widgets are not affected by the resize action.
     *
     * @param c
     * @param awm
     * @param appWidgetID
     * @param newOptions
     */

    @Override
    public void onAppWidgetOptionsChanged(Context c, AppWidgetManager awm, int appWidgetID, Bundle newOptions){
        int[] idarray = new int[appWidgetID];
        updateWidgetDisplay(c,awm,idarray);
    }

    /**
     * Reads the preferences of this app.
     */

    public void readPreferences() {
        preferences = new FeinstaubPreferences(context);
    }

    /**
     * This routine garbage-collects outdated database entries.
     *
     * Please note, that for security reasons it is called on the main thread to guarantee
     * execution.
     *
     */

    public void cleanDatabase(){
        Calendar calendar = Calendar.getInstance();
        Long cut_date_in_millis = calendar.getTimeInMillis()- DATABASE_TIMESPAN; // -24h in UTC
        Date cut_date = new Date(cut_date_in_millis);
        CardHandler ch = new CardHandler(context);
        ch.deleteOldEntries(cut_date,false);
    }

    /**
     * Gets the latest data of the two sensors from the API via the internet.
     *
     * This class will be overridden by the WidgetClose.
     *
     * @param c Context
     * @param awm AppWidgetManager
     * @param widget_instances WidgetInstances
     */

    public void fetchSensorData(Context c, AppWidgetManager awm, int[] widget_instances){
        CardHandler ch = new CardHandler(context);
        ArrayList<URL> urlList = new ArrayList<URL>();
        readPreferences();
        if (!preferences.sensor1_number.equals("")){
            urlList.add(ch.getSensorAPIURL(preferences.sensor1_number));
        }
        if (!preferences.sensor2_number.equals("")){
            urlList.add(ch.getSensorAPIURL(preferences.sensor2_number));
        }
        URL[] urls = new URL[urlList.size()];
        for (int i=0; i<urlList.size(); i++){
            urls[i]=urlList.get(i);
        }
        DataFetcher df = new DataFetcher(context,urls,awm,widget_instances);
        Executor executor = Executors.newSingleThreadExecutor();
        executor.execute(df);
    }

    /**
     * Currently not used.
     *
     * Calls the LoginActivity.
     *
     * Calling the LoginActivity is handled by the system upon placing the widget on the screen.
     *
     * @param c
     */

    private void call_SensorAPILogin(Context c){
        Intent i = new Intent(c, LoginActivity.class);
        c.startActivity(i);
    }

    /**
     * This class gets the data for the widget in an async task.
     *
     * New data is automatically added to the database.
     *
     */

    public class DataFetcher extends FetchSensorDataFromAPI {
        AppWidgetManager awm_instance = null;
        int[] appwidget_ids;

        public DataFetcher(Context c, URL[] urls, AppWidgetManager awm, int[] ids){
            super(c,urls);
            this.awm_instance = awm;
            this.appwidget_ids = ids;
        }

        /**
         * The onPositiveResult routine is called when the API request of new data was
         * successful. It triggers a refresh of the display of all widgets.
         */

        @Override
        public void onPositiveResult(){
            refreshWidgetDisplays(context);
        }

        /**
          * The onNegativeResult routine generates a runnable that is delayed for WIDGET_CONN_RETRY_DELAY milliseconds.
          * Then, the widget retries to get data.
        *
        * Caution: due to typical garbage collection behaviour of the OS, this post-delayed task is at
        * risk being garbage-collected at any time and therefore may never be called at all.
        */

        @Override
        public void onNegativeResult(){
            Runnable r = new Runnable(){
                @Override
                public void run() {
                    fetchSensorData(context,awm_instance,appwidget_ids);
                }
            };
            Handler h = new Handler();
            h.removeCallbacks(r);
            h.postDelayed(r,WIDGET_CONN_RETRY_DELAY); // 5 minutes delay
        }
    }

    /**
     * Triggers a refresh of all widgets by firing an intent.
     *
     * @param c Context

     */

    public void refreshWidgetDisplays(Context c){
        Intent widget_small = new Intent(c,StaubWidget.class);
        widget_small.setAction(WIDGET_CUSTOM_REFRESH_ACTION);
        c.sendBroadcast(widget_small);
        Intent widget_big = new Intent(c,WidgetBig.class);
        widget_big.setAction(WIDGET_CUSTOM_REFRESH_ACTION);
        c.sendBroadcast(widget_big);
        Intent widget_big_single = new Intent(c,WidgetBigSingle.class);
        widget_big_single.setAction(WIDGET_CUSTOM_REFRESH_ACTION);
        c.sendBroadcast(widget_big_single);
        Intent widget_close = new Intent(c,WidgetClose.class);
        widget_close.setAction(WIDGET_CUSTOM_REFRESH_ACTION);
        c.sendBroadcast(widget_close);
    }

    /**
     * This method is called if the widget should ask the system for a location update. This is the case
     * when the widget receives a WIDGET_UPDATE_ACTION via an intent.
     *
     * You can override this in the mobile widget or other widgets and implement the features here.
     *
     * @param c Context
     */

}
