/*

 This file is part of FeinstaubWidget.

 Copyright (c) 2018, 2019, 2020, 2021, 2022, 2023, 2024 Pawel Dube

 FeinstaubWidget is free software: you can redistribute it and/or modify it
 under the terms of the GNU General Public License as published by the
 Free Software Foundation, either version 3 of the License, or (at
 your option) any later version.

 FeinstaubWidget is distributed in the hope that it will be useful, but
 WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with FeinstaubWidget. If not, see <http://www.gnu.org/licenses/>.

 */

package de.kaffeemitkoffein.feinstaubwidget;

import android.location.Location;
import android.os.Parcelable;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

import android.os.Parcel;

/**
 * This class defines the basic sensor data set that is read from the API. Please note: the Luftdaten.info API
 * presents more data; this data set defines only the data needed by this app.
 *
 * Each child of this class holds the data of one sensor at a certain point in time.
 *
 * Values stored:
 *
 * sensordataValue[0]: large particles concentration OR temperature.
 * sensordataValue[1]: small particles concentration OR humidity.
 *
 * sensorTypeName: the type of the sensor, e.g. SDS011 or DHT22. Other sensor types are currently not supported by this app.
 *
 * timestamp: time of the measurement in UTC
 * timestamp_UTC_millis: timestamp in milliseconds.
 *
 * location*: the sensor location.
 *
 * database_id: currently not used. This field is reserved for future development to support other data sources.
 *
 */

public class SensorDataSet implements Parcelable {
    String[] sensordataValue = {null, null};
    String sensorTypeName = null;
    String timestamp = null;
    String sensorId = null;
    String locationAltitude = null;
    String locationLongitude = null;
    String locationCountry = null;
    String locationLatitude = null;
    Long timestamp_UTCmillis = null;
    Integer database_id = null;

    public static final int UNKNOWN_SENSOR = 0;
    public static final int PARTICULATE_SENSOR = 1;
    public static final int TEMPERATURE_SENSOR = 2;

    public static final String[] PARTICULATE_SENSOR_TYPES = {"SDS011","HPM","SDS021"};
    public static final String[] TEMPERATURE_SENSOR_TYPES = {"DHT22","DHT11","HTU21D","BME280"};

    public static final String[] PARTICULATE_SENSOR_VALUETYPES = {"P1","P2"};
    public static final String[] TEMPERATURE_SENSOR_VALUETYPES = {"temperature","humidity"};

    /**
     * Public constructor.
     */

    public SensorDataSet(){

    }

    /**
     * Gets the time of the data measurement.
     *
     * @return
     *
     * Returns the time as a string of hours:minutes IN UTC TIME.
     *
     */

    public String getTimestampUTCTime(){
        String s;
        SimpleDateFormat simpleDateFormat_source = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        SimpleDateFormat simpleDateFormat_target = new SimpleDateFormat("HH:mm");
        try{
            Date date = simpleDateFormat_source.parse(this.timestamp);
            s = simpleDateFormat_target.format(date);
            return s;
        } catch (Exception e){
            return "";
        }
    }

    /**
     * Gets the time of the data measurement.
     *
     * @return
     *
     * Returns the time in milliseconds IN UTC TIME.
     *
     */

    public long getTimestampUTCMillis(){
        SimpleDateFormat simpleDateFormat_source = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        try{
            Date date = simpleDateFormat_source.parse(this.timestamp);
            Calendar calendar = Calendar.getInstance();
            calendar.setTime(date);
            return calendar.getTimeInMillis();
        } catch (Exception e){
            return 0;
        }
    }

    /**
     * Gets the time of the data measurement.
     *
     * @return
     *
     * Returns the time as a string of hours:minutes IN LOCAL TIME.
     *
     */

    public String getTimestampLocalTime(){
        SimpleDateFormat simpleDateFormat_target = new SimpleDateFormat("HH:mm",Locale.getDefault());
        try{
            return simpleDateFormat_target.format(getTimestampLocalMillis());
        } catch (Exception e){
            return "";
        }
    }

    /**
     * Gets the date of the data measurement.
     *
     * @return
     *
     * Returns the date as a string of year-month-day of month IN LOCAL TIME.
     *
     */

    public String getDatestampLocalTime(){
        SimpleDateFormat simpleDateFormat_target = new SimpleDateFormat("yy-MM-dd", Locale.getDefault());
        try{
            return simpleDateFormat_target.format(getTimestampLocalMillis());
        } catch (Exception e){
            return "";
        }
    }

    /**
     * Gets the time of the data measurement.
     *
     * @return
     *
     * Returns the time in milliseconds IN LOCAL TIME.
     *
     */

    public long getTimestampLocalMillis(){
        SimpleDateFormat simpleDateFormat_source = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        try {
            Date date = simpleDateFormat_source.parse(this.timestamp);
            Calendar calendar = Calendar.getInstance();
            calendar.setTime(date);
            calendar.add(Calendar.MILLISECOND,calendar.get(Calendar.DST_OFFSET)+calendar.get(Calendar.ZONE_OFFSET));
            return calendar.getTimeInMillis();
        } catch (Exception e) {
            return 0;
        }
    }

    /**
     * Gets the sensor type.
     * @return int
     *
     * Returns PARTICULATE_SENSOR (1), TEMPERATURE_SENSOR (2) or UNKNOWN_SENSOR (0)
     * if sensor is unknown.
     */

    public int getSensorType(){
        for (String sensor:PARTICULATE_SENSOR_TYPES) {
            if (sensor.equals(sensorTypeName)){
                return PARTICULATE_SENSOR;
            }
        }
        for (String sensor:TEMPERATURE_SENSOR_TYPES){
            if (sensor.equals(sensorTypeName)){
                return TEMPERATURE_SENSOR;
            }
        }
        return UNKNOWN_SENSOR;
    }

    public int getSensorValueTypeIndex(String valuetype){
     for (int i=0; i<PARTICULATE_SENSOR_VALUETYPES.length; i++){
         if (PARTICULATE_SENSOR_VALUETYPES[i].equals(valuetype)){
             return i;
         }
     }
     for (int i=0; i<TEMPERATURE_SENSOR_VALUETYPES.length; i++){
         if (TEMPERATURE_SENSOR_VALUETYPES[i].equals(valuetype)){
             return i;
         }
     }
        return 99;
    }

    /**
     * Returns the sensor data value as float.
     * @param i
     * @return
     */

    public float getSensorValueFloat(int i){
        try {
            return Float.parseFloat(sensordataValue[i]);
        } catch (NumberFormatException e){
            return 0;
        }
    }

    /**
     * Returns the sensor data value as int.
     *
     * @param i the number of the data-set
     * @return
     */

    public int getSensorValueInt(int i){
        return Math.round(getSensorValueFloat(i));
    }

    /**
     * Returns the latitude of the sensor. Returns 0 if latitude does not exist or has wrong format.
     *
     * @return
     */

    public double getSensorLatitiude(){
        try {
            return Double.parseDouble(locationLatitude);
        } catch (NumberFormatException e) {
            return 0;
        }
    }

    /**
     * Returns the longitude of the sensor. Returns 0 if longitude does not exist or has wrong format.
     *
     * @return
     */

    public double getSensorLongitude(){
        try {
            return Double.parseDouble(locationLongitude);
        } catch (NumberFormatException e) {
            return 0;
        }
    }

    /**
     * Returns the altitude of the sensor. Returns 0 if altitude does not exist or has wrong format.
     *
     * @return
     */


    public double getSensorAltitude(){
        try {
            return Double.parseDouble(locationAltitude);
        } catch (NumberFormatException e) {
            return 0;
        }
    }

    public void setSensordataValue0(String s) {
        this.sensordataValue[0] = s;
    }

    public void setSensordataValue1(String s) {
        this.sensordataValue[1] = s;
    }

    public void setSensordataValue0(Float f) {
        this.sensordataValue[0] = String.valueOf(f);
    }

    public void setSensordataValue1(Float f) {
        this.sensordataValue[1] = String.valueOf(f);
    }

    public void setSensordataValue0(int i) {
        this.sensordataValue[0] = String.valueOf(i);
    }

    public void setSensordataValue1(int i) {
        this.sensordataValue[1] = String.valueOf(i);
    }

    public FullSensorDataSet toFullSensorDataSet(){
        FullSensorDataSet fullSensorDataSet = new FullSensorDataSet();
        fullSensorDataSet.sensordataValue[0] = this.sensordataValue[0];
        fullSensorDataSet.sensordataValue[1] = this.sensordataValue[1];
        fullSensorDataSet.sensorTypeName = this.sensorTypeName;
        fullSensorDataSet.timestamp = this.timestamp;
        fullSensorDataSet.sensorId = this.sensorId;
        fullSensorDataSet.locationAltitude = this.locationAltitude;
        fullSensorDataSet.locationLongitude = this.locationLongitude;
        fullSensorDataSet.locationCountry = this.locationCountry;
        fullSensorDataSet.locationLatitude = this.locationLatitude;
        fullSensorDataSet.timestamp_UTCmillis = this.timestamp_UTCmillis;
        fullSensorDataSet.database_id = this.database_id;
        return fullSensorDataSet;
    }

    public Location toLocation(){
        Location location = new Location("SENSOR");
        location.setLatitude(Double.parseDouble(locationLatitude));
        location.setLongitude(Double.parseDouble(locationLongitude));
        location.setAltitude(Double.parseDouble(locationAltitude));
        return location;
    }

    /**
     * Convenience method to check if any values are missing.
     *
     * @return
     * Returns false if ANY of the values is null or an empty string.
     */

    public Boolean hasData(){
        if ((sensordataValue[0] == null) || (sensordataValue[1] == null))
            return false;
        if ((sensordataValue[0].equals("")) || (sensordataValue.equals("")))
            return false;
        return true;
    }


    /**
     * The following code makes this class parcelable.
     *
     * @param parcel
     * @param flags
     */

    @Override
    public void writeToParcel(Parcel parcel, int flags){
        parcel.writeStringArray(sensordataValue);
        parcel.writeString(sensorTypeName);
        parcel.writeString(timestamp);
        parcel.writeString(sensorId);
        parcel.writeString(locationAltitude);
        parcel.writeString(locationLongitude);
        parcel.writeString(locationCountry);
        parcel.writeString(locationLatitude);
        if (timestamp_UTCmillis==null){
            parcel.writeLong(-1);
        } else {
            parcel.writeLong(timestamp_UTCmillis);
        }
        if (database_id == null) {
            parcel.writeInt(-1);
        } else {
            parcel.writeInt(database_id);
        }
    }

    @Override
    public int describeContents(){
        return 0;
    }

    public SensorDataSet(Parcel parcel){
        sensordataValue = parcel.createStringArray();
        sensorTypeName = parcel.readString();
        timestamp = parcel.readString();
        sensorId = parcel.readString();
        locationAltitude = parcel.readString();
        locationLongitude = parcel.readString();
        locationCountry = parcel.readString();
        locationLatitude = parcel.readString();
        timestamp_UTCmillis = parcel.readLong();
        if (timestamp_UTCmillis == -1){
            timestamp_UTCmillis = null;
        }
        database_id = parcel.readInt();
        if (database_id==-1){
            database_id = null;
        }
    }

    public static final Parcelable.Creator CREATOR = new Parcelable.Creator(){
        public SensorDataSet createFromParcel(Parcel parcel){
            return new SensorDataSet(parcel);
        }
        public SensorDataSet[] newArray(int i){
            return new SensorDataSet[i];
        }
    };

    public double distanceTo(Location targetLocation){
        Location thisLocation = toLocation();
        if ((thisLocation!=null) && (targetLocation!=null)){
            double distance = thisLocation.distanceTo(targetLocation);
            return distance;
        }
        return -1;
    }
}

