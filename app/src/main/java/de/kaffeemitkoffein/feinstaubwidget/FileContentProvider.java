/*

 This file is part of FeinstaubWidget.

 Copyright (c) 2018, 2019, 2020, 2021, 2022, 2023, 2024 Pawel Dube

 FeinstaubWidget is free software: you can redistribute it and/or modify it
 under the terms of the GNU General Public License as published by the
 Free Software Foundation, either version 3 of the License, or (at
 your option) any later version.

 FeinstaubWidget is distributed in the hope that it will be useful, but
 WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with FeinstaubWidget. If not, see <http://www.gnu.org/licenses/>.

 */

package de.kaffeemitkoffein.feinstaubwidget;

import android.content.ContentProvider;
import android.content.ContentValues;
import android.content.Context;
import android.content.pm.ProviderInfo;
import android.database.Cursor;
import android.database.MatrixCursor;
import android.net.Uri;
import android.os.ParcelFileDescriptor;
import android.provider.OpenableColumns;
import android.system.OsConstants;
import android.webkit.MimeTypeMap;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * This is a simple ContentProvider to share files from the own app to other apps without need to grant
 * WRITE_EXTERNAL_STORAGE and/or READ_INTENAL_STORAGE permissions to the sending app.
 *
 * The resulting content uri to share will be like:
 *
 * content://testprovider/files/test.jpeg
 *              |           |     |
 *              |           |     +---> file name with extension
 *              |           +---> storage location
 *              +---> provider name
 *
 * When you supply a display name, the content uri will be extended as follows:
 *
 * content://testprovider/files/test.jpeg?displayName=pigs_in_space
 *
 * You do not need to worry about the uri, the receiving target app will take care of it. The uri follows the
 * standards expected from a ContentProvider.
 *
 * Use:
 * 1) declare the provider in AndroidManifest.xml:
 *    within then <application> </application> tags, place the following lines and
 *    - replace com.example.package with your own package name
 *    - and replace "testprovider" with something meaningful, e.g. "imageprovider".
 *
 *         <provider
 *                 android:name="com.example.package.FileContentProvider"
 *                 android:grantUriPermissions="true"
 *                 android:exported="false"
 *                 android:authorities="testprovider">
 *         </provider>
 *
 *  2) Use in your code:
 *
 *  // this is your file to share:
 *  File fileDir = getFilesDir();
 *  File outFile = new File(fileDir,"test.jpeg");
 *
 *  // get the uri from File using this class:
 *  // - first parameter must match the provider name in AndroidManifest.xml, see above
 *  // - second parameter denotes the location of the file:
 *  //   TAG_FILES          internal private app storage, as obtained by getFilesDir();
 *  //   TAG_CACHE          internal private cache storage, as obtained by getCacheDir();
 *  //   TAG_FILES_EXTERNAL external (think of it as SD-Card) private app storage, as obtained by getExternalFilesDir();
 *  //   TAG_CACHE_EXTERNAL external (think of it as SD-Card) private cache storage, as obtained by getExternalCacheDir();
 *  // - third parameter is the File
 *  // - fourth parameter is an optional display name, may be null; if null, no display name will be added to the uri
 *
 *  Uri uri = FileContentProvider.getUriForFile("testprovider",FileContentProvider.TAG_FILES,outFile,"pigs_in_space");
 *
 *  // build the intent to share
 *  // CAUTION: setFlags MUST be called AFTER setData!
 *
 *  Intent intent = new Intent(Intent.ACTION_VIEW);
 *  intent.setDataAndType(uri,"img/jpeg");
 *  intent.setFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
 *  startActivity(intent);
 *
 *  That's all.
 */

public class FileContentProvider extends ContentProvider {

    /**
     * Tags used to determine the location of the file. They mean the following:
     * TAG_FILES          internal private app storage, as obtained by getFilesDir();
     * TAG_CACHE          internal private cache storage, as obtained by getCacheDir();
     * TAG_FILES_EXTERNAL external (think of it as SD-Card) private app storage, as obtained by getExternalFilesDir();
     * TAG_CACHE_EXTERNAL external (think of it as SD-Card) private cache storage, as obtained by getExternalCacheDir();
     */

    public static final String TAG_FILES = "files";
    public static final String TAG_CACHE = "cache";
    public static final String TAG_FILES_EXTERNAL = "files-external";
    public static final String TAG_CACHE_EXTERNAL = "cache-external";

    /**
     * Just the content uri prefix.
     */

    private static final String URI_PREFIX = "content";

    /**
     * This is the static name for the displayName column in the file metadata.
     */

    public static final String DISPLAYNAME_COLUMN = "displayName";

    /**
     * Context where this class is used from.
     */

    private Context context;

    @Override
    public boolean onCreate() {
        return true;
    }

    /**
     * This is automatically called after onCreate, here we save the context for further use.
     */

    @Override
    public void attachInfo(Context context, ProviderInfo info) {
        super.attachInfo(context, info);
        this.context = context;
    }

    /**
     * Static method to get the Uri from a File.
     *
     * @param authority this must be the same authority like defined in AndroidManifest.xml, see above
     * @param storageTag must be TAG_FILES, TAG_CACHE, TAG_FILES_EXTERNAL or TAG_CACHE_EXTERNAL and is the location where the file is
     * @param file the file to share
     * @param displayName optional display name, may be null
     */

    public static Uri getUriForFile(String authority, String storageTag, File file, String displayName){
        Uri.Builder builder = new Uri.Builder();
        builder.authority(authority).path(storageTag).scheme(URI_PREFIX).appendPath(file.getName());
        if (displayName!=null){
            builder.appendQueryParameter(DISPLAYNAME_COLUMN,displayName);
        }
        Uri uri = builder.build();
        return uri;
    }

    /**
     * Static method to get the Uri from a File.
     *
     * @param context context where this method is called from
     * @param authority this must be the same authority like defined in AndroidManifest.xml, see above
     * @param storage file pointing to getFilesDir(), getCacheDir(), getExternalFilesDir() or getExternalCacheDir()
     * @param file the file to share
     * @param displayName optional display name, may be null, then the file name will be taken as displayName
     *
     * @return a content uri pointing at the file
     */

    public static Uri getUriForFile(Context context, String authority, File storage, File file, String displayName){
        String tag = null;
        if ((storage.getAbsolutePath().equals(context.getFilesDir().getAbsolutePath()))){
            // is files dir internal storage
            return getUriForFile(authority,TAG_FILES,file,displayName);
        }
        if ((storage.getAbsolutePath().equals(context.getCacheDir().getAbsolutePath()))){
            // is cache dir internal
            return getUriForFile(authority,TAG_CACHE,file,displayName);
        }
        if (storage.getAbsolutePath().equals(context.getExternalFilesDir(null).getAbsolutePath())){
            // is external storage files
            return getUriForFile(authority,TAG_FILES_EXTERNAL,file,displayName);
        }
        if (storage.getAbsolutePath().equals(context.getExternalCacheDir().getAbsolutePath())){
            // is external cache
            return getUriForFile(authority,TAG_CACHE_EXTERNAL,file,displayName);
        }
        return getUriForFile(authority,storage.toString(),file,displayName);
    }

    /**
     * Gets a File object from an uri.
     *
     * @param context the context this method is called from
     * @param uri the file uri
     *
     * @return a file object
     */

    public static File getFileForUri(Context context, Uri uri){
        List<String> pathFragments = uri.getPathSegments();
        String tag=pathFragments.get(0);
        String file = pathFragments.get(pathFragments.size()-1);
        File path = null;
        if (tag.equals(TAG_FILES)){
            path = context.getFilesDir();
        }
        if (tag.equals(TAG_FILES_EXTERNAL)){
            path = context.getExternalFilesDir(null);
        }
        if (tag.equals(TAG_CACHE)){
            path = context.getCacheDir();
        }
        if (tag.equals(TAG_CACHE_EXTERNAL)){
            path = context.getExternalCacheDir();
        }
        if (path==null){
            return new File(String.valueOf(uri));
        }
        return new File(path, file);
    }

    /**
     * Custom query to get file name and file size for a content uri.
     * You usually do not need to bother with this, this is just for compatibility with other content providers when
     * called by other apps that might expect to get file name and file size this way. Works only with a single content
     * uri, and selection, selectionArgs and sortOrter are not used at all because the result is never more than 1 item.
     *
     * @param uri the content uri pointing to the file
     * @param projection the columns required, may be null
     * @param selection not used
     * @param selectionArgs not used
     * @param sortOrder not used
     * @return
     */

    @Override
    public Cursor query(Uri uri, String[] projection, String selection, String[] selectionArgs, String sortOrder) {
        final String[] DEFAULT_COLUMNS = {OpenableColumns.DISPLAY_NAME,OpenableColumns.SIZE};
        if (projection == null){
            projection = DEFAULT_COLUMNS;
        }
        File file = getFileForUri(context,uri);
        MatrixCursor matrixCursor = new MatrixCursor(projection,1);
        Object[] objects = new Object[projection.length];
        ArrayList<String> projectionArrayList = new ArrayList<String>(Arrays.asList(projection));
        int i=0;
        if (projectionArrayList.contains(OpenableColumns.DISPLAY_NAME)){
            objects[i] = file.getName();
            i++;
        }
        if (projectionArrayList.contains(OpenableColumns.SIZE)){
            objects[i] = file.length();
            i++;
        }
        matrixCursor.addRow(objects);
        return matrixCursor;
    }

    /**
     * Gets the mime type of the uri by extension. Since this class does not mess around with extensions and the
     * extension is always preserved in the generated content uri, there is no need to go back for the original file.
     * Please note that this will even work if the file is empty or does not exist, because the mime type is simply
     * determined by the file name extension!
     *
     * @param uri the content uri
     * @return
     */

    @Override
    public String getType(Uri uri) {
        String s = uri.toString();
        int i = s.lastIndexOf(".");
        final String ext = s.substring(i+1);
        final String mimeType =MimeTypeMap.getSingleton().getMimeTypeFromExtension(MimeTypeMap.getFileExtensionFromUrl(uri.toString()));
        if (mimeType==null){
            return "application/octet-stream";
        }
        return mimeType;
    }

    /*
     * inserting a file is not supported
     */

    @Override
    public Uri insert(Uri uri, ContentValues contentValues) {
        throw new UnsupportedOperationException("inserting not supported.");
    }

    /**
     * Deletes the file, if allowed
     *
     * @param uri the content uri of the file to delete
     * @param s not used
     * @param strings not used
     * @return 1 if deletion worked, 0 if it failed.
     */

    @Override
    public int delete(Uri uri, String s, String[] strings) {
        File file = getFileForUri(context,uri);
        boolean result = file.delete();
        if (result) {
            return 1;
        }
        return 0;
    }

    /*
     * updating a file is not supported.
     */

    @Override
    public int update(Uri uri, ContentValues contentValues, String s, String[] strings) {
        throw new UnsupportedOperationException("updating not supported.");
    }

    /**
     * Returns the opened ParcelFileDescriptor for the file. This is called (automatically) by the app that
     * receives the file, no need to use it manually at all!!!
     *
     * @param uri the content uri pointing to the file
     * @param mode the access mode (see ParcelFileDescriptor docs)
     * @return the opened ParcelFileDescriptor to read the file by another app, will return null if file does not exist
     */

    @Override
    public ParcelFileDescriptor openFile(Uri uri, String mode){
        int modeInt = OsConstants.O_RDONLY;
        try {
            modeInt = ParcelFileDescriptor.parseMode(mode);
        } catch (IllegalArgumentException e){
            // nothing to do, fall back to read only if mode is invalid
        }
        final File file = getFileForUri(context,uri);
        try {
            ParcelFileDescriptor parcelFileDescriptor = ParcelFileDescriptor.open(file,modeInt);
            return parcelFileDescriptor;
        } catch (FileNotFoundException e){
            return null;
        }
    }

}
