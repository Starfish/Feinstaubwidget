/*

 This file is part of FeinstaubWidget.

 Copyright (c) 2018, 2019, 2020, 2021, 2022, 2023, 2024 Pawel Dube

 FeinstaubWidget is free software: you can redistribute it and/or modify it
 under the terms of the GNU General Public License as published by the
 Free Software Foundation, either version 3 of the License, or (at
 your option) any later version.

 FeinstaubWidget is distributed in the hope that it will be useful, but
 WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with FeinstaubWidget. If not, see <http://www.gnu.org/licenses/>.

 */


package de.kaffeemitkoffein.feinstaubwidget;

import android.content.ContentResolver;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.location.Location;
import android.view.View;
import android.widget.TextView;
import android.app.Activity;
import java.net.URL;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.concurrent.Executor;

/**
 * This class handles the local SQLite database with known sensor locations
 * and the data queries from the API.
 *
 * This class has no public constructor.
 *
 * Obtain an instance by:
 * SensorlocationHandler sh = SensorlocationHandler.getInstance();
 *
 */

public class SensorlocationHandler{

    /**
     * String holding toast message for successful data retrieval from API
     */

    private static String fetch_negativeresultMessage = null;

    /**
     * String holding toast message for failed data retrieval from API
     */

    private static String fetch_positiveresultMessage = null;

    /**
     * Local variable to store the warning-view ("please do not close the app...")
     * while downloading the data.
     */

    private static View warning_container_view;

    /**
     * Hard-coded url to get the data for the last 5 minutes of all known sensors.
     */

    public static final String LUFTDATEN_API_URL_ALL_SENSORS="https://data.sensor.community/static/v1/data.json";

    private static final String[] SQL_QUERYCOLUMNS = {
            LuftDatenContentProvider.SensorLocationsDatabaseHelper.KEY_id,
            LuftDatenContentProvider.SensorLocationsDatabaseHelper.KEY_sensornumber,
            LuftDatenContentProvider.SensorLocationsDatabaseHelper.KEY_sensortype,
            LuftDatenContentProvider.SensorLocationsDatabaseHelper.KEY_sensorSource,
            LuftDatenContentProvider.SensorLocationsDatabaseHelper.KEY_sensorlatitude,
            LuftDatenContentProvider.SensorLocationsDatabaseHelper.KEY_sensorlongitude,
            LuftDatenContentProvider.SensorLocationsDatabaseHelper.KEY_sensoraltitude};

    private static final String SQL_COMMAND_QUERYALL = "SELECT * FROM " + LuftDatenContentProvider.SensorLocationsDatabaseHelper.TABLE_NAME;

    private Context context;
    private static ContentResolver contentResolver;

    SensorlocationHandler(Context c){
        context = c;
        if (contentResolver == null){
            contentResolver = context.getApplicationContext().getContentResolver();
        }
    }

    private ContentValues getContentValuesFromLocationDataSet(LocationDataSet locationDataSet){
        ContentValues card = new ContentValues();
        card.put(LuftDatenContentProvider.SensorLocationsDatabaseHelper.KEY_sensornumber,locationDataSet.number);
        card.put(LuftDatenContentProvider.SensorLocationsDatabaseHelper.KEY_sensortype,locationDataSet.type);
        card.put(LuftDatenContentProvider.SensorLocationsDatabaseHelper.KEY_sensorSource,locationDataSet.source);
        card.put(LuftDatenContentProvider.SensorLocationsDatabaseHelper.KEY_sensorlatitude,locationDataSet.latitude);
        card.put(LuftDatenContentProvider.SensorLocationsDatabaseHelper.KEY_sensorlongitude,locationDataSet.longitude);
        card.put(LuftDatenContentProvider.SensorLocationsDatabaseHelper.KEY_sensoraltitude,locationDataSet.altitude);
        return card;
    }

    public void addLocationDataSet(LocationDataSet locationDataSet){
        // context.getApplicationContext().getContentResolver().insert(LuftDatenContentProvider.URI_SENSORLOCATIONDATA,getContentValuesFromLocationDataSet(locationDataSet));
        contentResolver.insert(LuftDatenContentProvider.URI_SENSORLOCATIONDATA,getContentValuesFromLocationDataSet(locationDataSet));
    }

    private LocationDataSet getLocationDataSetFromCursor(Cursor c){
        LocationDataSet card = new LocationDataSet();
        card.number    = c.getString(c.getColumnIndex(LuftDatenContentProvider.SensorLocationsDatabaseHelper.KEY_sensornumber));
        card.type      = c.getInt(c.getColumnIndex(LuftDatenContentProvider.SensorLocationsDatabaseHelper.KEY_sensortype));
        card.source    = c.getString(c.getColumnIndex(LuftDatenContentProvider.SensorLocationsDatabaseHelper.KEY_sensorSource));
        card.latitude  = c.getDouble(c.getColumnIndex(LuftDatenContentProvider.SensorLocationsDatabaseHelper.KEY_sensorlatitude));
        card.longitude = c.getDouble(c.getColumnIndex(LuftDatenContentProvider.SensorLocationsDatabaseHelper.KEY_sensorlongitude));
        card.altitude  = c.getDouble(c.getColumnIndex(LuftDatenContentProvider.SensorLocationsDatabaseHelper.KEY_sensoraltitude));
        return card;
    }

    public ArrayList<LocationDataSet> readLocationDataSets(String[] selectionArguments){
        Cursor c = null;
        String selection = "";
        for (int i=0; i<selectionArguments.length; i++){
            selection = selection + LuftDatenContentProvider.SensorLocationsDatabaseHelper.KEY_sensornumber + " =?";
            if (i+1<selectionArguments.length){
                selection = selection + " OR ";
            }
        }
        ArrayList<LocationDataSet> locationDataSets = new ArrayList<LocationDataSet>();
        try {
            // c = context.getApplicationContext().getContentResolver().query(LuftDatenContentProvider.URI_SENSORLOCATIONDATA,
            c = contentResolver.query(LuftDatenContentProvider.URI_SENSORLOCATIONDATA,
                    SQL_QUERYCOLUMNS,
                    selection,
                    selectionArguments,
                    null);
            if (c.moveToFirst()){
                do {
                  LocationDataSet card = getLocationDataSetFromCursor(c);
                  locationDataSets.add(card);
                } while (c.moveToNext());
            }
            c.close();
            return locationDataSets;
        } catch (Exception SQLiteException){
            try {
                c.close();
            } catch (Exception e){
                // do nothing
            }
            return null;
        }
    }

    public LocationDataSet readLocationDataSet(String sensornumber){
        String[] list = {sensornumber};
        ArrayList<LocationDataSet> locationDataSets = readLocationDataSets(list);
        if (locationDataSets != null){
            if (locationDataSets.size()>0){
                return locationDataSets.get(0);
            }
        }
        return null;
    }

    public ArrayList<LocationDataSet> getSensorLocationData (int[] sensortypes) {
        ArrayList<LocationDataSet> cards = new ArrayList<LocationDataSet>();
        try {
            String selection = "";
            for (int i=0; i<sensortypes.length; i++){
                selection = selection + LuftDatenContentProvider.SensorLocationsDatabaseHelper.KEY_sensortype + " =?";
                if (i+1<sensortypes.length){
                    selection = selection + " OR ";
                }
            }
            ArrayList<String> selectionArgumentList = new ArrayList<String>();
            for (int i=0; i<sensortypes.length;i++){
                selectionArgumentList.add(String.valueOf(sensortypes[i]));
            }
            Cursor c = null;
            String[] selectionArguments = new String[selectionArgumentList.size()];
            selectionArguments = selectionArgumentList.toArray(selectionArguments);
            String order = LuftDatenContentProvider.SensorLocationsDatabaseHelper.KEY_sensornumber + " DESC";
            try {
                // c = context.getApplicationContext().getContentResolver().query(LuftDatenContentProvider.URI_SENSORLOCATIONDATA,
                c = contentResolver.query(LuftDatenContentProvider.URI_SENSORLOCATIONDATA,
                        SQL_QUERYCOLUMNS,
                        selection,
                        selectionArguments,
                        order);
                // c = sql_db.query(TABLE_NAME, SQL_QUERYCOLUMNS, selection, selectionArguments, null, null, order, null);
                if (c.moveToFirst())
                    do {
                        LocationDataSet card = getLocationDataSetFromCursor(c);
                        cards.add(card);
                    } while (c.moveToNext());
                c.close();
            } catch (Exception SQLiteException) {
                try {
                    c.close();
                } catch (Exception e){
                    // do nothing
                }
                return null;
            }
            c.close();
            return cards;
        } catch (Exception e){
            return null;
        }
    }

    public ArrayList<Integer> getKnownSensorNumbers(int[] sensortypes){
        ArrayList<LocationDataSet> knownlocations = getSensorLocationData(sensortypes);
        ArrayList<Integer> knownsensornumbers = new ArrayList<Integer>();
        if (knownlocations != null){
            for (int i=0; i<knownlocations.size(); i++){
                LocationDataSet l = knownlocations.get(i);
                knownsensornumbers.add(l.getSensorNumber());
            }
            return knownsensornumbers;
        }
        return null;
    }

    public ArrayList<LocationDataSet> getNextSensors(Location device_location, int number, int[] sensortypes){
        ArrayList<LocationDataSet> all_locations = new ArrayList<LocationDataSet>();
        ArrayList<LocationDataSet> locations_with_distance = new ArrayList<LocationDataSet>();
        all_locations = getSensorLocationData(sensortypes);
        int progress = 0;
        if (all_locations!=null){
            for (int i=0; i<all_locations.size(); i++){
                LocationDataSet l = all_locations.get(i);
                Location sensor_location = new Location(LuftDatenContentProvider.SensorLocationsDatabaseHelper.LOCATION_PROVIDER);
                sensor_location.setLatitude(l.latitude);
                sensor_location.setLongitude(l.longitude);
                sensor_location.setAltitude(l.altitude);
                l.distance = calculateDistance(device_location,sensor_location);
                l.bearing = device_location.bearingTo(sensor_location);
                locations_with_distance.add(l);
            }
            Collections.sort(locations_with_distance, new Comparator<LocationDataSet>() {
                @Override
                public int compare(LocationDataSet t0, LocationDataSet t1) {
                    return t0.distance.compareTo(t1.distance);
                }
            });
            ArrayList<LocationDataSet> locations = new ArrayList<LocationDataSet>();
            for (int i=0; i<number; i++){
                try {
                    locations.add(locations_with_distance.get(i));
                } catch (IndexOutOfBoundsException e){
                    // ignore, as there are no database entries to fill the sensor list.
                }
            }
            return locations;
        } else {
            return null;
        }
    }


    private Float calculateDistance(Location l1, Location l2){
        return l1.distanceTo(l2);
    }

    public int getDataSetCount(){
        int i = 0;
        try {
            // Cursor c = context.getApplicationContext().getContentResolver().query(LuftDatenContentProvider.URI_SENSORLOCATIONDATA,SQL_QUERYCOLUMNS,null,null,null);
            Cursor c = contentResolver.query(LuftDatenContentProvider.URI_SENSORLOCATIONDATA,SQL_QUERYCOLUMNS,null,null,null);
            // Cursor c = sql_db.rawQuery(SQL_COMMAND_QUERYALL,null);
            if (c != null){
                i = c.getCount();
                c.close();
            }
        } catch (Exception e){
            // Nothing to do, when this operation fails the result is zero, meaning no entries.
            // Failure likely indicates that the database does not exist (yet).
        }
        return i;
    }

    public URL getWholeSensorDataAPIURL() {
        try {
            URL url = new URL(LUFTDATEN_API_URL_ALL_SENSORS);
            return url;
        } catch (Exception e) {
            return null;
        }
    }

    public Location getLocationFromSensor(String sensor){
        LocationDataSet locationDataSet = readLocationDataSet(sensor);
        if (locationDataSet != null){
           return locationDataSet.getLocation();
        } else {
            return null;
        }
    }

    public void deleteSensorDataBase(){
        contentResolver.delete(LuftDatenContentProvider.URI_SENSORLOCATIONDATA,null,null);
    }

    private TextView progressTextView = null;

    public void setFetchList_negativeresultMessage(String fetchDatabase_negativeresultMessage) {
        SensorlocationHandler.fetch_negativeresultMessage = fetchDatabase_negativeresultMessage;
    }

    public void setFetchList_positiveresultMessage(String fetchDatabase_positiveresultMessage){
        SensorlocationHandler.fetch_positiveresultMessage = fetchDatabase_positiveresultMessage;
    }

    private void updateProgress(int i){
        if (progressTextView != null) {
            progressTextView.setVisibility(View.VISIBLE);
            progressTextView.setText(context.getResources().getString(R.string.main_location_data_progress) + " " + String.valueOf(i));
        }
    }

    private void updateProgress(String s){
        if (progressTextView != null) {
            progressTextView.setVisibility(View.VISIBLE);
            progressTextView.setText(s);
        }
    }

    private void showgetLocationListFromAPIprogress(int i){
        final int progress = i;
        Activity a = (Activity) context;
        a.runOnUiThread(new Runnable(){
            public void run(){
                updateProgress(progress);
            }
        });
    }

    private void showgetLocationListFromAPIprogress(String s){
        final String progress = s;
        Activity a = (Activity) context;
        a.runOnUiThread(new Runnable(){
            public void run(){
                updateProgress(progress);
            }
        });
    }

    public void setWarning_container_view(View v){
        warning_container_view = v;
    }

    private void removeWarningViews(){
        final Activity a = (Activity) context;
        a.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if ((warning_container_view != null)){
                    warning_container_view.setVisibility(View.GONE);
                    warning_container_view.invalidate();
                }
            }
        });
    }

    public void getLocationDataFromAPI(Context context, Executor executor, int[] sensortypes, Boolean append_to_list, Runnable r_error, Runnable r_success){
        FeinstaubPreferences feinstaubPreferences = new FeinstaubPreferences(context);
        LuftDatenAPILocationStreamReader luftDatenAPILocationStreamReader = new LuftDatenAPILocationStreamReader(this);
        luftDatenAPILocationStreamReader.setContext(context);
        luftDatenAPILocationStreamReader.setSensortypes(sensortypes);
        luftDatenAPILocationStreamReader.setUrlToAllSensors();
        luftDatenAPILocationStreamReader.setProgressTextView(progressTextView);
        luftDatenAPILocationStreamReader.setWarning_container_view(warning_container_view);
        luftDatenAPILocationStreamReader.streamOnlyToDatabase();
        luftDatenAPILocationStreamReader.setRunOnError(r_error);
        luftDatenAPILocationStreamReader.setRunOnSuccess(r_success);
        if (append_to_list){
            luftDatenAPILocationStreamReader.knownlocations = getKnownSensorNumbers(sensortypes);
        }
        executor.execute(luftDatenAPILocationStreamReader);
        //luftDatenAPILocationStreamReader.run();
    }

    public void getLocationDataFromAPI(Context context, Executor executor, int[] sensortypes, Boolean append_to_list, Runnable r_error, Runnable r_success, TextView textView) {
        progressTextView = textView;
        getLocationDataFromAPI(context, executor, sensortypes, append_to_list, r_error, r_success);
    }
}
