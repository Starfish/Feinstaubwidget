/*

 This file is part of FeinstaubWidget.

 Copyright (c) 2018, 2019, 2020, 2021, 2022, 2023, 2024 Pawel Dube

 FeinstaubWidget is free software: you can redistribute it and/or modify it
 under the terms of the GNU General Public License as published by the
 Free Software Foundation, either version 3 of the License, or (at
 your option) any later version.

 FeinstaubWidget is distributed in the hope that it will be useful, but
 WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with FeinstaubWidget. If not, see <http://www.gnu.org/licenses/>.

 */

package de.kaffeemitkoffein.feinstaubwidget;

import android.Manifest;
import android.app.Activity;
import android.appwidget.AppWidgetManager;
import android.content.Context;
import android.content.Intent;
import android.graphics.*;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.Settings;
import android.view.View;
import android.widget.Button;
import android.widget.RemoteViews;

public class WidgetCloseSetup extends Activity {

    int appWidgetID = AppWidgetManager.INVALID_APPWIDGET_ID;
    Intent callingIntent;
    Context context;

    Button buttonCancel;
    Button buttonGrandForegroundLocation;
    Button buttonGrandBackgroundLocation;
    Button buttonOpenSettings;

    final static int REQUEST_LOCATION_FOREGROUND = 1;
    final static int REQUEST_LOCATION_BACKGROUND = 2;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // on api below 23, all permissions are granted so nothing to do here, just exit with OK result
        if (Build.VERSION.SDK_INT < 23) {
            setupFinished();
        }
        callingIntent = getIntent();
        context = getApplicationContext();
        if (callingIntent!=null){
            Bundle extras = callingIntent.getExtras();
            if (extras!=null){
                appWidgetID = extras.getInt(AppWidgetManager.EXTRA_APPWIDGET_ID,AppWidgetManager.INVALID_APPWIDGET_ID);
            }
        }
        if (appWidgetID!=AppWidgetManager.INVALID_APPWIDGET_ID){
            // if background permission is already granted, we are done here
            if (FeinstaubLocationManager.hasBackgroundLocationPermission(context)){
                setupFinished();
            }
            setContentView(R.layout.activity_widgetclosesetup);
            buttonCancel = (Button) findViewById(R.id.setupclose_cancel_button);
            buttonGrandForegroundLocation = (Button) findViewById(R.id.setupclose_location_foreground_button);
            buttonGrandBackgroundLocation = (Button) findViewById(R.id.setupclose_location_background_button);
            buttonOpenSettings = (Button) findViewById(R.id.setupclose_location_opensettings_button);
            displayContent();
        } else {
            setupCancelled();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    protected void onPause() {
        super.onPause();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    private void setupCancelled(){
        setResult(Activity.RESULT_CANCELED,callingIntent);
        finish();
    }

    private void setupFinished(){
        RemoteViews remoteViews = new RemoteViews(context.getPackageName(),R.layout.widget_layout_close);
        AppWidgetManager appWidgetManager = AppWidgetManager.getInstance(context);
        appWidgetManager.updateAppWidget(appWidgetID,remoteViews);
        Intent finishIntent = new Intent();
        finishIntent.putExtra(AppWidgetManager.EXTRA_APPWIDGET_ID,appWidgetID);
        setResult(RESULT_OK,finishIntent);
        finish();
    }

    public static void applyColor(final Bitmap bitmap, int color){
        Canvas canvas = new Canvas(bitmap);
        Paint paint = new Paint();
        paint.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.SRC_IN));
        paint.setColor(color);
        canvas.drawRect(0,0,bitmap.getWidth(),bitmap.getHeight(),paint);
    }

    public void displayContent(){
        buttonCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                setupCancelled();
            }
        });
        if (FeinstaubLocationManager.hasLocationPermission(context)){
            buttonGrandForegroundLocation.setVisibility(View.GONE);
        } else {
            buttonGrandForegroundLocation.setVisibility(View.VISIBLE);
            buttonGrandForegroundLocation.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (Build.VERSION.SDK_INT >= 23) {
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                requestPermissions(new String[]{Manifest.permission.ACCESS_COARSE_LOCATION,Manifest.permission.ACCESS_FINE_LOCATION},REQUEST_LOCATION_FOREGROUND);
                            }
                        });
                    }
                }
            });
        }
        if (FeinstaubLocationManager.hasBackgroundLocationPermission(context)){
            buttonGrandBackgroundLocation.setVisibility(View.GONE);
        } else {
            buttonGrandBackgroundLocation.setVisibility(View.VISIBLE);
            buttonGrandBackgroundLocation.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (Build.VERSION.SDK_INT >= 29) {
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                requestPermissions(new String[]{Manifest.permission.ACCESS_BACKGROUND_LOCATION},REQUEST_LOCATION_BACKGROUND);
                            }
                        });
                    }
                }
            });
        }
        if (Build.VERSION.SDK_INT >= 23) {
            if ((shouldShowRequestPermissionRationale(Manifest.permission.ACCESS_FINE_LOCATION)) ||
                    (shouldShowRequestPermissionRationale(Manifest.permission.ACCESS_BACKGROUND_LOCATION))){
                buttonOpenSettings.setVisibility(View.VISIBLE);
                buttonOpenSettings.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        Intent openSettingsIntent = new Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
                        openSettingsIntent.setData(Uri.parse("package:de.kaffeemitkoffein.feinstaubwidget"));
                        startActivity(openSettingsIntent);
                    }
                });
            } else {
                buttonOpenSettings.setVisibility(View.GONE);
            }
        } else {
            buttonOpenSettings.setVisibility(View.GONE);
        }
    }

    @Override
    public void onRequestPermissionsResult(int permRequestCode, String[] perms, int[] grantRes){
        displayContent();
        if (FeinstaubLocationManager.hasBackgroundLocationPermission(context)){
            setupFinished();
        }
    }


}
