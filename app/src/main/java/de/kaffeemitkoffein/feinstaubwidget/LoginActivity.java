/**

 This file is part of FeinstaubWidget.

 Copyright (c) 2018, 2019, 2020, 2021, 2022, 2023, 2024 Pawel Dube

 FeinstaubWidget is free software: you can redistribute it and/or modify it
 under the terms of the GNU General Public License as published by the
 Free Software Foundation, either version 3 of the License, or (at
 your option) any later version.

 FeinstaubWidget is distributed in the hope that it will be useful, but
 WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with FeinstaubWidget. If not, see <http://www.gnu.org/licenses/>.

 */

package de.kaffeemitkoffein.feinstaubwidget;

import android.app.Activity;
import android.appwidget.AppWidgetManager;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.location.Location;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.*;

import java.net.URL;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.concurrent.Executor;
import java.util.concurrent.Executors;

/**
 * The Login-Screen / Setup-Screen.
 */

public class LoginActivity extends Activity {

    private Executor executor;

    private int widgetid = 0;

    private FeinstaubPreferences preferences;

    private EditText editText_sensor1;
    private EditText editText_sensor2;

    private Context context;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        executor = Executors.newSingleThreadExecutor();
        readPreferences();
        Bundle bundle = getIntent().getExtras();
        /*
        Boolean called_from_settings = false;
        if (bundle != null){
            String action = bundle.getString("ACTION");
            if (action != null){
                if (action.equals("FROMSETTINGS")){
                    called_from_settings = true;
                }
            }
        }
        if (!called_from_settings){
            Intent resultintent = new Intent();
            resultintent.putExtra(AppWidgetManager.EXTRA_APPWIDGET_ID,widgetid);
            setResult(RESULT_OK,resultintent);
            finish();
        }
         */
        setContentView(R.layout.activity_login);
        if (bundle != null){
            widgetid = bundle.getInt(AppWidgetManager.EXTRA_APPWIDGET_ID, AppWidgetManager.INVALID_APPWIDGET_ID);
        }
        context = this;
        editText_sensor1 = (EditText) findViewById(R.id.login_sensor1_edit);
        editText_sensor1.setText(preferences.sensor1_number);
        checkConnection((EditText) findViewById(R.id.login_sensor1_edit),(ImageView) findViewById(R.id.login_sensor1_connresult_graph),(TextView) findViewById(R.id.login_sensor1_connresult_text),SensorDataSet.PARTICULATE_SENSOR,false);
        editText_sensor1.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                updateAppData();
                checkConnection((EditText) findViewById(R.id.login_sensor1_edit),(ImageView) findViewById(R.id.login_sensor1_connresult_graph),(TextView) findViewById(R.id.login_sensor1_connresult_text),SensorDataSet.PARTICULATE_SENSOR,preferences.autoguess_tempsensor);
            }
        });
        editText_sensor2 = (EditText) findViewById(R.id.login_sensor2_edit);
        editText_sensor2.setText(preferences.sensor2_number);
        checkConnection((EditText) findViewById(R.id.login_sensor2_edit),(ImageView) findViewById(R.id.login_sensor2_connresult_graph),(TextView) findViewById(R.id.login_sensor2_connresult_text),SensorDataSet.TEMPERATURE_SENSOR,false);
        editText_sensor2.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                updateAppData();
                checkConnection((EditText) findViewById(R.id.login_sensor2_edit),(ImageView) findViewById(R.id.login_sensor2_connresult_graph),(TextView) findViewById(R.id.login_sensor2_connresult_text),SensorDataSet.TEMPERATURE_SENSOR,false);
            }
        });
        CheckBox checkbox_drawlines = (CheckBox) findViewById(R.id.login_drawlines_checkbox);
        checkbox_drawlines.setChecked(preferences.drawlines);
        checkbox_drawlines.setOnCheckedChangeListener(new CheckBox.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                updateAppData();
            }
        });
        CheckBox checkbox_darktheme = (CheckBox) findViewById(R.id.login_usedarkcolortheme_checkbox);
        if (preferences.widget_theme == DisplayManager.WIDGET_COLOR_THEME_DARK) {
            checkbox_darktheme.setChecked(true);
        } else {
            checkbox_darktheme.setChecked(false);
        }
        checkbox_darktheme.setOnCheckedChangeListener(new CheckBox.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                updateAppData();
            }
        });
        CheckBox checkbox_compacttext = (CheckBox) findViewById(R.id.login_compacttext_checkbox);
        checkbox_compacttext.setChecked(preferences.display_only_two_lines);
        checkbox_compacttext.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                updateAppData();
            }
        });
        checkbox_compacttext.setOnCheckedChangeListener(new CheckBox.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                updateAppData();
            }
        });
        SeekBar opacity_bar = (SeekBar) findViewById(R.id.login_opacity_seekbar);
        opacity_bar.setProgress(preferences.widget_opacity);
        updateOpacityText(opacity_bar.getProgress());
        opacity_bar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int i, boolean b) {
               updateOpacityText(i);
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
               updateAppData();
            }
        });
        Button button_connect = (Button) findViewById(R.id.login_connect_ok);

        button_connect.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                updateAppData();
                if (widgetid != 0){
                    Intent resultintent = new Intent();
                    resultintent.putExtra(AppWidgetManager.EXTRA_APPWIDGET_ID,widgetid);
                    setResult(RESULT_OK,resultintent);
                }
                finish();
            }
        });

    }

    private void updateOpacityText(int i){
        TextView opacity_value = (TextView) findViewById(R.id.login_opacity_value);
        String s = " ("+String.valueOf(i)+"%) :";
        opacity_value.setText(s);
    }

    private void checkConnection(EditText editText, ImageView imageView, TextView textView, int est, Boolean guess_temp_sensor){
        String sensornumber = editText.getText().toString();
        CardHandler ch = new CardHandler(this);
        URL url = null;
        if (!sensornumber.equals(""))
        {
            url = ch.getSensorAPIURL(sensornumber);
            URL[] urls = new URL[1];
            urls[0] = url;
            DataChecker dc = new DataChecker(this,urls,imageView,textView,est,guess_temp_sensor){
                @Override
                public void autoSearchNextSensor(){
                    guessTemperatureSensorNumber();
                }
            };
            executor.execute(dc);
        }
    }

    private class SmartTemperatureSensorFetcher extends FetchSensorDataFromAPI {

        private String latitude;
        private String longitude;
        private String altitude;
        private String sensor_result = null;

        public SmartTemperatureSensorFetcher (Context c, URL[] urls, String p_latitude, String p_longitude, String p_altitude){
            super(c,urls);
            latitude = p_latitude;
            longitude = p_longitude;
            altitude = p_altitude;
        }

        @Override
        public void onNegativeResult(){
            findNextTemperatureSensor();
        }

        @Override
        public void onPositiveResult(){
            editText_sensor2.setText(sensor_result);
            preferences.sensor2_number = sensor_result;
            updateAppData();
            checkConnection((EditText) findViewById(R.id.login_sensor2_edit),(ImageView) findViewById(R.id.login_sensor2_connresult_graph),(TextView) findViewById(R.id.login_sensor2_connresult_text),SensorDataSet.TEMPERATURE_SENSOR,false);
        }

        @Override
        protected void onPostExecute(ArrayList<FullSensorDataSet> content) {
            if (content == null) {
                onNegativeResult();
            } else {
                SensorDataSet sd;
                loop:
                    for (int i=0; i<content.size(); i++){
                        sd = content.get(i);
                        if (sd.getSensorType()==sd.TEMPERATURE_SENSOR){
                            if ((sd.locationLatitude.equals(latitude)) &&
                                    (sd.locationLongitude.equals(longitude)) &&
                                    (sd.locationAltitude.equals(altitude))){
                                sensor_result = sd.sensorId;
                                onPositiveResult();
                                break loop;
                            }
                        }
                }
                if (sensor_result == null){
                    onNegativeResult();
                }
            }
        }
    }

    private void guessTemperatureSensorNumber(){
        String sensor2_guess;
        try {
            int i = Integer.valueOf(preferences.sensor1_number);
            i = i + 1;
            sensor2_guess = String.valueOf(i);
        } catch (Exception e){
            return;
        }
        CardHandler ch = new CardHandler(context);
        SensorDataSet sensor1_data = ch.getLatestDataSet(preferences.sensor1_number);
        if (sensor1_data != null){
            if ((sensor1_data.locationAltitude != null) && (sensor1_data.locationLatitude != null) && (sensor1_data.locationLongitude != null)){
                URL[] urls = new URL[1];
                urls[0]=ch.getSensorAPIURL(sensor2_guess);
                SmartTemperatureSensorFetcher stsf = new SmartTemperatureSensorFetcher(context,urls,sensor1_data.locationLatitude,sensor1_data.locationLongitude,sensor1_data.locationAltitude);
                executor.execute(stsf);
            }
        }
    }

    private void findNextTemperatureSensor(){
        try {
            CardHandler cardHandler = new CardHandler(this);
            SensorDataSet sensor1 = cardHandler.getLatestDataSet(preferences.sensor1_number);
            Location location = new Location("dummy_provider");
            location.setLatitude(sensor1.getSensorLatitiude());
            location.setLongitude(sensor1.getSensorLongitude());
            location.setAltitude(sensor1.getSensorAltitude());
            int[] sensortypes = {SensorDataSet.TEMPERATURE_SENSOR};
            SensorlocationHandler sensorlocationHandler = new SensorlocationHandler(this);
            ArrayList<LocationDataSet> locations_list = sensorlocationHandler.getNextSensors(location,1,sensortypes);
            if (locations_list!=null){
                if (locations_list.size()>0){
                    preferences.sensor2_number = locations_list.get(0).number;
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            editText_sensor2.setText(preferences.sensor2_number);
                        }
                    });
                    // setting text already triggers checkConnection in the textChangedListener
                    //checkConnection((EditText) findViewById(R.id.login_sensor2_edit),(ImageView) findViewById(R.id.login_sensor2_connresult_graph),(TextView) findViewById(R.id.login_sensor2_connresult_text),SensorDataSet.TEMPERATURE_SENSOR,false);
                }
            }
        } catch (Exception e){
            // nothing to do; finding the sensor is very resource-intensive and can fail, but this is not critical at all.
        }
    }

    /**
     * Updates widget instances by category.
     *
     * Following this update call, the widgets display present data only and do not
     * query for new data.
     *
     * The intention of this sub is to pass on now data obtained from the main app
     * to the widgets, so that they also display the most current data.
     *
     * @param update_type which widgets to update, see constants above
     */

    public void refreshWidgets(int update_type){
        if ((update_type==MainActivity.REPFRESH_ALL_WIDGETS) || (update_type == MainActivity.REPFRESH_LOCAL_WIDGETS)){
            // refresh small widgets
            Intent widget_small = new Intent(this,StaubWidget.class);
            widget_small.setAction(StaubWidget.WIDGET_CUSTOM_REFRESH_ACTION);
            sendBroadcast(widget_small);
            // refresh big widgets
            Intent widget_big = new Intent(this,WidgetBig.class);
            widget_big.setAction(StaubWidget.WIDGET_CUSTOM_REFRESH_ACTION);
            sendBroadcast(widget_big);
        }
        if ((update_type==MainActivity.REPFRESH_ALL_WIDGETS) || (update_type == MainActivity.REPFRESH_CLOSE_WIDGETS)){
            // refresh close widgets
            Intent widget_close = new Intent(this,WidgetClose.class);
            widget_close.setAction(StaubWidget.WIDGET_CUSTOM_REFRESH_ACTION);
            sendBroadcast(widget_close);
        }
    }


    private void updateAppData(){
        writeNewDefaults();

    }

    private void readPreferences() {
        preferences = new FeinstaubPreferences(this);
    }

    private void writeNewDefaults() {
        EditText editText_sensor1 = (EditText) findViewById(R.id.login_sensor1_edit);
        EditText editText_sensor2 = (EditText) findViewById(R.id.login_sensor2_edit);
        CheckBox checkbox_drawlines = (CheckBox) findViewById(R.id.login_drawlines_checkbox);
        CheckBox checkbox_darktheme = (CheckBox) findViewById(R.id.login_usedarkcolortheme_checkbox);
        CheckBox checkbox_twolines = (CheckBox) findViewById(R.id.login_compacttext_checkbox);
        SeekBar seekbar_opacity = (SeekBar) findViewById(R.id.login_opacity_seekbar);
        preferences.sensor1_number = editText_sensor1.getText().toString();
        preferences.applyPreference(FeinstaubPreferences.PREF_SENSOR1_NUMBER, preferences.sensor1_number);
        preferences.sensor2_number = editText_sensor2.getText().toString();
        preferences.applyPreference(FeinstaubPreferences.PREF_SENSOR2_NUMBER,preferences.sensor2_number);
        if (checkbox_darktheme.isChecked()){
            preferences.widget_theme = DisplayManager.WIDGET_COLOR_THEME_DARK;
        } else {
            preferences.widget_theme = DisplayManager.WIDGET_COLOR_THEME_LIGHT;
        }
        preferences.applyPreference(FeinstaubPreferences.PREF_THEME,preferences.widget_theme);
        preferences.drawlines = checkbox_drawlines.isChecked();
        preferences.applyPreference(FeinstaubPreferences.PREF_DRAWLINES,preferences.drawlines);
        preferences.display_only_two_lines = checkbox_twolines.isChecked();
        preferences.applyPreference(FeinstaubPreferences.PREF_TWOLINES,preferences.display_only_two_lines);
        preferences.widget_opacity = seekbar_opacity.getProgress();
        preferences.applyPreference(FeinstaubPreferences.PREF_OPACITY,preferences.widget_opacity);
    }

    /**
     * This class checks the connectivity for a sensor number.
     *
     * It extends the FetchSensorDataFromAPI class to perform a connectivity check.
     *
     * The results are directly displayed by updating the views given to the public constructor.
     * See below for more details.
     *
     */

    public class DataChecker extends FetchSensorDataFromAPI {

        private ImageView resultGraphics;
        private TextView resultText;
        private int expected_sensortype;
        private Boolean do_sensorguess = false;

        /**
         * Public constructor for this class.
         *
         * The result is presented by updating the given views:
         *
         * @param c     activity context.
         * @param iv    ImageView where to place the conn.-result icon. May be null.
         * @param tv    TextView where to place the conn.-result text. May be null.
         * @param est   Expected sensor type. May be null to ignore check for expected sensor type.
         * @param b     Sets if an auto-guess of the temperature sensor should be performed. This makes the class to call the autoSearchNextSensor() sub, which then needs be overridden with the desired task.
         *
         *
         */

        public DataChecker(Context c, URL[] urls, ImageView iv, TextView tv, int est, Boolean b){
            super(c,urls);
            context = c;
            resultGraphics = iv;
            resultText = tv;
            expected_sensortype = est;
            do_sensorguess = b;
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    resultGraphics.setImageResource(R.mipmap.ic_sync_white_24dp);
                    resultText.setText(context.getResources().getString(R.string.login_connresult_trying));
                    resultText.setTextColor(Color.YELLOW);
                }
            });
        }


        /**
         * Override this sub with the routines/call to auto-guess the temperature sensor.
         * It does nothing by default.
         *
         * This sub is called finally after a connection was successfully made. It is not called if the conn. fails.
         *
         */

        public void autoSearchNextSensor(){
            // add action to search for sensor here.
        }

        @Override
        public void onPositiveResult(final ArrayList<Integer> sensortypes){
            // updateWidgetDisplay(context,AppWidgetManager.getInstance(context),AppWidgetManager.getInstance(context).getAppWidgetIds(new ComponentName(context,StaubWidget.class)));
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    int sensortype = SensorDataSet.UNKNOWN_SENSOR;
                    if (sensortypes.size()>0)
                        sensortype = sensortypes.get(0);
                    if (sensortypes.size()==0){
                        if (resultGraphics != null) {
                            resultGraphics.setImageResource(R.mipmap.ic_warning_white_24dp);
                        }
                        if (resultText != null) {
                            resultText.setText(context.getResources().getString(R.string.login_connresult_nodata));
                            resultText.setTextColor(Color.YELLOW);
                        }
                    } else {
                        if (sensortype == expected_sensortype){
                            if (resultGraphics != null) {
                                resultGraphics.setImageResource(R.mipmap.ic_https_white_24dp);
                            }
                            if (resultText != null) {
                                resultText.setText(context.getResources().getString(R.string.login_connresult_ok));
                                resultText.setTextColor(Color.GREEN);
                            }
                        } else {
                            if (resultGraphics != null) {
                                resultGraphics.setImageResource(R.mipmap.ic_error_white_24dp);
                            }
                            if (resultText != null) {
                                resultText.setText(context.getResources().getString(R.string.login_wrongsensortype));
                                resultText.setTextColor(Color.YELLOW);
                            }
                        }
                    }
                }
            });
            if (do_sensorguess){
                autoSearchNextSensor();
            }
        }

        @Override
        public void onNegativeResult(){
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    if (resultGraphics != null) {
                        resultGraphics.setImageResource(R.mipmap.ic_sync_problem_white_24dp);
                    }
                    if (resultText != null) {
                        resultText.setText(context.getResources().getString(R.string.login_connresult_fail));
                        resultText.setTextColor(Color.RED);
                    }
                }
            });
        }
    }

}
