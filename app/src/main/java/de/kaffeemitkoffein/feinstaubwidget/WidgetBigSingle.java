/*

 This file is part of FeinstaubWidget.

 Copyright (c) 2018, 2019, 2020, 2021, 2022, 2023, 2024 Pawel Dube

 FeinstaubWidget is free software: you can redistribute it and/or modify it
 under the terms of the GNU General Public License as published by the
 Free Software Foundation, either version 3 of the License, or (at
 your option) any later version.

 FeinstaubWidget is distributed in the hope that it will be useful, but
 WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with FeinstaubWidget. If not, see <http://www.gnu.org/licenses/>.

 */

package de.kaffeemitkoffein.feinstaubwidget;


import android.annotation.SuppressLint;
import android.app.PendingIntent;
import android.appwidget.AppWidgetManager;
import android.content.Context;
import android.content.Intent;
import android.view.View;

import android.widget.RemoteViews;


import java.util.ArrayList;

/**
 * This class defines the large widget with the graphs.
 *
 * Please note that is extends the StaubWidget class; for all the basic widget functionality, see
 * this class.
 *
 */


public class WidgetBigSingle extends StaubWidget {

    /**
     * Refreshes the widget display with known, local data.
     *
     * Please note that this class extends StaubWidget; basic widget functionality is implemented
     * there.
     *
     * @param c Context
     * @param awm AppWidgetManager
     * @param widget_instances widget instances
     */

    @SuppressLint("UnspecifiedImmutableFlag")
    @Override
    public void updateWidgetDisplay(Context c, AppWidgetManager awm, int[] widget_instances){
        final int instances = widget_instances.length;
        readPreferences();
        CardHandler ch = new CardHandler(c);
        ArrayList<SensorDataSet> sensordata = new ArrayList<SensorDataSet>();
        SensorDataSet data1 = ch.getLatestDataSet(preferences.sensor1_number);
        for (int i=0; i<instances; i++) {
            // Register the widget view as a "button" for refresh
            RemoteViews rv = new RemoteViews(c.getPackageName(), R.layout.widget_layout_big_single);
            Intent intent = new Intent();
            intent.setClass(c,getClass());
            intent.setAction(WIDGET_CUSTOM_UPDATE_ACTION);
            PendingIntent pi;
            if (android.os.Build.VERSION.SDK_INT >= 23) {
                pi = PendingIntent.getBroadcast(c,0,intent,PendingIntent.FLAG_UPDATE_CURRENT|PendingIntent.FLAG_IMMUTABLE);
            } else {
                pi = PendingIntent.getBroadcast(c,0,intent,PendingIntent.FLAG_UPDATE_CURRENT);
            }
            rv.setOnClickPendingIntent(R.id.widget_maincontainer_big_relative,pi);
            // update the widget
            WidgetDimensionManager wdm = new WidgetDimensionManager(c,awm,widget_instances[i]);
            DisplayManager dsm = new DisplayManager();
            dsm.setTheme(preferences.widget_theme);
            dsm.connectLines(preferences.drawlines);
            rv=dsm.displayCurrentDataSetBig(wdm,
                    data1,
                    rv,
                    R.id.widget_big_line1_left,
                    R.id.widget_big_line2_left,
                    R.id.widget_big_line3_left,
                    R.id.widget_big_left_graph,
                    preferences.display_only_two_lines,
                    preferences.display_reference,
                    preferences.widget_opacity);
            if (preferences.display_only_two_lines) {
                rv.setViewVisibility(R.id.widget_big_line3_left, View.GONE);
            }
            awm.updateAppWidget(widget_instances[i], rv);
        }
    }
}
