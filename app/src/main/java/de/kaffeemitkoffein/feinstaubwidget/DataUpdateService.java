/*
        This file is part of FeinstaubWidget.

        Copyright (c) 2018, 2019, 2020, 2021, 2022, 2023, 2024 Pawel Dube

        FeinstaubWidget is free software: you can redistribute it and/or modify it
        under the terms of the GNU General Public License as published by the
        Free Software Foundation, either version 3 of the License, or (at
        your option) any later version.

        FeinstaubWidget is distributed in the hope that it will be useful, but
        WITHOUT ANY WARRANTY; without even the implied warranty of
        MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
        General Public License for more details.

        You should have received a copy of the GNU General Public License
        along with FeinstaubWidget. If not, see <http://www.gnu.org/licenses/>.
*/

package de.kaffeemitkoffein.feinstaubwidget;

import android.annotation.TargetApi;
import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.Network;
import android.net.NetworkCapabilities;
import android.net.NetworkInfo;
import android.os.Build;
import android.os.IBinder;
import android.os.Parcelable;

import java.net.URL;
import java.util.ArrayList;
import java.util.Calendar;

@TargetApi(Build.VERSION_CODES.N)
public class DataUpdateService extends Service {

    private NotificationManager notificationManager;
    int notification_id;
    Notification notification;
    Notification.Builder notificationBuilder;

    private static boolean serviceStarted = false;

    public static String IC_ID = "FEINSTAUB_NOTIFICATION";
    public static String IC_NAME = "Updating data service";
    public static int    IC_IMPORTANCE = NotificationManager.IMPORTANCE_LOW;

    private Object networkCallback;
    private ConnectivityManager connectivityManager;

    private static final long TIMEOUTTASK_DELAY = 1000*60*60*3; // 3 minutes

    public final static String ACTION_SHOW_PROGRESS = "ACTION_SHOW_PROGRESS";
    public final static String ACTION_HIDE_PROGRESS = "ACTION_HIDE_PROGRESS";
    public final static String ACTION_UPDATE="ACTION_UPDATE";

    public final static String SERVICE_SETUP_URLS = "SERVICE_SETUP_URLS";
    public final static String SERVICE_SETUP_NUMBER = "SERVICE_SETUP_NUMBER";
    public final static String SERVICE_SETUP_SENSORTYPES = "SERVICE_SETUP_SENSORTYPES";

    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    private void stopThisService(){
        Intent intent = new Intent();
        intent.setAction(ACTION_HIDE_PROGRESS);
        sendBroadcast(intent);
        notificationManager.cancel(notification_id);
        if ((connectivityManager!=null) && (networkCallback!=null) && (Build.VERSION.SDK_INT > 23)){
            connectivityManager.unregisterNetworkCallback((ConnectivityManager.NetworkCallback) networkCallback);
        }
        stopSelf();
    }

    @Override
    public void onCreate(){
        notificationManager = (NotificationManager)getSystemService(NOTIFICATION_SERVICE);
        notification_id = (int) Calendar.getInstance().getTimeInMillis();
        notification = getNotification();
        startForeground(notification_id,notification);
        serviceStarted = false;
    }

    public int onStartCommand(Intent intent, int flags, int startID) {
        if (!serviceStarted) {
            serviceStarted = true;
            connectivityManager = (ConnectivityManager) this.getApplicationContext().getSystemService(Context.CONNECTIVITY_SERVICE);
            // must have data & internet conn, otherwise skip
            if ((intent!=null) && (isConnectedToInternet(this))){
                LuftDatenAPIStreamReader luftDatenAPIStreamReader = new LuftDatenAPIStreamReader();
                if (intent.hasExtra(SERVICE_SETUP_NUMBER)){
                    luftDatenAPIStreamReader.setNumberOfValidSensorsToRead(intent.getExtras().getInt(SERVICE_SETUP_NUMBER));
                }
                if (intent.hasExtra(SERVICE_SETUP_URLS)){
                    ArrayList<String> urlArrayList = intent.getStringArrayListExtra(SERVICE_SETUP_URLS);
                }


            }
        }
        return START_NOT_STICKY;
    }

    private Notification getNotification(){
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O){
            NotificationChannel nc = new NotificationChannel(IC_ID,IC_NAME,IC_IMPORTANCE);
            nc.setDescription(getResources().getString(R.string.service_notification_channelname));
            nc.setShowBadge(true);
            notificationManager.createNotificationChannel(nc);
        }
        // Generate a unique ID for the notification, derived from the current time. The tag ist static.
        Notification n;
        notificationBuilder = new Notification.Builder(getApplicationContext());
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            n = notificationBuilder
                    .setContentTitle(getResources().getString(R.string.service_notification_title))
                    .setStyle(new Notification.BigTextStyle().bigText(getResources().getString(R.string.service_notification_text)))
                    //.setContentText(getResources().getString(R.string.service_notification_text0))
                    .setSmallIcon(R.mipmap.ic_launcher)
                    .setAutoCancel(true)
                    .setOngoing(false)
                    .setProgress(4,0,true)
                    .setChannelId(IC_ID)
                    .build();
        } else {
            n = notificationBuilder
                    .setContentTitle(getResources().getString(R.string.service_notification_title))
                    .setStyle(new Notification.BigTextStyle().bigText(getResources().getString(R.string.service_notification_text)))
                    // .setContentText(getResources().getString(R.string.service_notification_text0))
                    .setSmallIcon(R.mipmap.ic_launcher)
                    .setProgress(4,0,true)
                    .setAutoCancel(true)
                    .build();
        }
        return n;
    }

    public static boolean isConnectedToInternet(Context context){
        try {
            ConnectivityManager connectivityManager = (ConnectivityManager) context.getApplicationContext().getSystemService(Context.CONNECTIVITY_SERVICE);
            if (connectivityManager!=null){
                // use networkInfo for api below 23
                if (Build.VERSION.SDK_INT < 23){
                    NetworkInfo networkInfo = connectivityManager.getActiveNetworkInfo();
                    if (networkInfo != null) {
                        // returns if the network can establish connections and pass data.
                        return networkInfo.isConnected();
                    }
                    return false;
                    // use connectivityManager on api 23 and higher
                } else {
                    Network network = connectivityManager.getActiveNetwork();
                    NetworkCapabilities networkCapabilities = connectivityManager.getNetworkCapabilities(network);
                    if (networkCapabilities==null) {
                        // no network
                        return false;
                    } else {
                        if (networkCapabilities.hasCapability(NetworkCapabilities.NET_CAPABILITY_INTERNET) && (networkCapabilities.hasCapability(NetworkCapabilities.NET_CAPABILITY_VALIDATED))){
                            // valid internet conn
                            return true;
                        } else {
                            // conn is not proven
                            return false;
                        }
                    }
                }
            }
        } catch (Exception e){
            // connectivityManager not found, return true to be safe
        }
        return true;
    }
}

