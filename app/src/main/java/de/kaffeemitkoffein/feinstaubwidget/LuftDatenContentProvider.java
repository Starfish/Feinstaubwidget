/*

 This file is part of FeinstaubWidget.

 Copyright (c) 2018, 2019, 2020, 2021, 2022, 2023, 2024 Pawel Dube

 FeinstaubWidget is free software: you can redistribute it and/or modify it
 under the terms of the GNU General Public License as published by the
 Free Software Foundation, either version 3 of the License, or (at
 your option) any later version.

 FeinstaubWidget is distributed in the hope that it will be useful, but
 WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with FeinstaubWidget. If not, see <http://www.gnu.org/licenses/>.

 */

package de.kaffeemitkoffein.feinstaubwidget;

import android.content.ContentProvider;
import android.content.ContentValues;
import android.content.UriMatcher;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.net.Uri;
import java.io.File;

/**
 * This content provider is an interface to provide access to the local SQLite data bases
 * that store the app data. The relations between the classes within this app are:
 *                                                                                   Basic data elements are:
 *                +----------------+      +-------------------+
 *                |SQLite database |      |SQLite database    |                      - SensorDataSet
 *                |(sensordata)    |      |(sensor locations) |                         +-> FullSensorDataSet
 *                +----------------+      +-------------------+                      - LocationDataSet
 *
 *                         + ^                      + ^
 *                reading  | | writing      reading | | writing
 *                         v +                      v +                              Three classes read data from
 *                                                                                   the API:
 *                +----------------------------------------------------------+
 *                |              LuftDatenContentProvider                    | <---+ LuftDatenAPIStreamReader
 *                |              (ContentProvider that manages all           | <---+ LuftDatenAPILocationReader
 *                |              accesses to the two SQLite databases)       | <---+ FetchSensorDataFromAPI
 *                +----------------------------------------------------------+
 *
 *                        ^                                 ^
 *                        |  data operations                |  data operations
 *                        v                                 v
 *
 *   +--------------------------------------------------------------------------------+
 *   | +---------------------------------+    +-------------------------------------+ |
 *   | | CardHandler                     |    | SensorLocationHandler               | |
 *   | | (class with convenient methods  |    | (class with convenient methods      | |
 *   | | to read & write the sensordata) |    | to read & write the sensor location | |
 *   | +---------------------------------+    | data)                               | |
 *   |                                        +-------------------------------------+ |
 *   +--------------------------------------------------------------------------------+
 *
 *         ^                   ^                    ^                      ^  all app components interact with the
 *         |                   |                    |                      |  CardHandler and the SensorLocationHandler
 *         v                   v                    v                      v  to handle the data.
 *
 *   +--------------+  +--------------+  +----------------------+  +-------------------------------------+
 *   | Small Widget |  | Large Widget |  | Close Sensors Widget |  | Main App, SensorInfo, LoginActivity |
 *   +--------------+  +--------------+  +----------------------+  +-------------------------------------+
 *
 *         ^                 ^            ^                               +
 *         |                 |            |                               |
 *         v                 v            v                               v
 *
 * +-------------------------------------------------------------------------------+
 * |   all the widgets launch intents that indicate whenever the visible displays  |
 * |   should be updated with new data. The main app also launches intents to      |
 * |   notify the widgets about new data available.                                |
 * +-------------------------------------------------------------------------------+
 *
 */

public class LuftDatenContentProvider extends ContentProvider{

    static final String AUTHORITY="de.kaffeemitkoffein.feinstaubwidget.luftdatenprovider";

    static final String DATASERVICE = "luftdaten";
    static final String URL_SENSORDATA = "content://"+AUTHORITY+"/" + DATASERVICE;
    static final Uri URI_SENSORDATA = Uri.parse(URL_SENSORDATA);

    private LuftDatenDatabaseHelper luftDatenDatabase;
    private SQLiteDatabase luftDatenSQLdatabase;

    static final String SENSORLOCATIONSERVICE = "sensorlocations";
    static final String URL_SENSORLOCATIONDATA = "content://"+AUTHORITY+"/" + SENSORLOCATIONSERVICE;
    static final Uri URI_SENSORLOCATIONDATA = Uri.parse(URL_SENSORLOCATIONDATA);

    private SensorLocationsDatabaseHelper sensorLocationsDatabase;
    private SQLiteDatabase sensorLocationsSQLdatabase;

    private static final int SENSORDATASETS = 1;
    private static final int SENSORLOCATIONSETS = 2;

    private static final UriMatcher uriMatcher = new UriMatcher(UriMatcher.NO_MATCH);
    static {
        uriMatcher.addURI(AUTHORITY,"luftdaten",SENSORDATASETS);
        uriMatcher.addURI(AUTHORITY,"sensorlocations",SENSORLOCATIONSETS);
    }

    @Override
    public boolean onCreate() {
        // sensordata database
        luftDatenDatabase          = new LuftDatenDatabaseHelper(getContext().getApplicationContext());
        // sensor locations database
        sensorLocationsDatabase    = new SensorLocationsDatabaseHelper(getContext().getApplicationContext());
        return true;
    }

    public Cursor query(Uri uri, String[] projection, String selection, String[] selectionArgs, String sortOrder){
        String tablename = null;
        SQLiteDatabase sqLiteDatabase = null;
        switch (uriMatcher.match(uri)) {
            case SENSORDATASETS:
                luftDatenSQLdatabase = luftDatenDatabase.getReadableDatabase();
                sqLiteDatabase = luftDatenSQLdatabase;
                tablename = LuftDatenDatabaseHelper.TABLE_NAME;
                break;
            case SENSORLOCATIONSETS:
                sensorLocationsSQLdatabase = sensorLocationsDatabase.getReadableDatabase();
                sqLiteDatabase = sensorLocationsSQLdatabase;
                tablename = SensorLocationsDatabaseHelper.TABLE_NAME;
                break;
        }
        sqLiteDatabase.enableWriteAheadLogging();
        Cursor c = sqLiteDatabase.query(tablename,projection,selection,selectionArgs,null,null,sortOrder,null);
        return c;
    }

    public int update(Uri uri, ContentValues contentValues, String selection, String[] selectionArgs){
        String tablename = null;
        SQLiteDatabase sqLiteDatabase = null;
        switch (uriMatcher.match(uri)) {
            case SENSORDATASETS:
                luftDatenSQLdatabase = luftDatenDatabase.getWritableDatabase();
                sqLiteDatabase = luftDatenSQLdatabase;
                tablename = LuftDatenDatabaseHelper.TABLE_NAME;
                break;
            case SENSORLOCATIONSETS:
                sensorLocationsSQLdatabase = sensorLocationsDatabase.getWritableDatabase();
                sqLiteDatabase = sensorLocationsSQLdatabase;
                tablename = SensorLocationsDatabaseHelper.TABLE_NAME;
                break;
        }
        sqLiteDatabase.enableWriteAheadLogging();
        int i = sqLiteDatabase.update(tablename,contentValues,selection,selectionArgs);
        return i;
    }

    public Uri insert(Uri uri, ContentValues contentValues){
        String tablename = null;
        SQLiteDatabase sqLiteDatabase = null;
        switch (uriMatcher.match(uri)) {
            case SENSORDATASETS:
                luftDatenSQLdatabase = luftDatenDatabase.getWritableDatabase();
                sqLiteDatabase = luftDatenSQLdatabase;
                tablename = LuftDatenDatabaseHelper.TABLE_NAME;
                break;
            case SENSORLOCATIONSETS:
                sensorLocationsSQLdatabase = sensorLocationsDatabase.getWritableDatabase();
                sqLiteDatabase = sensorLocationsSQLdatabase;
                tablename = SensorLocationsDatabaseHelper.TABLE_NAME;
                break;
        }
        sqLiteDatabase.enableWriteAheadLogging();
        if (contentValues!=null){
            long result = sqLiteDatabase.insert(tablename,null,contentValues);
            if (result == -1){
                // error
            }
        }
        // to do : call notify changed
        return uri;
    }

    @Override
    public int delete(Uri uri, String selection, String[] selectionArgs){
        String tablename = null;
        SQLiteDatabase sqLiteDatabase = null;
        switch (uriMatcher.match(uri)) {
            case SENSORDATASETS:
                luftDatenSQLdatabase = luftDatenDatabase.getWritableDatabase();
                sqLiteDatabase = luftDatenSQLdatabase;
                tablename = LuftDatenDatabaseHelper.TABLE_NAME;
                break;
            case SENSORLOCATIONSETS:
                sensorLocationsSQLdatabase = sensorLocationsDatabase.getWritableDatabase();
                sqLiteDatabase = sensorLocationsSQLdatabase;
                tablename = SensorLocationsDatabaseHelper.TABLE_NAME;
                break;
        }
        sqLiteDatabase.enableWriteAheadLogging();
        int i = sqLiteDatabase.delete(tablename,selection,selectionArgs);
        return i;

    }

    @Override
    public String getType(Uri uri){
        return "text/plain";
    }

    public static class LuftDatenDatabaseHelper extends SQLiteOpenHelper{

        public static final int DATABASE_VERSION = 2;
        public static final String DATABASE_NAME = "Luftdaten";
        public static final String TABLE_NAME = "Sensordata";
        public static final String KEY_id = "id";
        public static final String KEY_sensordataValue0 = "sensordataValue0";
        public static final String KEY_sensordataValue1 = "sensordataValue1";
        public static final String KEY_sensorId = "sensorId";
        public static final String KEY_sensorTypeName = "sensorTypeName";
        public static final String KEY_timestamp = "timestamp";
        public static final String KEY_locationAltitude = "locationAltitude";
        public static final String KEY_locationLongitude = "locationLongitude";
        public static final String KEY_locationCountry = "locationCountry";
        public static final String KEY_locationLatitude = "locationLatitude";
        public static final String KEY_timestampUTCmillis = "timestampUTCmillis";

        public static final String SQL_COMMAND_CREATE = "CREATE TABLE "+TABLE_NAME+"("
                + KEY_id+" INTEGER PRIMARY KEY ASC,"
                + KEY_sensordataValue0+" TEXT,"
                + KEY_sensordataValue1+" TEXT,"
                + KEY_sensorId+" TEXT,"
                + KEY_sensorTypeName+" TEXT,"
                + KEY_timestamp+" TEXT,"
                + KEY_locationAltitude+" TEXT,"
                + KEY_locationLongitude+" TEXT,"
                + KEY_locationLatitude+" TEXT,"
                + KEY_locationCountry+" TEXT,"
                + KEY_timestampUTCmillis + " INTEGER"+");";

        public static final String SQL_COMMAND_DROP_TABLE = "DROP TABLE IF EXISTS "+TABLE_NAME;

        LuftDatenDatabaseHelper(Context context){
            super(context,DATABASE_NAME,null,DATABASE_VERSION);
        }

        @Override
        public void onCreate(SQLiteDatabase sql_db){
            sql_db.execSQL(SQL_COMMAND_CREATE);
        }

        @Override
        public void onUpgrade(SQLiteDatabase sql_db, int version1, int version2){
            sql_db.execSQL(SQL_COMMAND_DROP_TABLE);
            onCreate(sql_db);
        }

    }

    public static class SensorLocationsDatabaseHelper extends SQLiteOpenHelper{

        /**
         * This variable holds the version of the database. This number must be
         * increased if the structure of the data base is modified.
         */

        private static final int DATA_VERSION = 2;

        /**
         * Database name within the app.
         */

        public static final String DATABASE_NAME = "Luftdaten2";

        /**
         * A string to define the "location provider" for the coordinates of the sensors.
         */

        public static final String LOCATION_PROVIDER = "LUFTDATEN.INFO";

        public static final String TABLE_NAME = "Locationdata";
        public static final String KEY_id = "ID";
        public static final String KEY_sensornumber = "SENSOR_NUMBER";
        public static final String KEY_sensortype = "SENSOR_TYPE";
        public static final String KEY_sensorSource = "SENSOR_SOURCE";
        public static final String KEY_sensorlatitude = "SENSOR_LATITUDE";
        public static final String KEY_sensorlongitude = "SENSOR_LONGITUDE";
        public static final String KEY_sensoraltitude = "SENSOR_ALTITUDE";

        // private static final String SQL_COMMAND_CREATE = "CREATE TABLE IF NOT EXISTS "+TABLE_NAME+"("
        private static final String SQL_COMMAND_CREATE = "CREATE TABLE "+TABLE_NAME+"("
                + KEY_id+" INTEGER PRIMARY KEY ASC,"
                + KEY_sensornumber+" TEXT,"
                + KEY_sensortype+" TEXT,"
                + KEY_sensorSource+" TEXT,"
                + KEY_sensorlatitude+" TEXT,"
                + KEY_sensorlongitude+" TEXT,"
                + KEY_sensoraltitude+ " TEXT" + ");";

        private static final String SQL_COMMAND_DROP_TABLE = "DROP TABLE IF EXISTS "+TABLE_NAME;

        SensorLocationsDatabaseHelper(Context c){
            super(c,DATABASE_NAME,null,DATA_VERSION);
        }

        @Override
        public void onCreate(SQLiteDatabase sql_db){
            sql_db.execSQL(SQL_COMMAND_CREATE);
        }

        public void dropDB(SQLiteDatabase sql_db) {
            try{
                sql_db.execSQL(SQL_COMMAND_DROP_TABLE);
            } catch (Exception e){
                //
            }
        }

        public void resetDB(SQLiteDatabase sql_db){
            dropDB(sql_db);
            onCreate(sql_db);
        }

        @Override
        public void onUpgrade(SQLiteDatabase sql_db, int version1, int version2){
            onCreate(sql_db);
        }

    }

    }
