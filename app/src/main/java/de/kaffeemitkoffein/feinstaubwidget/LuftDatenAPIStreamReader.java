/**

 This file is part of FeinstaubWidget.

 Copyright (c) 2018, 2019, 2020, 2021, 2022, 2023, 2024 Pawel Dube

 FeinstaubWidget is free software: you can redistribute it and/or modify it
 under the terms of the GNU General Public License as published by the
 Free Software Foundation, either version 3 of the License, or (at
 your option) any later version.

 FeinstaubWidget is distributed in the hope that it will be useful, but
 WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with FeinstaubWidget. If not, see <http://www.gnu.org/licenses/>.

 */

package de.kaffeemitkoffein.feinstaubwidget;

import android.app.Activity;
import android.util.JsonReader;
import android.view.View;
import android.widget.TextView;
import javax.net.ssl.HttpsURLConnection;
import java.io.*;
import java.net.URL;
import java.util.ArrayList;
import java.util.Arrays;

import android.content.Context;

/**
 * This is a multi-purpose, very powerful class to read data from the Luftdaten.Info API.
 *
 * It is designed to run in a separate thread. The basic use is:
 *
 *         LuftDatenAPIStreamReader streamReader = new LuftDatenAPIStreamReader();
 *
 *         streamReader.setContext(context);
 *         streamReader.setUrls(urls);
 *         streamReader.setResultDataList(content);
 *         streamReader.run();
 *
 *         Where:
 *         context is the Context of the current activity,
 *         urls is an array of urls to read,
 *         content is an ArrayList of FullSensorDataSets where the results are saved.
 *
 *
 * Always remember that this thread runs independently of the main ui thread. To get notified
 * when the class has finished the job, you can define two runnables:
 *
 *         Runnable r_success = new Runnable(...);
 *         Runnable r_fail = new Runnable(...);
 *
 *         streamReader.setRunOnSuccess(r);
 *         streamReader.setRunOnError(r);
 *
 * There are some more options available:
 *
 *         streamReader.setUrlToAllSensors();
 *
 *         Will read the last few datasets of all known sensors.
 *
 *         streamReader.setNumberOfValidSensorsToRead(i);
 *
 *         Will stop the reader after i urls were read successfully.
 *
 *         streamReader.setNumberOfValidDataSetsToRead(i);
 *
 *         Will read only i data sets per sensor. It reads the data in the order
 *         received and stops. The read data will not always be the latest
 *         dataset.
 *
 *         streamReader.setSensortypes([int array]);
 *
 *         Will limit the result to the sensortypes defined in the array.
 *
 *         streamReader.streamOnlyToDatabase();
 *
 *         This forces the reader to fill the local database instead of
 *         populating a list. This is recommended for very large calls to
 *         prevent "out of memory" errors. In this case, all retrieved data
 *         is immediately written to the local SQLite database and not stored
 *         locally in a list. Please keep in mind that this will likely take
 *         more time than the standard behaviour.
 *
 *         Some more options handle the possibility to display text messages
 *         indicating the progress on the main ui thread.
 *
 */

public class LuftDatenAPIStreamReader implements Runnable{

    static final String JSONSTREAM_IO_ERROR="The JSon stream is unavailable; conn. loss?";
    static final String JSONSTREAM_STATE_ERROR="The JSon stream has errors.";
    static final String JSONSTREAM_ERROR_CAUSE="Reading the JSon stream was not successful.";

    ArrayList<FullSensorDataSet> resultDataList;

    private URL[] urls;
    private int[] sensortypes;

    Context context = null;

    private TextView progressTextView = null;
    private View warning_container_view;
    private Runnable runOnError = null;
    private Runnable runOnSuccess = null;

    private Boolean streamonlytodatabase = false;

    private Boolean read_limited_dataset_number = false;
    private int dataset_number_to_read = 2^31-1;
    private int max_number_of_urls_with_data_to_read = 2^31-1;

    private Boolean widget_context = false;

    private Boolean use_lenient_jsonparser = FeinstaubPreferences.PREF_USE_LENIENT_JSONPARSER_DEFAULT;

    public static final String LUFTDATEN_API_URL_ALL_SENSORS="https://data.sensor.community/static/v1/data.json";

    public void isCalledFromWidget(Boolean b){
        this.widget_context = b;
    }

    public void setUrls(URL[] urls){
        this.urls = urls;
    }

    public void setUrls(String[] sensorids){
        urls = new URL[sensorids.length];
        for (int i=0; i<sensorids.length; i++){
            urls[i] = getSensorAPIURL(sensorids[i]);
        }
    }

    /**
     * Sets the urls to read to the sensorid passed.
     * Convenience method to read only one sensor.
     *
     * @param sensorid
     */

    public void setUrl(String sensorid){
        urls = new URL[1];
        urls[0] = getSensorAPIURL(sensorid);
    }

    /**
     * This sets the urls to read the last few datasets of all sensors that
     * currently present data.
     */

    public void setUrlToAllSensors(){
        urls = new URL[1];
        try {
            urls[0] = new URL(LUFTDATEN_API_URL_ALL_SENSORS);
        } catch (Exception e){
            urls[0] = null;
        }
    }

    /**
     * Set this to stop reading each url after i entries were retrieved successfully.
     *
     * Example:
     * streamReader.setUrls(urls);
     * streamReader.setNumberOfValidDataSetsToRead(1);
     *
     * This will read the urls (e.g. a list of 20 urls) and will only return 1 data
     * set for each url and not more.
     *
     * Please note: the entries are processed in the order retrieved from the API. This
     * means that the first entry will not always be the latest dataset available.
     *
     * @param i number of data sets needed.
     */

    public void setNumberOfValidDataSetsToRead(int i){
        read_limited_dataset_number = true;
        dataset_number_to_read = i;
    }

    /**
     * Set this to stop reading urls after data for i sensors was retrieved successfully.
     * Sensors/Urls that currently do not present data are ignored.
     *
     * Example:
     * streamReader.setUrls(urls);
     * streamReader.setNumberOfValidSensorsToRead(5);
     *
     * This will read the urls (e.g. a list of 20 urls) and will stop as soon as there is
     * data from 5 sensors.
     *
     * @param i number of data sets needed.
     */

    public void setNumberOfValidSensorsToRead(int i){
        max_number_of_urls_with_data_to_read = i;
    }

    /**
     * Sets the arraylist of FullSensorDataSets where the results are put. Use this list in your
     * main thread to receive the results from this class.
     *
     * You can access this list after the processing is complete using setRunOnSuccess.
     *
     * Example:
     *
     * final ArrayList<FullSensorDataSet> myData;
     * final int datasetcount;
     * ...
     * streamReader.setUrls(urls);
     * streamReader.setResultDataList(myData);
     * ...
     * streamReader.setOnSuccess(new Runnable() {
     *      @Override
     *      public void run() {
     *         datasetcount = myData.size();
     *         }
     *      );
     * streamReader.start();
     *
     * @param list
     */

    public void setResultDataList(ArrayList<FullSensorDataSet> list){
        this.resultDataList = list;
    }

    /**
     * Sets the context from where this streamReader was called. It is mandatory to set this
     * before running/starting the streamReader.
     *
     * @param c the context
     */

    public void setContext(Context c){
        this.context = c;
    }

    /**
     * Limits the output to the sensortypes passed.
     *
     * @param sensortypes the sensorTypes to read.
     */

    public void setSensortypes(int[] sensortypes){
        this.sensortypes = sensortypes;
    }

    /**
     * Sets a textview to display standard progress text messages when reading. This
     * may be omitted and/or null.
     *
     * @param tv
     */

    public void setProgressTextView(TextView tv) {
        this.progressTextView = tv;
    }

    /**
     * This is a convenience method for calls from the main thread (application).
     * This is used to pass the view of the "warning" so that things get displayed
     * correctly in the main app.
     *
     * @param v
     */

    public void setWarning_container_view(View v){
        this.warning_container_view = v;
    }

    /**
     * Set this to true to read from the API and to immediately save data to the local
     * SQLite database without filling the local result array.
     *
     * This is the way to go if you read very large data amounts and there is a risk
     * of low memory due to the size of the resulting arraylist.
     */

    public void streamOnlyToDatabase(){
        this.streamonlytodatabase = true;
    }

    /**
     * Defines a runnable that will be launched after the streamReader finished
     * successfully.
     *
     * Use this to launch any action when data was read successfully.
     *
     * @param r
     */

    public void setRunOnSuccess(Runnable r){
        this.runOnSuccess = r;
    }

    /**
     * Defines a runnable that will be launched after the streamReader failed.
     * Failure is defined as one of the following:
     * - there were IOErrors
     * - there ware IllegalStateExeptions
     * - the internet connection failed
     *
     * This usually means the Json data from the api was corrupt and/or the data
     * could not be read.
     *
     * Please note: empty url entries that were passed to the streamReader are
     * ignored and not considered as errors.
     *
     * Use this to launch any recovery action (if neccessary).
     *
     * @param r
     */

    public void setRunOnError(Runnable r){
        this.runOnError = r;
    }

    /**
     * Generates a url from a sensor number to call the API.
     *
     * @param sensor
     * @return url of sensor
     */

    public URL getSensorAPIURL(String sensor){
        final String LUFTDATEN_API_URL="https://data.sensor.community/airrohr/v1/sensor/";
        try {
            URL url = new URL(LUFTDATEN_API_URL+sensor+"/");
            return url;
        } catch (Exception e){
            return null;
        }
    }

    /**
     * Forces the Json parser to be lenient.
     *
     * @param b
     */

    public void useLenientJsonParser(Boolean b){
        this.use_lenient_jsonparser = b;
    }

    /**
     * Determines if a FullSensorDataSet is in the list of desired sensortypes.
     *
     * @param dataSet
     * @return
     *  - true if the sensortype is in the list of desired sensortypes or the list
     *    of sensortypes is empty.
     *  - otherwise returns false
     */

    public Boolean isWantedSensorType(FullSensorDataSet dataSet){
        if (sensortypes == null){
            return true;
        }
        for (int sensortype: sensortypes){
            if (dataSet.getSensorType() == sensortype){
                return true;
            } /* else if (dataSet.getSensorType() == SensorDataSet.UNKNOWN_SENSOR){
                Log.v("Unknown sensor:","ID: "+dataSet.sensorId +" Type: "+dataSet.sensorTypeName);
            } */
        }
        return false;
    }

    /**
     * Determines if the sensortypename is a particualte matter sensor.
     *
     * @param sensortypename
     * @return true if this is a perticulate matter sensor, otherwise false.
     */

    public Boolean isParticulateSensor(String sensortypename){
        for (String s: SensorDataSet.PARTICULATE_SENSOR_TYPES){
            if (sensortypename.equals(s))
                return true;
        }
        return false;
    }

    /**
     * Determines if the sensortypename is a temperature/humidity sensor.
     *
     * @param sensortypename
     * @return true is this is a temperature/humidity sensor, otherwise false.
     */

    public Boolean isTemperatureSensor(String sensortypename){
        for (String s: SensorDataSet.TEMPERATURE_SENSOR_TYPES){
            if (sensortypename.equals(s))
                return true;
        }
        return false;
    }

    /**
     * Convenience method that checks if still more datasets from a specific
     * url have to be read.
     *
     * @param i
     * @return
     */

    public Boolean stillNeedsDatasets(int i){
        if (!read_limited_dataset_number){
            return true;
        }
        if (i>0) {
         return true;
        }
        return false;
    }

    /**
     * Saves a stream of JSon data to the local database.
     *
     * @param reader
     * @return true if any records were saved, otherwise false.
     * @throws IOException
     * @throws IllegalStateException
     */

    public Boolean saveLuftdatenAPIStream(Reader reader) throws IOException, IllegalStateException{
        Boolean result;
        JsonReader jsonReader = new JsonReader(reader);
        jsonReader.setLenient(use_lenient_jsonparser);
        try {
            result = saveLuftdatenAPIArray(jsonReader);
        } catch (IOException e){
            throw new IOException(JSONSTREAM_IO_ERROR, new Throwable(JSONSTREAM_ERROR_CAUSE));
        } catch (IllegalStateException e) {
            throw new IllegalStateException(JSONSTREAM_STATE_ERROR, new Throwable(JSONSTREAM_ERROR_CAUSE));
        } finally {
            try {
                jsonReader.close();
            } catch (Exception e) {
                // nothing to do, as only closing the JsonReader failed here.
            }
        }
        return result;
    }

    /**
     * Reads a stream of JSon data to the local variable.
     * @param reader
     * @return true if any records were read, otherwise false.
     * @throws IOException
     * @throws IllegalStateException
     */

    public Boolean readLuftdatenAPIStream(Reader reader) throws IOException, IllegalStateException{
        Boolean result = false;
        JsonReader jsonReader = new JsonReader(reader);
        jsonReader.setLenient(use_lenient_jsonparser);
        try {
            result = readLuftdatenAPIArray(jsonReader);
        } catch (IOException e){
            throw new IOException(JSONSTREAM_IO_ERROR, new Throwable(JSONSTREAM_ERROR_CAUSE));
        } catch (IllegalStateException e) {
            throw new IllegalStateException(JSONSTREAM_STATE_ERROR, new Throwable(JSONSTREAM_ERROR_CAUSE));
        }
         finally {
            try {
                jsonReader.close();
            } catch (Exception e){
                // nothing to do, as only closing the JsonReader failed here.
            }
        }
        return result;
    }

    /**
     * Processes a stream of Json data. It determines if the result should be stored in the
     * variable passed with
     *
     * streamReader.setResultDataList(myresult);
     *
     * or written directly to the local SQLite database without storing in the variable.
     *
     * @param reader
     * @return true if any data sets were retrieved successfully, otherwise false.
     * @throws IOException
     * @throws IllegalStateException
     */

    public Boolean processLuftdatenAPIStream(Reader reader) throws IOException, IllegalStateException{
        if (streamonlytodatabase){
            try {
                return saveLuftdatenAPIStream(reader);
            } catch (IOException e){
                throw new IOException(JSONSTREAM_IO_ERROR, new Throwable(JSONSTREAM_ERROR_CAUSE));
            } catch (IllegalStateException e) {
                throw new IllegalStateException(JSONSTREAM_STATE_ERROR, new Throwable(JSONSTREAM_ERROR_CAUSE));
            }
        } else {
            try {
                return readLuftdatenAPIStream(reader);
            } catch (IOException e){
                throw new IOException(JSONSTREAM_IO_ERROR, new Throwable(JSONSTREAM_ERROR_CAUSE));
            } catch (IllegalStateException e) {
                throw new IllegalStateException(JSONSTREAM_STATE_ERROR, new Throwable(JSONSTREAM_ERROR_CAUSE));
            }
        }
    }

    /**
     * Reads an array from the Json stream an processes it.
     *
     * @param jsonReader
     * @return true if at least one data set was retrieved successfully, otherwise false.
     * @throws IOException
     * @throws IllegalStateException
     */

    public Boolean readLuftdatenAPIArray(JsonReader jsonReader) throws IOException, IllegalStateException{
        if (resultDataList == null){
            resultDataList = new ArrayList<FullSensorDataSet>();
        }
        int needed_datasets = dataset_number_to_read;
        int added_datasets  = 0;
        FullSensorDataSet dataset;
        try {
            jsonReader.beginArray();
            while (jsonReader.hasNext() && stillNeedsDatasets(needed_datasets)){
                dataset = readLuftdatenAPISensorData(jsonReader);
                if (dataset != null){
                    if ((isWantedSensorType(dataset)) && (dataset.hasData())){
                        resultDataList.add(dataset);
                        needed_datasets = needed_datasets - 1;
                        added_datasets  = added_datasets + 1;
                    }
                }
            }
            jsonReader.endArray();
            if (added_datasets>0){
                return true;
            } else {
                return false;
            }
        } catch (IOException e){
            throw new IOException(JSONSTREAM_IO_ERROR, new Throwable(JSONSTREAM_ERROR_CAUSE));
        } catch (IllegalStateException e) {
            throw new IllegalStateException(JSONSTREAM_STATE_ERROR, new Throwable(JSONSTREAM_ERROR_CAUSE));
        }
    }

    /**
     * Reads FullSensorDataSets from the JSon Array and saves them to the local database.
     * The number of records to read may be limited by setting the "dataset_number_to_read" variable.
     *
     * @param jsonReader
     * @return true if any FullSensorDataSets were read, otherwise false.
     * @throws IOException
     * @throws IllegalStateException
     */

    public Boolean saveLuftdatenAPIArray(JsonReader jsonReader) throws IOException, IllegalStateException{
        FullSensorDataSet dataset;
        CardHandler handler = new CardHandler(context);
        int needed_datasets = dataset_number_to_read;
        int added_datasets  = 0;
        try {
            jsonReader.beginArray();
        while (jsonReader.hasNext() && (stillNeedsDatasets(needed_datasets))){
               try {
                   dataset = readLuftdatenAPISensorData(jsonReader);
                   if (dataset != null) {
                      if ((isWantedSensorType(dataset)) && (dataset.hasData())) {
                          handler.addSensorDataSetIfNew(dataset);
                          needed_datasets = needed_datasets - 1;
                          added_datasets  = added_datasets + 1;
                      }
                   }
               } catch (IOException e){
                   throw new IOException(JSONSTREAM_IO_ERROR, new Throwable(JSONSTREAM_ERROR_CAUSE));
               } catch (IllegalStateException e) {
                   throw new IllegalStateException(JSONSTREAM_STATE_ERROR, new Throwable(JSONSTREAM_ERROR_CAUSE));
               }
            }
            jsonReader.endArray();
            if (added_datasets>0){
                return true;
            } else {
                return false;
            }
        } catch (IOException e){
            throw new IOException(JSONSTREAM_IO_ERROR, new Throwable(JSONSTREAM_ERROR_CAUSE));
        }
    }

    private String nextData(JsonReader jsonReader) throws IOException,IllegalStateException {
        try {
            return jsonReader.nextString();
        } catch (IOException e){
            throw new IOException(JSONSTREAM_IO_ERROR, new Throwable(JSONSTREAM_ERROR_CAUSE));
        } catch (IllegalStateException e) {
            throw new IllegalStateException(JSONSTREAM_STATE_ERROR, new Throwable(JSONSTREAM_ERROR_CAUSE));
        }
    }

    /**
     * Parses a FullSensorDataSet from the Json stream and returns it.
     *
     * @param jsonReader
     * @return the data set. The dataset may be empty if there was no data. Never returns null.
     * @throws IOException
     * @throws IllegalStateException
     */

    public FullSensorDataSet readLuftdatenAPISensorData(JsonReader jsonReader) throws IOException, IllegalStateException{
        FullSensorDataSet dataSet = new FullSensorDataSet();
        ArrayList<String> sensordatavalueid   = new ArrayList<>();
        ArrayList<String> sensordatavaluetype = new ArrayList<>();
        ArrayList<String> sensordatavalue     = new ArrayList<>();
        try {
            jsonReader.beginObject();
            while (jsonReader.hasNext()){
                String main_valuename = jsonReader.nextName();
                if (main_valuename.equals(FullSensorDataSet.SENSORDATAVALUES)){
                    jsonReader.beginArray();
                    int i = 0;
                    while (jsonReader.hasNext()){
                        jsonReader.beginObject();
                        while (jsonReader.hasNext()){
                            String dataset_valuename = jsonReader.nextName();
                            if (dataset_valuename.equals(FullSensorDataSet.SENSORDATAVALUEID)){
                                    sensordatavalueid.add(jsonReader.nextString());
                            } else if (dataset_valuename.equals(FullSensorDataSet.SENSORDATAVALUETYPE)){
                                    sensordatavaluetype.add(jsonReader.nextString());
                            } else if (dataset_valuename.equals(FullSensorDataSet.SENSORDATAVALUE)){
                                    sensordatavalue.add(jsonReader.nextString());
                            } else {
                                // unknown item
                                jsonReader.skipValue();
                            }
                        }
                        jsonReader.endObject();
                    }
                    jsonReader.endArray();
                } else if (main_valuename.equals(FullSensorDataSet.SAMPLINGRATE)){
                        // this value is only known to be "null". This value is
                        // currently ignored and skipped.
                            jsonReader.skipValue();
                } else if (main_valuename.equals(FullSensorDataSet.LOCATION)){
                    jsonReader.beginObject();
                    while (jsonReader.hasNext()){
                        String dataset_valuename = jsonReader.nextName();
                        if (dataset_valuename.equals(FullSensorDataSet.LOCATIONID)){
                                dataSet.locationId = jsonReader.nextString();
                        } else if (dataset_valuename.equals(FullSensorDataSet.ALTITUDE)){
                                dataSet.locationAltitude = jsonReader.nextString();
                        } else if (dataset_valuename.equals(FullSensorDataSet.LONGITUDE)){
                                dataSet.locationLongitude = jsonReader.nextString();
                        } else if (dataset_valuename.equals(FullSensorDataSet.LATITUDE)){
                                dataSet.locationLatitude = jsonReader.nextString();
                        } else if (dataset_valuename.equals(FullSensorDataSet.COUNTRY)){
                                dataSet.locationCountry = jsonReader.nextString();
                        } else {
                            // unknown item
                            jsonReader.skipValue();
                        }
                    }
                    jsonReader.endObject();
                } else if (main_valuename.equals(FullSensorDataSet.DATASETID)){
                        dataSet.dataSetId = jsonReader.nextString();
                } else if (main_valuename.equals(FullSensorDataSet.TIMESTAMP)){
                        dataSet.timestamp = jsonReader.nextString();
                        dataSet.timestamp_UTCmillis = dataSet.getTimestampUTCMillis();
                } else if (main_valuename.equals(FullSensorDataSet.SENSOR)){
                    jsonReader.beginObject();
                    while (jsonReader.hasNext()){
                        String dataset_valuename = jsonReader.nextName();
                        if (dataset_valuename.equals(FullSensorDataSet.SENSORID)){
                                dataSet.sensorId = jsonReader.nextString();
                        } else if (dataset_valuename.equals(FullSensorDataSet.SENSORPIN)){
                                dataSet.sensorPin = jsonReader.nextString();
                        } else if (dataset_valuename.equals(FullSensorDataSet.SENSORTYPE)){
                            jsonReader.beginObject();
                            while (jsonReader.hasNext()){
                                String dataset_valuename2 = jsonReader.nextName();
                                if (dataset_valuename2.equals(FullSensorDataSet.SENSORTYPENAME)){
                                        dataSet.sensorTypeName = jsonReader.nextString();
                                } else if (dataset_valuename2.equals(FullSensorDataSet.SENSORTYPEID)){
                                        dataSet.sensorTypeId = jsonReader.nextString();
                                } else if (dataset_valuename2.equals(FullSensorDataSet.SENSORMANUFACTURER)){
                                        dataSet.manufacturer = jsonReader.nextString();
                                } else {
                                    // unknown value
                                    jsonReader.skipValue();
                                }
                            }
                            jsonReader.endObject();
                        } else {
                            // unknown value
                            jsonReader.skipValue();
                        }
                    }
                    jsonReader.endObject();
                } else {
                    // unknown value
                    jsonReader.skipValue();
                }
            }
            jsonReader.endObject();
            // dataSet.fixSensorDataValueOrder();
            /*
             * The following code inserts the sensor values in the proper order into the dataset.
             * Unknown sensorDataSetTypes are ignored.
            */
            for (int index=0; index<sensordatavalueid.size(); index++){
                int position = dataSet.getSensorValueTypeIndex(sensordatavaluetype.get(index));
                if ((position>=0) && (position<=1)){
                    dataSet.sensordataValue[position] = sensordatavalue.get(index);
                    dataSet.sensordataValueType[position] = sensordatavaluetype.get(index);
                    dataSet.sensordataValueId[position] = sensordatavalueid.get(index);
                }
            }
            return dataSet;
        } catch (IOException e){
            throw new IOException(JSONSTREAM_IO_ERROR, new Throwable(JSONSTREAM_ERROR_CAUSE));
        } catch (IllegalStateException e) {
            throw new IllegalStateException(JSONSTREAM_STATE_ERROR, new Throwable(JSONSTREAM_ERROR_CAUSE));
        }
    }

    @Override
    public void run() {
        Boolean haserrors = false;
        showgetLocationListFromAPIprogress(context.getResources().getString(R.string.main_location_data_progress1));
        int urls_with_data = 0;
        for (int i = 0; i < urls.length; i++) {
            if (urls[i] != null) {
                try {
                    HttpsURLConnection httpsURLConnection = (HttpsURLConnection) urls[i].openConnection();
                    InputStream inputStream = httpsURLConnection.getInputStream();
                    BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream));
                    showgetLocationListFromAPIprogress(context.getResources().getString(R.string.main_location_data_progress2));
                    try {
                        Boolean result = processLuftdatenAPIStream(bufferedReader);
                        if (result) {
                            urls_with_data = urls_with_data + 1;
                        }
                        //isp.close();
                        bufferedReader.close();
                        if (urls_with_data>=max_number_of_urls_with_data_to_read)
                            break;
                    } catch (IOException|IllegalStateException e){
                        haserrors = true;
                    }
                } catch (Exception e) {
                    /*
                     * This means the internet connection failed.
                     */
                    haserrors = true;
                }
            }
        }
        if (warning_container_view != null){
            removeWarningViews();
        }
        if (haserrors) {
            if (runOnError != null){
                if (widget_context){
                    Thread thread = new Thread(runOnError);
                    thread.start();
                    // runOnError.run();
                } else {
                    Activity a = (Activity) context;
                    a.runOnUiThread(runOnError);
                }
            }
        } else {
            if (runOnSuccess != null){
                if (widget_context){
                    runOnSuccess.run();
                } else {
                    Activity a = (Activity) context;
                    a.runOnUiThread(runOnSuccess);
                }
            }
        }
    }

    private void updateProgress(int i, int e){
        if (progressTextView != null) {
            progressTextView.setVisibility(View.VISIBLE);
            progressTextView.setText(context.getResources().getString(R.string.main_location_data_progress) + " " + String.valueOf(i) + " " + context.getResources().getString(R.string.main_location_data_errors) + " "+e);
            progressTextView.invalidate();
        }
    }

    private void updateProgress(String s){
        if (progressTextView != null) {
            progressTextView.setVisibility(View.VISIBLE);
            progressTextView.setText(s);
        }
    }

    public void showgetLocationListFromAPIprogress(int i, int e){
        if (progressTextView != null){
            final int progress = i;
            final int errors = e;
            Activity a = (Activity) context;
            a.runOnUiThread(new Runnable(){
                public void run(){
                    updateProgress(progress,errors);
                }
            });
        }
    }

    private void showgetLocationListFromAPIprogress(String s){
        if (progressTextView != null){
            final String progress = s;
            Activity a = (Activity) context;
            a.runOnUiThread(new Runnable(){
                public void run(){
                    updateProgress(progress);
                }
            });
        }
    }

    private void removeWarningViews(){
        final Activity a = (Activity) context;
        a.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if ((warning_container_view != null)){
                    warning_container_view.setVisibility(View.GONE);
                    warning_container_view.invalidate();
                }
            }
        });
    }

    public ArrayList<FullSensorDataSet> getResultArrayList(){
        return this.resultDataList;
    }

}
