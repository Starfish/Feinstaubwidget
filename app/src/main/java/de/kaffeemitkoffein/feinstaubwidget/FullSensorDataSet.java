/*

 This file is part of FeinstaubWidget.

 Copyright (c) 2018, 2019, 2020, 2021, 2022, 2023, 2024 Pawel Dube

 FeinstaubWidget is free software: you can redistribute it and/or modify it
 under the terms of the GNU General Public License as published by the
 Free Software Foundation, either version 3 of the License, or (at
 your option) any later version.

 FeinstaubWidget is distributed in the hope that it will be useful, but
 WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with FeinstaubWidget. If not, see <http://www.gnu.org/licenses/>.

 */

package de.kaffeemitkoffein.feinstaubwidget;

import android.os.Parcel;
import android.os.Parcelable;

public class FullSensorDataSet extends SensorDataSet implements Parcelable{

    public static final String DATASETID = "id";
    public static final String SAMPLINGRATE = "sampling_rate";
    public static final String TIMESTAMP = "timestamp";
    public static final String LOCATION = "location";
    public static final String LOCATIONID = "id";
    public static final String LATITUDE = "latitude";
    public static final String LONGITUDE = "longitude";
    public static final String ALTITUDE = "altitude";
    public static final String COUNTRY = "country";
    public static final String SENSOR = "sensor";
    public static final String SENSORID = "id";
    public static final String SENSORPIN = "pin";
    public static final String SENSORTYPE = "sensor_type";
    public static final String SENSORTYPEID = "id";
    public static final String SENSORTYPENAME = "name";
    public static final String SENSORMANUFACTURER = "manufacturer";
    public static final String SENSORDATAVALUES = "sensordatavalues";
    public static final String SENSORDATAVALUEID = "id";
    public static final String SENSORDATAVALUE = "value";
    public static final String SENSORDATAVALUETYPE = "value_type";

    String dataSetId = null;
    String samplingRate = null;
    String locationId = null;
    String sensorPin = null;
    String sensorTypeId = null;
    String[] sensordataValueId = {null, null};
    String[] sensordataValueType = {null, null};
    String manufacturer = null;

    public FullSensorDataSet(){
    }

    public Boolean fixSensorDataValueOrder(){
        if ((sensordataValueType[0] == null) || (sensordataValueType[1] == null))
            return false;
        if (sensordataValueType[0].equals(TEMPERATURE_SENSOR_VALUETYPES[1]) || sensordataValueType[0].equals(PARTICULATE_SENSOR_VALUETYPES[1])){
            String svt0  = sensordataValueType[0];
            String sv0   = sensordataValue[0];
            String svid0 = sensordataValueId[0];
            sensordataValueType[0] = sensordataValueType[1];
            sensordataValue[0] = sensordataValue[1];
            sensordataValueId[0] = sensordataValueId[1];
            sensordataValueType[1] = svt0;
            sensordataValue[1] = sv0;
            sensordataValueId[1] = svid0;
            return true;
        }
        return false;
    }

    public SensorDataSet toSensorDataSet(){
        this.fixSensorDataValueOrder();
        SensorDataSet dataSet = new SensorDataSet();
        dataSet.sensordataValue[0] = this.sensordataValue[0];
        dataSet.sensordataValue[1] = this.sensordataValue[1];
        dataSet.sensorTypeName = this.sensorTypeName;
        dataSet.timestamp = this.timestamp;
        dataSet.sensorId = this.sensorId;
        dataSet.locationAltitude = this.locationAltitude;
        dataSet.locationLongitude = this.locationLongitude;
        dataSet.locationCountry = this.locationCountry;
        dataSet.locationLatitude = this.locationLatitude;
        dataSet.timestamp_UTCmillis = this.timestamp_UTCmillis;
        dataSet.database_id = this.database_id;
        return dataSet;
    }

    public String toString(){
        String l = "|";
        String s = ": ";
        String h1 = "*";
        String sensordatavalues = "";
        for (int i=0; i<sensordataValue.length; i++){
            sensordatavalues = sensordatavalues +
                SENSORDATAVALUEID + s + this.sensordataValueId[i] + l +
                SENSORDATAVALUETYPE + s + this.sensordataValueType[i] + l +
                SENSORDATAVALUE + s + this.sensordataValue[i] +l;
        }
        return h1 + SENSOR + l +
               SENSORID + s + this.sensorId + l +
               SENSORPIN + s + this.sensorPin + l +
                h1 + SENSORTYPE + l +
                SENSORTYPENAME + s + this.sensorTypeName + l +
                SENSORTYPEID + s + this.sensorTypeId + l +
                SENSORMANUFACTURER + s + this.manufacturer + l +
               SAMPLINGRATE + s + this.samplingRate + l +
               DATASETID + s + this.dataSetId + l +
               TIMESTAMP + s + this.timestamp + " (UTCmillis:"+this.timestamp_UTCmillis+")" + l +
                h1 + LOCATION + l +
                LOCATIONID + s + this.locationId + l +
                ALTITUDE + s + this.locationAltitude + l +
                LATITUDE + s + this.locationLatitude + l +
                LONGITUDE + s + this.locationLatitude + l +
                COUNTRY + s + this.locationCountry + l +
                h1 + SENSORDATAVALUES + l +
                sensordatavalues;
    }

    @Override
    public void writeToParcel(Parcel parcel, int flags){
        parcel.writeStringArray(sensordataValue);
        parcel.writeString(sensorTypeName);
        parcel.writeString(timestamp);
        parcel.writeString(sensorId);
        parcel.writeString(locationAltitude);
        parcel.writeString(locationLongitude);
        parcel.writeString(locationCountry);
        parcel.writeString(locationLatitude);
        if (timestamp_UTCmillis==null){
            parcel.writeLong(-1);
        } else {
            parcel.writeLong(timestamp_UTCmillis);
        }
        if (database_id == null) {
            parcel.writeInt(-1);
        } else {
            parcel.writeInt(database_id);
        }

        parcel.writeString(dataSetId);
        if (samplingRate==null){
            parcel.writeString("");
        } else {
            parcel.writeString(samplingRate);
        }
        parcel.writeString(locationId);
        parcel.writeString(sensorPin);
        parcel.writeString(sensorTypeId);
        parcel.writeStringArray(sensordataValueId);
        parcel.writeStringArray(sensordataValueType);
        parcel.writeString(manufacturer);
    }

    @Override
    public int describeContents(){
        return 0;
    }

    public FullSensorDataSet(Parcel parcel){
        sensordataValue = parcel.createStringArray();
        sensorTypeName = parcel.readString();
        timestamp = parcel.readString();
        sensorId = parcel.readString();
        locationAltitude = parcel.readString();
        locationLongitude = parcel.readString();
        locationCountry = parcel.readString();
        locationLatitude = parcel.readString();
        timestamp_UTCmillis = parcel.readLong();
        if (timestamp_UTCmillis == -1){
            timestamp_UTCmillis = null;
        }
        database_id = parcel.readInt();
        if (database_id==-1){
            database_id = null;
        }

        dataSetId = parcel.readString();
        samplingRate = parcel.readString();
        if (samplingRate==""){
            samplingRate = null;
        }
        locationId = parcel.readString();
        sensorPin = parcel.readString();
        sensorTypeId = parcel.readString();
        sensordataValueId = parcel.createStringArray();
        sensordataValueType = parcel.createStringArray();
        manufacturer = parcel.readString();
    }

    public static final Parcelable.Creator CREATOR = new Parcelable.Creator(){
        public FullSensorDataSet createFromParcel(Parcel parcel){
            return new FullSensorDataSet(parcel);
        }
        public FullSensorDataSet[] newArray(int i){
            return new FullSensorDataSet[i];
        }
    };

}
