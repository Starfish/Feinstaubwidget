/**

 This file is part of FeinstaubWidget.

 Copyright (c) 2018, 2019, 2020, 2021, 2022, 2023, 2024 Pawel Dube

 FeinstaubWidget is free software: you can redistribute it and/or modify it
 under the terms of the GNU General Public License as published by the
 Free Software Foundation, either version 3 of the License, or (at
 your option) any later version.

 FeinstaubWidget is distributed in the hope that it will be useful, but
 WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with FeinstaubWidget. If not, see <http://www.gnu.org/licenses/>.

 */

package de.kaffeemitkoffein.feinstaubwidget;

import android.Manifest;
import android.annotation.TargetApi;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.ActivityInfo;
import android.content.pm.PackageManager;
import android.graphics.Canvas;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.location.Location;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.view.*;
import android.widget.*;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.*;
import android.graphics.Bitmap;
import java.io.File;
import java.util.concurrent.Executor;
import java.util.concurrent.Executors;

public class MainActivity extends Activity {

    private Context context;
    private Executor executor;
    private LinearLayout mainContainer;

    int fetchDisplayPosition;
    int fetchIDNumber;
    ArrayList<String> sensorIDs;
    ArrayList<SensorDataSet> sensorDataSets;
    CardHandler cardHandler;

    /**
     * Field names to interact with the StaubInfo activity that displays a custom text
     * in a scrollable view.
     */

    private static final String DATA_TITLE="DATA_TITLE";
    private static final String DATA_TEXTRESOURCE="DATA_TEXTRESOURCE";
    private static final String DATA_BUTTONTEXT="DATA_BUTTONTEXT";

    /**
     * Bundle-id to identify the sensor number parameter.
     * Used to handle over a sensor number to the SensorInfo activity.
     */

    public static final String DATA_SENSORNUMBER="DATA_SENSORNUMBER";

    /**
     * This constant defines the size of the database by time. Entries older than that
     * are deleted.
     */

    private static final int DATABASE_TIMESPAN = 86400000;

    /**
     * This defines the time to display in the graphs.
     */

    private static final int GRAPH_TIMESPAN = DATABASE_TIMESPAN;

    /**
     * Constants for the visible views (mobile sensors or local sensors).
     */

    static final int CARDSELECTOR_MOBILE=0;
    static final int CARDSELECTOR_LOCAL=1;

    /**
     * Local variable holding the visible main view type (mobile sensors or local sensor).
     */

    private int visible_cardselector = CARDSELECTOR_MOBILE;

    private Boolean dataselectorposition = FeinstaubPreferences.DATASWITCHPOSITION_PARTICULATEMATTER;

    /**
     * Constant to identify the permission granted / not granted callbacks.
     */

    private static final int PERMISSION_CALLBACK_LOCATION = 97;

    /**
     * String with the last *database* update time of the mobile view. This indicates the
     * last time when the API was queried.
     */

    private String table_lastupdatetext;

    /**
     * String with the last *location* update time. Please note that this time may be quite
     * old, e.g. when the device is not able to get a current location and/or the location
     * services have been turned off.
     */

    private String table_lastlocationtime;

    /**
     * A listener to listen for preference changes. Needs to be defined here
     * to prevent garbage collection by the OS.
     */

    private static SharedPreferences.OnSharedPreferenceChangeListener preferenceListener = null;

    /**
     * The local preferences.
     */

    private FeinstaubPreferences preferences;

    /**
     * View variables.
     */

    private ImageView graph1;
    private ImageView graph2;

    private Dialog introDialog;
    private AlertDialog shareHint;

    /**
     * Current location.
     */

    private FeinstaubLocationManager lm;
    private FeinstaubLocationListener loc_listener;
    private Location localLastKnownLocation;

    private Uri sharedFileUri;

    /**
     * This local variable is always set to true when the list of mobile sensors is
     * updated. This is necessary to block the luftObserver listener from updating
     * all the views because the SQL database has changed.
     *
     * This is mainly done for performance increase, as the views would be updated
     * too often.
     *
     * The listener is blocked in two cases:
     *  a) at app launch between onResume and onCreate, because onCreate already
     *     handles view updates.
     *  b) during the update of the list of next sensors from the API, as the
     *     getCloseDataSet class handles view updates by itself.
     */

    private Boolean updateCloseSensors_is_running = false;

    /**
     * This variable indicates if an update of the known sensors list is
     * currently running.
     */

    private Boolean readingAllLocationsList_is_running = false;

    /**
     * Local convenience variables that hold the view IDs of the table showing
     * the list of next close sensors.
     */

    private int[] locationtable_sensor_id = new int[6];
    private int[] locationtable_direction_id = new int[6];
    private int[] locationtable_distance_id = new int[6];
    private int[] locationtable_value1_id = new int[6];
    private int[] locationtable_value2_id = new int[6];
    private int[] locationtable_row_id = new int[6];

    /**
     * Local convenience sub to populate the arrays listed above.
     */

    private void setViewIdArrays() {
        locationtable_sensor_id[0] = R.id.main_nextsensors_id_1;
        locationtable_sensor_id[1] = R.id.main_nextsensors_id_2;
        locationtable_sensor_id[2] = R.id.main_nextsensors_id_3;
        locationtable_sensor_id[3] = R.id.main_nextsensors_id_4;
        locationtable_sensor_id[4] = R.id.main_nextsensors_id_5;
        locationtable_sensor_id[5] = R.id.main_nextsensors_id_6;

        locationtable_direction_id[0] = R.id.main_nextsensors_direction_1;
        locationtable_direction_id[1] = R.id.main_nextsensors_direction_2;
        locationtable_direction_id[2] = R.id.main_nextsensors_direction_3;
        locationtable_direction_id[3] = R.id.main_nextsensors_direction_4;
        locationtable_direction_id[4] = R.id.main_nextsensors_direction_5;
        locationtable_direction_id[5] = R.id.main_nextsensors_direction_6;

        locationtable_distance_id[0] = R.id.main_nextsensors_distance_1;
        locationtable_distance_id[1] = R.id.main_nextsensors_distance_2;
        locationtable_distance_id[2] = R.id.main_nextsensors_distance_3;
        locationtable_distance_id[3] = R.id.main_nextsensors_distance_4;
        locationtable_distance_id[4] = R.id.main_nextsensors_distance_5;
        locationtable_distance_id[5] = R.id.main_nextsensors_distance_6;

        locationtable_value1_id[0] = R.id.main_nextsensors_value1_1;
        locationtable_value1_id[1] = R.id.main_nextsensors_value1_2;
        locationtable_value1_id[2] = R.id.main_nextsensors_value1_3;
        locationtable_value1_id[3] = R.id.main_nextsensors_value1_4;
        locationtable_value1_id[4] = R.id.main_nextsensors_value1_5;
        locationtable_value1_id[5] = R.id.main_nextsensors_value1_6;

        locationtable_value2_id[0] = R.id.main_nextsensors_value2_1;
        locationtable_value2_id[1] = R.id.main_nextsensors_value2_2;
        locationtable_value2_id[2] = R.id.main_nextsensors_value2_3;
        locationtable_value2_id[3] = R.id.main_nextsensors_value2_4;
        locationtable_value2_id[4] = R.id.main_nextsensors_value2_5;
        locationtable_value2_id[5] = R.id.main_nextsensors_value2_6;

        locationtable_row_id[0] = R.id.main_closesensorrow_1;
        locationtable_row_id[1] = R.id.main_closesensorrow_2;
        locationtable_row_id[2] = R.id.main_closesensorrow_3;
        locationtable_row_id[3] = R.id.main_closesensorrow_4;
        locationtable_row_id[4] = R.id.main_closesensorrow_5;
        locationtable_row_id[5] = R.id.main_closesensorrow_6;
    }

    /**
     * Local variable if the local list of sensors should be reloaded. Necessary
     * for the interaction with the perferences screen.
     */

    private Boolean prompt_for_local_list_download = false;

    /**
     * Local variable if the local list of sensors should be updated. Necessary
     * for the interaction with the perferences screen.
     */

    private Boolean do_local_list_update = false;

    @Override
    protected void onCreate(final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        executor = Executors.newSingleThreadExecutor();
        View mainView = getWindow().getDecorView().getRootView();
        /*
         * Read the app defaults and set the context variable.
         */
        context = this;
        readPreferences();
        mainContainer = (LinearLayout) findViewById(R.id.main_container);
        /*
         * Inits the main selector between "mobile" and "local" and adds the respective
         * listeners to switch the views.
         */
        TextView mainselector_mobile = (TextView) findViewById(R.id.main_cardselector_text1);
        mainselector_mobile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                switchMainView(CARDSELECTOR_MOBILE);
            }
        });
        final TextView mainselector_local = (TextView) findViewById(R.id.main_cardselector_text2);
        mainselector_local.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                switchMainView(CARDSELECTOR_LOCAL);
            }
        });
        /*
         * This is the switch to change the mobile view between fine particulate matter display
         * and temperature / humidity.
         */
        final Switch mode_switch = (Switch) findViewById(R.id.main_nextsensors_dataswitch);
        mode_switch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if (!updateCloseSensors_is_running) {
                    // it is safe to switch as data is currently not updated from the api
                    updateSensorData();
                } else {
                    // the update is blocked and the switch is toggled back to it's last
                    // position because data is currently updated from the api. Allowing this
                    // would cause a running condition, because we would run at least
                    // two update processes in parallel.
                    mode_switch.setChecked(!mode_switch.isChecked());
                }
                storeDataSelectorPosition(mode_switch.isChecked());
                dataselectorposition = mode_switch.isChecked();
            }
        });
        /*
         * This catches a call from PreferencesActivity (call is defined in preferences.xml)
         * to re-load the database. This is triggered by resetting lastlocationupdate
         * to zero.
         *
         * As a result of this, selecting to re-load the database in the preferences screen
         * directly opens the dialog to download the database.
         *
         * This also requires to set the main selector to "mobile", as otherwise this view would
         * not be directly visible to the user.
         */

        Bundle extras = getIntent().getExtras();
        prompt_for_local_list_download = false;
        do_local_list_update = false;
        if (extras!=null){
            String action = extras.getString("ACTION");
            if (action != null){
                if (action.equals("LOADSENSORLOCATIONS")){
                    resetSensorLocationsLoadedFlag();
                    prompt_for_local_list_download = true;
                }
                if (action.equals("UPDATESENSORLOCATIONS")){
                    do_local_list_update = true;
                }
            }
        }
        mainView.post(new Runnable() {
            @Override
            public void run() {
                setViewIdArrays();
                visible_cardselector = getCardSelectorValue();
                /*
                 * The main view is restored to the last known position (mobile or local),
                 * UNLESS the list with the sensor locations has to be reloaded the user
                 * is prompted to download it. In this case, the main view is
                 * set to CARDSELECTOR_MOBILE.
                 */
                if ((!prompt_for_local_list_download) && (!do_local_list_update)){
                    switchMainView(visible_cardselector);
                } else {
                    switchMainView(CARDSELECTOR_MOBILE);
                }
                /*
                 * Read the dataselector position and assign it to the switch.
                 */
                dataselectorposition = getDataSelectorPosition();
                Switch dataswitch = (Switch) findViewById(R.id.main_nextsensors_dataswitch);
                if (dataswitch!=null){
                    dataswitch.setChecked(dataselectorposition);
                }
                /*
                 * The user is prompted to download the sensor locations if necessary.
                 */
                if (preferences.lastlocationlistupdatetime == 0){
                    LinearLayout initial_location_info = (LinearLayout) findViewById(R.id.main_location_init_dialog_container);
                    initial_location_info.setVisibility(LinearLayout.VISIBLE);
                    initial_location_info.invalidate();
                    Button initial_location_loadbutton = (Button) findViewById(R.id.location_init_dialog_download_button);
                    initial_location_loadbutton.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            updateSensorLocationsList(false,true);
                            showDownloadWarning();
                        }
                    });
                }
                if (do_local_list_update){
                    updateSensorLocationsList(false,false);
                    showDownloadWarning();
                }
                    /*
                    /*
                     * Garbage-collect old sensor data.
                     */
                cleanDatabase();
                /*
                 * Add the listeners for the sensor info.
                 */
                graph1.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        Intent i = new Intent(context, SensorInfo.class);
                        i.putExtra(DATA_SENSORNUMBER, preferences.sensor1_number);
                        startActivity(i);
                    }
                });
                graph2.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        Intent i = new Intent(context, SensorInfo.class);
                        i.putExtra(DATA_SENSORNUMBER, preferences.sensor2_number);
                        startActivity(i);
                    }
                });
                /*
                 * release blocked updates.
                 */
                releaseUpdateBlocker();
                /*
                 * We ask for the location permission, but only if:
                 * - the mobile view is visible,
                 * - the user has not been redirected here from the settings to update
                 */
                if ((visible_cardselector==CARDSELECTOR_MOBILE) && (!prompt_for_local_list_download) && (!do_local_list_update)) {

                    hasLocationPermission(true);
                }
            }
        });
        refreshWidgets(context,REPFRESH_ALL_WIDGETS,localLastKnownLocation,sensorDataSets);
    }

    @Override
    protected void onPause(){
        preferences.sharedPreferences.unregisterOnSharedPreferenceChangeListener(preferenceListener);
        /*
         * Store that currently visible main menu (mobile or local).
         */
        storeCardSelectorValue(visible_cardselector);
        /*
         * If any info dialog is open, close it.
         */
        if (introDialog != null){
            if (introDialog.isShowing())
                introDialog.dismiss();
        }
        if (shareHint != null){
            if (shareHint.isShowing()){
                shareHint.dismiss();
            }
        }
        super.onPause();
    }

    @Override
    protected void onResume(){
        super.onResume();
        readPreferences();
        /*
         * Restore the last known visible main menu (mobile or local).
         */
        visible_cardselector = getCardSelectorValue();
        /*
         * Restore data switch position.
         */
        dataselectorposition = getDataSelectorPosition();
        Switch dataswitch = (Switch) findViewById(R.id.main_nextsensors_dataswitch);
        if (dataswitch != null){
            dataswitch.setChecked(dataselectorposition);
        }
        /*
         * Sets up a listener to listen for preference changes. This ensures that
         * the displays (and widgets) get updated, when:
         *  a) the database of sensors was downloaded successfully
         *  b) the local sensors were changed
         *  c) GPS settings were changed
         *  d) display settings were changed
         *
         *  Note: preferenceListener needs to be global to prevent garbage-
         *  collection of this listener.
         */
        registerPreferenceListener();
        updateDataDisplays();
        /*
         * Finally, update the mobile view with data. Try first, if local data
         * satisfies the needs, load new data otherwise.
         */

        //getLocalSensorDataFromAPI();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    /**
     * Some field names to parcel the currently displayed data.
     */

    public final static String NEXTSENSORS_DATA = "NEXTSENSORS_DATA";
    public final static String NEXTSENSORS_LOCATION = "NEXTSENSORS_LOCATION";
    public final static String LASTTABLEUPDATE = "LASTTABLEUPDATE";
    public final static String LASTLOCUPDATE = "LASTLOCUPDATE";
    public final static String TABLEUPDATERUNNING = "TABLEUPDATERUNNING";
    public final static String LOCALLASTKNOWNLOCATION = "LOCALLASTKNOWNLOCATION";

    @Override
    public void onSaveInstanceState(Bundle savedInstanceState){
        super.onSaveInstanceState(savedInstanceState);
        /*
         * Parcel the currently displayed/known sensor data to restore it during
         * onCreate. This is mainly done to speed up the UI display when the
         * screen is rotated.
         */
        savedInstanceState.putParcelableArrayList(NEXTSENSORS_DATA,sensorDataSets);
        //savedInstanceState.putParcelableArrayList(NEXTSENSORS_LOCATION,nextSensors_location);
        TextView v = (TextView) findViewById(R.id.main_nextsensors_lastupdate_text);
        if (v!=null) {
            savedInstanceState.putString(LASTTABLEUPDATE, String.valueOf(v.getText()));
        }
        TextView x = (TextView) findViewById(R.id.main_nextsensors_devicelocation_text);
        if (x!=null) {
            savedInstanceState.putString(LASTLOCUPDATE, String.valueOf(x.getText()));
        }
        savedInstanceState.putBoolean(TABLEUPDATERUNNING,readingAllLocationsList_is_running);
        savedInstanceState.putParcelable(LOCALLASTKNOWNLOCATION,localLastKnownLocation);
    }

    private void restoreLocalVariablesFromInstanceState(Bundle restoredInstanceState){
        sensorDataSets                     = restoredInstanceState.getParcelableArrayList(NEXTSENSORS_DATA);
        table_lastupdatetext               = restoredInstanceState.getString(LASTTABLEUPDATE);
        table_lastlocationtime             = restoredInstanceState.getString(LASTLOCUPDATE);
        readingAllLocationsList_is_running = restoredInstanceState.getBoolean(TABLEUPDATERUNNING);
        localLastKnownLocation             = restoredInstanceState.getParcelable(LOCALLASTKNOWNLOCATION);
    }


    @Override
    public void onRestoreInstanceState(Bundle restoredInstanceState){
        super.onRestoreInstanceState(restoredInstanceState);
        /*
         * Retrieve the last displayed/known sensor data to restore the displays. This
         * is mainly done to speed up the UI display when the screen is roated.
         */
        restoreLocalVariablesFromInstanceState(restoredInstanceState);
        View mainView = getWindow().getDecorView().getRootView();
        mainView.post(new Runnable() {
            @Override
            public void run() {
                updateCloseSensorsMap(sensorDataSets);
            }
        });
    }

    public void registerPreferenceListener(){
        if (preferenceListener==null){
            preferenceListener = new SharedPreferences.OnSharedPreferenceChangeListener(){
                @Override
                public void onSharedPreferenceChanged(SharedPreferences sharedPreferences, String s) {
                    readPreferences();
                    updateDataDisplays();
                    registerToLocationUpdates();
                }
            };
            preferences.sharedPreferences.registerOnSharedPreferenceChangeListener(preferenceListener);
        }
    }

    /**
     * Checks for location permissions. Takes into account the different handling of
     * permissions after API 23, where permissions have to be granted at runtime.
     *
     * Prior to API 23, permissions are granted during app install. So for devices
     * running API 23 or earlier, the app can assume that all necessary permissions
     * are granted.
     *
     * @return
     */

    private boolean hasLocationPermission(Boolean request){
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.M) {
            // handle runtime permissions only if android is >= Marshmellow api 23
            if (this.checkSelfPermission(Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED)
            {
                if (request){
                    requestPermissions(new String[]{Manifest.permission.ACCESS_FINE_LOCATION},PERMISSION_CALLBACK_LOCATION);
                    // at this moment, we request the permission and leave with a negative result. The user needs to try saving again after permission was granted.
                }
                // permission not granted
                return false;
            }
            else
            {
                // permission is granted, ok
                return true;
            }
        }
        else
        {
            // before api 23, permissions are always granted, so everything is ok
            return true;
        }
    }

    /**
     * Callback to catch the granted or denied location permission.
     *
     * @param permRequestCode
     * @param perms
     * @param grantRes
     */

    @TargetApi(Build.VERSION_CODES.M)
    @Override
    public void onRequestPermissionsResult(int permRequestCode, String perms[], int[] grantRes){
        Boolean hasLocationPermission = false;
        for (int i=0; i<grantRes.length; i++){
            if ((perms[i].equals(Manifest.permission.ACCESS_FINE_LOCATION)) && (grantRes[i]==PackageManager.PERMISSION_GRANTED)){
                hasLocationPermission = true;
            }
        }
        if (permRequestCode == PERMISSION_CALLBACK_LOCATION){
            if (hasLocationPermission){
                //
                // Define the actions once permissions are granted.
                //
                registerToLocationUpdates();
            } else {
                if (shouldShowRequestPermissionRationale(Manifest.permission.ACCESS_FINE_LOCATION)){
                    showPermissionsRationale();
                }
            }
        }
    }

    /**
     * Shows an explanation why the location permission is needed if it was
     * not granted by the user.
     */

    private void showPermissionsRationale(){
        final Dialog dialog = new Dialog(this);
        dialog.setContentView(R.layout.dialogpermrationale);
        dialog.setTitle(R.string.permissions_title);
        dialog.setCancelable(true);
        TextView tv = (TextView) dialog.findViewById(R.id.permrationale_text);
        tv.setText(getResources().getString(R.string.permissions_rationale_1)+
                System.lineSeparator()+
                getResources().getString(R.string.permissions_rationale_2)+
                System.lineSeparator()+
                getResources().getString(R.string.permissions_rationale_3)+
                System.lineSeparator()+
                getResources().getString(R.string.permissions_rationale_4)+
                System.lineSeparator()+
                getResources().getString(R.string.permissions_rationale_5)+
                System.lineSeparator()+
                getResources().getString(R.string.permissions_rationale_6)+
                System.lineSeparator()+
                getResources().getString(R.string.permissions_rationale_7)+
                System.lineSeparator()+
                getResources().getString(R.string.permissions_rationale_8)+
                System.lineSeparator());
        Button permok_button = (Button) dialog.findViewById(R.id.permok_button);
        permok_button.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v){
                dialog.dismiss();
            }
        });
        dialog.show();
    }

    /**
     * Populates the action bar.
     *
     * @param menu
     * @return
     */

    @Override
    public boolean onCreateOptionsMenu(Menu menu){
        MenuInflater mi = getMenuInflater();
        mi.inflate(R.menu.mainactivity,menu);
        return super.onCreateOptionsMenu(menu);
    }

    /**
     * Defines the actions for the selected menu items in the action bar.
     *
     * @param mi
     * @return
     */

    @Override
    public boolean onOptionsItemSelected(MenuItem mi){
        int item_id = mi.getItemId();
        if (item_id == R.id.menu_settings) {
            Intent i = new Intent(this,PreferencesActivity.class);
            startActivity(i);
            return true;
        }
        if (item_id == R.id.menu_refresh) {
            //updateSensorDataFromAPI();
            updateSensorData();
            return true;
        }
        if (item_id == R.id.menu_share) {
            shareCurrentVisibleData(ACTION_SHARE);
        }
        if (item_id==R.id.menu_licence) {
            Intent i = new Intent(this, StaubInfo.class);
            i.putExtra(DATA_TITLE, getResources().getString(R.string.license_title));
            i.putExtra(DATA_TEXTRESOURCE, "license");
            i.putExtra(DATA_BUTTONTEXT,getResources().getString(R.string.license_button_text));
            startActivity(i);
            return true;
        }
        if (item_id == R.id.menu_about) {
            showIntroDialog();
            return true;
        }
        if (item_id==R.id.menu_help) {
            Intent i = new Intent(this, StaubInfo.class);
            i.putExtra(DATA_TITLE, getResources().getString(R.string.help_title));
            i.putExtra(DATA_TEXTRESOURCE, "help");
            i.putExtra(DATA_BUTTONTEXT,getResources().getString(R.string.license_button_text));
            startActivity(i);
            return true;
        }
        return super.onOptionsItemSelected(mi);
    }

    /**
     * Switches between the two main views: mobile and local.
     *
     * @param i
     */

    private void switchMainView(int i){
        final LinearLayout mobilecontainer = (LinearLayout) findViewById(R.id.main_mobile_container);
        final LinearLayout localcontainer = (LinearLayout) findViewById(R.id.main_local_container);
        final View mobile_line = (View) findViewById(R.id.main_cardselector_line1);
        final View local_line = (View) findViewById(R.id.main_cardselector_line2);
        if (i==CARDSELECTOR_MOBILE){
            this.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    mobilecontainer.setVisibility(View.VISIBLE);
                    mobilecontainer.invalidate();
                    localcontainer.setVisibility(View.GONE);
                    localcontainer.invalidate();
                    mobile_line.setVisibility(View.VISIBLE);
                    mobile_line.invalidate();
                    local_line.setVisibility(View.INVISIBLE);
                    local_line.invalidate();
                }
            });
        } else {
            this.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    localcontainer.setVisibility(View.VISIBLE);
                    localcontainer.invalidate();
                    mobilecontainer.setVisibility(View.GONE);
                    mobilecontainer.invalidate();
                    mobile_line.setVisibility(View.INVISIBLE);
                    mobile_line.invalidate();
                    local_line.setVisibility(View.VISIBLE);
                    local_line.invalidate();
                    displayLocalDataset();
                    drawGraphs();
                }
            });
        }
        visible_cardselector = i;
    }

    /**
     * Calls the sensor setup activity.
     */

    private void call_SensorAPILogin(){
        Intent i = new Intent(this, LoginActivity.class);
        startActivity(i);
    }


    private void setUpdateBlocker(final int max){
        updateCloseSensors_is_running = true;
        Runnable r = new Runnable(){
            @TargetApi(Build.VERSION_CODES.M)
            @Override
            public void run(){
                ProgressBar progressBar = (ProgressBar) findViewById(R.id.main_updatebar);
                if (progressBar!=null){
                    Drawable d = progressBar.getIndeterminateDrawable().mutate();
                    if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                        d.setColorFilter(getResources().getColor(R.color.primaryLightColor,context.getTheme()),PorterDuff.Mode.SCREEN);
                    } else {
                        d.setColorFilter(getResources().getColor(R.color.primaryLightColor),PorterDuff.Mode.SCREEN);
                    }
                    progressBar.setIndeterminateDrawable(d);
                    Drawable d2 = progressBar.getProgressDrawable().mutate();
                    if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                        d2.setColorFilter(getResources().getColor(R.color.primaryLightColor,context.getTheme()),PorterDuff.Mode.SCREEN);
                    } else {
                        d2.setColorFilter(getResources().getColor(R.color.primaryLightColor),PorterDuff.Mode.SCREEN);
                    }
                    progressBar.setProgressDrawable(d2);
                    if (max != 0){
                        progressBar.setMax(max);
                        progressBar.setProgress(0);
                        progressBar.setIndeterminate(false);
                    } else {
                        progressBar.setIndeterminate(true);

                    }
                    progressBar.setVisibility(View.VISIBLE);
                    progressBar.invalidate();
                }
            }
        };
        runOnUiThread(r);
    }

    private void updateProgressBar(final int progress){
        Runnable r = new Runnable() {
            @Override
            public void run() {
                ProgressBar progressBar = (ProgressBar) findViewById(R.id.main_updatebar);
                if (progressBar != null) {
                    if ((!progressBar.isIndeterminate()) && (progressBar.getVisibility()==View.VISIBLE)){
                        progressBar.setProgress(progress);
                        progressBar.invalidate();
                    }
                }
            }
        };
        runOnUiThread(r);
    }

    private void releaseUpdateBlocker(){
        updateCloseSensors_is_running = false;
        final ProgressBar progressBar = (ProgressBar) findViewById(R.id.main_updatebar);
        if (progressBar!=null){
            if (progressBar.getVisibility() == View.VISIBLE){
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        progressBar.setVisibility(View.GONE);
                    }
                });
            }
        }
    }

    /**
     * Updates all displays including all widgets with known, available data, either present
     * in variables or in the local data base.
     *
     * Calling this will not call the API.
     */


    private void updateDataDisplays(){
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                readPreferences();
                displayLocalDataset();
                drawGraphs();
                updateSensorData();
                checkForWarning();
                checkForReferenceDisplay();
            }
        });
    }


    public static final int REPFRESH_ALL_WIDGETS = 0;
    public static final int REPFRESH_LOCAL_WIDGETS = 1;
    public static final int REPFRESH_CLOSE_WIDGETS = 2;

    /**
     * Updates widget instances by category.
     *
     * Following this update call, the widgets display present data only and do not
     * query for new data.
     *
     * The intention of this sub is to pass on now data obtained from the main app
     * to the widgets, so that they also display the most current data.
     */

    public static void refreshWidgets(Context context, int update_type, Location localLastKnownLocation, ArrayList<SensorDataSet> sensorDataSets){
        if ((update_type==REPFRESH_ALL_WIDGETS) || (update_type == REPFRESH_LOCAL_WIDGETS)){
            // refresh small widgets
            Intent widget_small = new Intent(context,StaubWidget.class);
            widget_small.setAction(StaubWidget.WIDGET_CUSTOM_REFRESH_ACTION);
            context.sendBroadcast(widget_small);
            // refresh big widgets
            Intent widget_big = new Intent(context,WidgetBig.class);
            widget_big.setAction(StaubWidget.WIDGET_CUSTOM_REFRESH_ACTION);
            context.sendBroadcast(widget_big);
            // refresh big widgets, one chart
            Intent widget_big_single = new Intent(context,WidgetBigSingle.class);
            widget_big_single.setAction(StaubWidget.WIDGET_CUSTOM_REFRESH_ACTION);
            context.sendBroadcast(widget_big_single);
        }
        if ((update_type==REPFRESH_ALL_WIDGETS) || (update_type == REPFRESH_CLOSE_WIDGETS)){
            // refresh close widgets
            Intent widget_close = new Intent(context,WidgetClose.class);
            widget_close.setAction(StaubWidget.WIDGET_CUSTOM_REFRESH_ACTION);
            /*
             * Put known location to intent so that the close widgets get it. This avoids
             * unnecessary calls of the location api by the widgets.
             */
            if ((localLastKnownLocation != null) && (sensorDataSets!=null)){
                Bundle b = new Bundle();
                b.putParcelable(FeinstaubLocationManager.KEY_LOCATION_CHANGED,localLastKnownLocation);
                b.putParcelableArrayList(NEXTSENSORS_DATA,sensorDataSets);
                widget_close.putExtras(b);
            }
            context.sendBroadcast(widget_close);
        }
    }

    /**
     * Convenience method to get a string with the time from a location.
     */

    private String getLocalLocationTimeString(Location l){
        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(l.getTime());
        SimpleDateFormat simpleDateFormat_target = new SimpleDateFormat("dd.MM. HH:mm", Locale.getDefault());
        try{
            return simpleDateFormat_target.format(calendar.getTimeInMillis());
        } catch (Exception e){
            return "";
        }
    }

    /**
     * Convenience method to get a string with the current date & time.
     */

    private String getCurrentTimeString(){
        Calendar calendar = Calendar.getInstance();
        SimpleDateFormat simpleDateFormat_target = new SimpleDateFormat("dd.MM. HH:mm", Locale.getDefault());
        try{
            return simpleDateFormat_target.format(calendar.getTimeInMillis());
        } catch (Exception e){
            return "";
        }
    }

    /**
     * Displays the status of the location service if necessary.
     */

    private void displayLocationServiceStatus(){
        if (lm == null){
            registerToLocationUpdates();
        }
        if (lm == null)
            return;
        LinearLayout container = (LinearLayout) findViewById(R.id.main_nextsensors_nolocation_hint_container);
        ImageView icon = (ImageView) findViewById(R.id.main_nextsensors_nolocation_hint_icon);
        TextView hint = (TextView) findViewById(R.id.main_nextsensors_nolocation_hint_text);
        if ((container!=null) && (hint != null) && (icon != null)){
            if (!lm.isProviderEnabled(preferences)){
                hint.setText(getResources().getString(R.string.main_location_devicestatus_nolocationhint_service_off));
                container.setVisibility(LinearLayout.VISIBLE);
                if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    container.setBackgroundColor(this.getResources().getColor(R.color.secondaryDarkColor,getTheme()));
                } else {
                    container.setBackgroundColor(this.getResources().getColor(R.color.secondaryDarkColor));
                }
                container.invalidate();
                icon.setVisibility(ImageView.VISIBLE);
                icon.setImageResource(R.mipmap.ic_location_off_white_24dp);
                icon.invalidate();
                hint.setVisibility(TextView.VISIBLE);
                hint.invalidate();
            } else {
                if (localLastKnownLocation==null){
                    hint.setText(getResources().getString(R.string.main_location_devicestatus_nolocationhint_service_on));
                    container.setVisibility(LinearLayout.VISIBLE);
                    if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                        container.setBackgroundColor(this.getResources().getColor(R.color.secondaryDarkColor,getTheme()));
                    } else {
                        container.setBackgroundColor(this.getResources().getColor(R.color.secondaryDarkColor));
                    }
                    container.invalidate();
                    icon.setVisibility(ImageView.VISIBLE);
                    icon.setImageResource(R.mipmap.ic_location_on_white_24dp);
                    icon.invalidate();
                    hint.setVisibility(TextView.VISIBLE);
                    hint.invalidate();
                    registerToLocationUpdates();
                } else {
                    container.setVisibility(LinearLayout.GONE);
                    container.invalidate();
                }
            }
        }
    }



    /**
     * Updates the map (fine particulate matter) or the chart (temperature/humidity) of the mobile view. The
     * type of data is handled by the DisplayManager, which produces the correct image.
     */

    public void updateCloseSensorsMap(ArrayList<SensorDataSet> sensorDataSets){
        ImageView i = (ImageView) findViewById(R.id.main_closesensors_graph);
        if (i != null){
            if (sensorDataSets != null) {
                if (sensorDataSets.size()>0){
                    DisplayManager dm = new DisplayManager(localLastKnownLocation);
                    dm.drawCloseSensorsMap(sensorDataSets,i,preferences.large_circles);
                }
            }
        }
    }

    public void displayCloseLine(int position, final SensorDataSet sensorDataSet){
        String unit_distance = context.getResources().getString(R.string.main_distance_unit);
        String unit_string_value1;
        String unit_string_value2;
        TextView textView_sensor_id   = (TextView)  findViewById(locationtable_sensor_id[position]);
        ImageView imageView_direction = (ImageView) findViewById(locationtable_direction_id[position]);
        TextView textView_distance    = (TextView)  findViewById(locationtable_distance_id[position]);
        TextView textView_value1      = (TextView)  findViewById(locationtable_value1_id[position]);
        TextView textView_value2      = (TextView)  findViewById(locationtable_value2_id[position]);
        TableRow tableRow             = (TableRow)  findViewById(locationtable_row_id[position]);
        if (sensorDataSet!=null) {
            if (sensorDataSet.getSensorType()==SensorDataSet.PARTICULATE_SENSOR){
                unit_string_value1 = context.getResources().getString(R.string.main_particulate_unit);
                unit_string_value2 = context.getResources().getString(R.string.main_particulate_unit);
            } else {
                unit_string_value1 = context.getResources().getString(R.string.main_temperature_unit);
                unit_string_value2 = context.getResources().getString(R.string.main_humidity_unit);
            }
            if (textView_sensor_id != null){
                textView_sensor_id.setText(String.valueOf(sensorDataSet.sensorId));
            }
            if (textView_value1 != null) {
                textView_value1.setText(sensorDataSet.sensordataValue[0]+" "+unit_string_value1);
            }
            if (textView_value2 != null){
                textView_value2.setText(sensorDataSet.sensordataValue[1]+" "+unit_string_value2);
            }
        } else {
            if (textView_sensor_id != null){
                textView_sensor_id.setText("-");
            }
            if (textView_value1 != null) {
                textView_value1.setText("-");
            }
            if (textView_value2 != null){
                textView_value2.setText("-");
            }
        }
        Location sensorLocation = sensorDataSet.toLocation();
        if (sensorLocation!=null){
            if (imageView_direction != null){
                imageView_direction.setImageBitmap(DisplayManager.getArrowBitmap(context,localLastKnownLocation.bearingTo(sensorLocation)));
            }
            if (textView_distance != null) {
                textView_distance.setText(String.valueOf(Math.round(sensorLocation.distanceTo(localLastKnownLocation)))+" "+unit_distance);
            }
        } else {
            if (imageView_direction != null){
                imageView_direction.setImageBitmap(null);
            }
            if (textView_distance != null) {
                textView_distance.setText("-");
            }
        }
        if (tableRow != null){
            tableRow.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (sensorDataSet!=null){
                        if (sensorDataSet.sensorId != null){
                            Intent i = new Intent(context, SensorInfo.class);
                            i.putExtra(DATA_SENSORNUMBER, sensorDataSet.sensorId);
                            startActivity(i);
                        }
                    }
                }
            });
        }
    }

    private void checkFetchProgress(){
        if ((fetchDisplayPosition<6) && (fetchIDNumber<sensorIDs.size())){
            executor.execute(new Runnable() {
                @Override
                public void run() {
                    fetchSingleSensor(cardHandler,fetchDisplayPosition,sensorIDs.get(fetchIDNumber));
                }
            });
        }  else {
            releaseUpdateBlocker();
            updateDataTimeOfTable();
            updateCloseSensorsMap(sensorDataSets);
            refreshWidgets(context,REPFRESH_ALL_WIDGETS,localLastKnownLocation,sensorDataSets);
        }
    }

    public void fetchSingleSensor(final CardHandler cardHandler, final int postion,String sensor){
        boolean fetchNewData = false;
        updateProgressBar(postion);
        // get from database
        final SensorDataSet sensorDataSet = cardHandler.getLatestDataSet(sensor);
        if (sensorDataSet==null){
            fetchNewData = true;
        } else {
            if (sensorDataSet.timestamp_UTCmillis+300000 > Calendar.getInstance().getTimeInMillis()){ // 5 minutes
                fetchNewData = true;
            }
        }
        if (fetchNewData){
            final LuftDatenAPIStreamReader luftStreamReader = new LuftDatenAPIStreamReader();
            luftStreamReader.setUrls(new String[]{sensor});
            luftStreamReader.setContext(context);
            luftStreamReader.setRunOnError(new Runnable() {
                @Override
                public void run() {
                    fetchIDNumber++;
                    checkFetchProgress();
                }
            });
            luftStreamReader.setRunOnSuccess(new Runnable() {
                @Override
                public void run() {
                    ArrayList<FullSensorDataSet> sensorDataResult = luftStreamReader.getResultArrayList();
                    if (sensorDataResult.size()>0){
                        SensorDataSet sensorDataSet = sensorDataResult.get(0).toSensorDataSet();
                        displayCloseLine(postion,sensorDataSet);
                        sensorDataSets.add(sensorDataSet);
                        cardHandler.addSensorDataSet(sensorDataSet);
                        fetchDisplayPosition++;
                    } else {
                    }
                    fetchIDNumber++;
                    checkFetchProgress();
                }
            });
            luftStreamReader.run();
        } else {
            sensorDataSets.add(sensorDataSet);
            fetchIDNumber++; fetchDisplayPosition++;
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    displayCloseLine(postion,sensorDataSet);
                    checkFetchProgress();
                }
            });
        }
    }

    public void updateSensorData(){
        if (mainContainer!=null){
            if (localLastKnownLocation==null){
                if (lm==null){
                    registerToLocationUpdates();
                }
                localLastKnownLocation = lm.getLastKnownLocation();
                if (localLastKnownLocation==null){
                    return;
                }
            }
            mainContainer.post(new Runnable() {
                @Override
                public void run() {
                    executor.execute(new Runnable() {
                        @Override
                        public void run() {
                            if (!updateCloseSensors_is_running){
                                fetchDisplayPosition=0; fetchIDNumber=0; sensorDataSets=new ArrayList<SensorDataSet>();
                                setUpdateBlocker(6);
                                sensorIDs = new ArrayList<String>();
                                SensorlocationHandler sensorlocationHandler = new SensorlocationHandler(context);
                                int[] sensortypes = new int[1];
                                if (getSelectedVisualizationType() == VISUALIZE_PARTICULATE){
                                    sensortypes[0] = SensorDataSet.PARTICULATE_SENSOR;
                                } else {
                                    sensortypes[0] = SensorDataSet.TEMPERATURE_SENSOR;
                                }
                                if (lm==null){
                                    registerToLocationUpdates();
                                }
                                lm.getLastKnownLocation();
                                if (localLastKnownLocation!=null){
                                    ArrayList<LocationDataSet> locationDataSets = sensorlocationHandler.getNextSensors(localLastKnownLocation,20,sensortypes);
                                    for (int i=0; ((i<20) && (i<locationDataSets.size())); i++){
                                        sensorIDs.add(locationDataSets.get(i).number);
                                    }
                                } else {
                                    registerToLocationUpdates();
                                    runOnUiThread(new Runnable() {
                                        @Override
                                        public void run() {
                                            lm.requestSingleUpdate(loc_listener,executor,preferences);
                                        }
                                    });
                                    runOnUiThread(new Runnable() {
                                        @Override
                                        public void run() {
                                            displayLocationServiceStatus();
                                        }
                                    });
                                }
                                if (sensorIDs.size()>0){
                                    if (cardHandler==null){
                                        cardHandler = new CardHandler(context);
                                    }
                                    checkFetchProgress();
                                }
                            } else {
                                // failed update
                            }
                        }
                    });
                }
            });
        }
    }

    /**
     * Shows the dialog to download the database with all the sensor locations. Please note that this
     * is not really a dialog but a view that simply gets invisible when not needed.
     */

    private void showDownloadWarning(){
        LinearLayout initial_location_info = (LinearLayout) findViewById(R.id.main_location_init_dialog_container);
        initial_location_info.setVisibility(LinearLayout.VISIBLE);
        ProgressBar spinner = (ProgressBar) findViewById(R.id.main_location_init_dialog_spinner);
        spinner.setVisibility(View.VISIBLE);
        spinner.invalidate();
        TextView location_info_text = (TextView) findViewById(R.id.location_init_dialog_download_text);
        location_info_text.setText(context.getResources().getString(R.string.main_location_data_keepopen));
        initial_location_info.invalidate();
        Button initial_location_loadbutton = (Button) findViewById(R.id.location_init_dialog_download_button);
        initial_location_loadbutton.setVisibility(View.GONE);
    }

    /**
     * Checks if the dialog (see above) can be closed and closes it when necessary.
     *
     * In particular, it gets invisible when there is a local database with location data.
     */

    private void checkForWarning(){
        LinearLayout initial_location_info = (LinearLayout) findViewById(R.id.main_location_init_dialog_container);
        ProgressBar progressBar            = (ProgressBar) findViewById(R.id.main_location_init_dialog_spinner);
        TextView location_info_text        = (TextView) findViewById(R.id.location_init_dialog_download_text);
        if ((preferences.lastlocationlistupdatetime !=0 ) && (initial_location_info.getVisibility() != View.GONE)){
            initial_location_info.setVisibility(View.GONE);
            initial_location_info.invalidate();
        } else if (readingAllLocationsList_is_running){
            showDownloadWarning();
        }
    }

    /**
     * Convenience call to reset the PREF_LOCATIONUPDATE to zero. This lets the app now that the
     * list of sensor locations has to be reloaded.
     * In other words: this call invalidates the list of known sensor locations and forces a
     * reload.
     */

    private void resetSensorLocationsLoadedFlag(){
        /*
         * .commit() is used (synchronous) instead of .apply() to prevent any running conditions
         * regarding the state of the list of known sensors.
         * In particular, this forces all app components to stop using the invalidated list of
         * known sensor locations immediately.
         */
        preferences.commitPreference(preferences.PREF_LOCATIONUPDATE,new Long(0));
    }

    /**
     * Downloads all known sensor locations form the API and stores them locally.
     *
     * @param onlyifempty
     * true  = download only if no data present,
     * false = always download
     *
     * @param delete_list
     * true  = delete the present list before loading. This removes all known sensors
     *         before looking for new ones.
     * false = add new sensors to the existing data base
     *
     *
     * @return
     * Returns true if the download started. Please note: this does not indicate *success* of
     * downloading and parsing the data, but only that the process was started.
     */

    private Boolean updateSensorLocationsList(Boolean onlyifempty, Boolean delete_list){
        if ((onlyifempty) && (preferences.lastlocationlistupdatetime != 0)) {
            return false;
        }
        SensorlocationHandler sldh = new SensorlocationHandler(context);
        if (delete_list) {
        /*
           Set the flag in the settings that here is no data. This is done to prevent
           broken / incomplete databases.
        */
            resetSensorLocationsLoadedFlag();
        /*
           Tries to delete the data first.
         */
            SensorlocationHandler sensorlocationHandler = new SensorlocationHandler(this);
            sensorlocationHandler.deleteSensorDataBase();
            // this.deleteDatabase(LuftDatenContentProvider.SensorLocationsDatabaseHelper.DATABASE_NAME);
        }
        /*
           Defines a runnable that will run after the processing of new sensors was
           sucessful. This will then look for new data and promptly update the
           mobile screen.
         */
        Runnable r_success = new Runnable() {
            @Override
            public void run() {
                Calendar c = Calendar.getInstance();
                preferences.commitPreference(preferences.PREF_LOCATIONUPDATE,new Long(c.getTimeInMillis()));
                releaseUpdateBlocker();
                // Allow screen rotation within this app again
                setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_UNSPECIFIED);

                Toast.makeText(context,getResources().getString(R.string.main_location_data_success),Toast.LENGTH_LONG).show();
                updateSensorData();
            }
        };
        /*
           Defines a runnable that will run after the processing of new sensors
           failed.
         */
        Runnable r_error = new Runnable() {
            @Override
            public void run() {
                Toast.makeText(context,getResources().getString(R.string.main_location_data_failed),Toast.LENGTH_LONG).show();
                releaseUpdateBlocker();
                // Allow screen rotation within this app again
                setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_UNSPECIFIED);
            }
        };
        /*
           Starts download and processing of the sensor locations.
         */
        int[] supported_sensortypes = {SensorDataSet.PARTICULATE_SENSOR, SensorDataSet.TEMPERATURE_SENSOR};
        try {
            sldh.setFetchList_positiveresultMessage(getResources().getString(R.string.main_location_data_success));
            sldh.setFetchList_negativeresultMessage(getResources().getString(R.string.main_location_data_failed));
            sldh.setWarning_container_view(findViewById(R.id.main_location_init_dialog_container));
            setUpdateBlocker(0);
            // Lock screen rotation during list download & processing.
            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LOCKED);
            sldh.getLocationDataFromAPI(this,executor,supported_sensortypes,!delete_list, r_error,r_success,(TextView) findViewById(R.id.location_init_dialog_progress_text));
        } catch (Exception e){
            releaseUpdateBlocker();
            return false;
        }
        return true;
    }


    /**
     * Deletes old data entries that are older than DATABASE_TIMESPAN from now.
     *
     * Please note: the CardHandler can do this synchronously in the main thread or
     * asynchronously.
     *
     * The synchronous delete is recommended for the widget, the async delete is
     * recommended to the main app.
     */

    private void cleanDatabase(){
        Calendar calendar = Calendar.getInstance();
        Long cut_date_in_millis = calendar.getTimeInMillis()- DATABASE_TIMESPAN; // -24h in UTC
        Date cut_date = new Date(cut_date_in_millis);
        CardHandler ch = new CardHandler(this);
        ch.deleteOldEntries(cut_date,true);
    }

    /**
     * Displays the text values of the local sensor.
     */

    private void displayLocalDataset(){
        CardHandler ch = new CardHandler(this);
        SensorDataSet sensor1_data = ch.getLatestDataSet(preferences.sensor1_number);
        SensorDataSet sensor2_data = ch.getLatestDataSet(preferences.sensor2_number);
        DisplayManager dsm = new DisplayManager(localLastKnownLocation);
        if (sensor1_data != null){
            dsm.displayCurrentDataset(context,
                    sensor1_data,
                    (TextView) findViewById(R.id.main_sensor1_value),
                    (TextView) findViewById(R.id.main_sensor1_timestamp),
                    (TextView) findViewById(R.id.main_sensor1_timestamp_value),
                    (TextView) findViewById(R.id.main_sensor1_data1_label),
                    (TextView) findViewById(R.id.main_sensor1_data2_label),
                    (TextView) findViewById(R.id.main_sensor1_data1_value),
                    (TextView) findViewById(R.id.main_sensor1_data2_value),
                    (TextView) findViewById(R.id.main_sensor1_location_country_value),
                    (TextView) findViewById(R.id.main_sensor1_location_long_value),
                    (TextView) findViewById(R.id.main_sensor1_location_lat_value),
                    (TextView) findViewById(R.id.main_sensor1_location_alt_value));
        }
        if (sensor2_data != null){
            dsm.displayCurrentDataset(context,
                    sensor2_data,
                    (TextView) findViewById(R.id.main_sensor2_value),
                    (TextView) findViewById(R.id.main_sensor2_timestamp),
                    (TextView) findViewById(R.id.main_sensor2_timestamp_value),
                    (TextView) findViewById(R.id.main_sensor2_data1_label),
                    (TextView) findViewById(R.id.main_sensor2_data2_label),
                    (TextView) findViewById(R.id.main_sensor2_data1_value),
                    (TextView) findViewById(R.id.main_sensor2_data2_value),
                    (TextView) findViewById(R.id.main_sensor2_location_country_value),
                    (TextView) findViewById(R.id.main_sensor2_location_long_value),
                    (TextView) findViewById(R.id.main_sensor2_location_lat_value),
                    (TextView) findViewById(R.id.main_sensor2_location_alt_value));
        }
    }

    /**
     * Handles drawing the graphs of both local sensors.
     *
     * @param sensor1
     * @param sensor2
     * @param image1
     * @param image2
     */

    private void drawGraphs(String sensor1, String sensor2, ImageView image1, ImageView image2){
        CardHandler ch = new CardHandler(this);
        DisplayManager dsm = new DisplayManager(localLastKnownLocation);
        dsm.connectLines(preferences.drawlines);
        if ((sensor1 != "") && (image1 != null)){
            ArrayList<SensorDataSet> data1 = new ArrayList<SensorDataSet>();
            data1 = ch.getSensorDataSetArrayByTimeInterval(sensor1, (long) GRAPH_TIMESPAN);
            if (data1.size()>0){
                SensorDataSet sds = data1.get(0);
                if (sds.getSensorType() == SensorDataSet.PARTICULATE_SENSOR)
                    dsm.drawParticulate(context, data1,image1);
                if (sds.getSensorType() == SensorDataSet.TEMPERATURE_SENSOR)
                    dsm.drawTemperature(context, data1,image1);
            }
        }
        if ((sensor2 != "") && (image2 != null)){
            ArrayList<SensorDataSet> data2 = new ArrayList<SensorDataSet>();
            data2 = ch.getSensorDataSetArray(sensor2);
            if (data2.size()>0){
                SensorDataSet sds = data2.get(0);
                if (sds.getSensorType() == SensorDataSet.PARTICULATE_SENSOR)
                    dsm.drawParticulate(context,data2,image2);
                if (sds.getSensorType() == SensorDataSet.TEMPERATURE_SENSOR)
                    dsm.drawTemperature(context,data2,image2);
            }
        }
    }

    /**
     * Convenience call for the above method that determines the view ids and
     * passes them over.
     */

    private void drawGraphs(){
        ScrollView mainviewcontainer = (ScrollView) findViewById(R.id.main_maincontainer_layout);
        graph1 = (ImageView) findViewById(R.id.main_graph1);
        graph2 = (ImageView) findViewById(R.id.main_graph2);
        mainviewcontainer.post(new Runnable() {
            @Override
            public void run() {
                drawGraphs(preferences.sensor1_number, preferences.sensor2_number, graph1, graph2);
            }
        });
    }

    private Boolean isLocalSensorDataTooOld(SensorDataSet[] sensorDataSets){
        if (sensorDataSets == null)
            return true;
        if (sensorDataSets.length==0)
            return true;
        Calendar c = Calendar.getInstance();
        long time = 0;
        for (int i=0; i<sensorDataSets.length; i++){
            if (sensorDataSets[i].getTimestampLocalMillis()>time)
                time = sensorDataSets[i].getTimestampLocalMillis();
        }
        if (c.getTimeInMillis()-time<StaubWidget.WIDGET_CONN_RETRY_DELAY/2){
            return false;
        }
        return true;
    }

    private void updateDataTimeOfTable(){
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                TextView status = (TextView) findViewById(R.id.main_nextsensors_lastupdate_text);
                table_lastupdatetext = getResources().getString(R.string.main_location_lastupdate_time)+" "+getCurrentTimeString();
                if (status != null){
                    status.setText(table_lastupdatetext);
                    status.setVisibility(View.VISIBLE);
                    status.invalidate();
                }
            }
        });
        if (localLastKnownLocation!=null) {
            final TextView own_location_text = (TextView) findViewById(R.id.main_nextsensors_devicelocation_text);
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    DecimalFormat decimalFormat = new DecimalFormat();
                    decimalFormat.applyPattern("###.###");
                    String lat_string = decimalFormat.format(localLastKnownLocation.getLatitude());
                    String long_string = decimalFormat.format(localLastKnownLocation.getLongitude());
                    own_location_text.setText(getResources().getString(R.string.main_location_devicestatus_position_text) + " " +
                            getResources().getString(R.string.main_location_devicestatus_position_lat) + " " + lat_string + " " +
                            getResources().getString(R.string.main_location_devicestatus_position_long) + " " + long_string + " " +
                            "(" + getLocalLocationTimeString(localLastKnownLocation) + " " + lm.getLocationSourceChar(localLastKnownLocation) + ")");
                }
            });
        }
    }

    /**
     * The following routine handles the switch between the display of
     * fine particulate matter OR temperature/humidity in the mobile view.
     */

    private final static Boolean VISUALIZE_PARTICULATE = false;
    private final static Boolean VISUALIZE_TEMPERATURE = true;

    /**
     * Reads the value of the switch.
     *
     * @return is "true" or "false", meaning checked or not checked.
     */

    public Boolean getSelectedVisualizationType(){
        Switch s = (Switch) findViewById(R.id.main_nextsensors_dataswitch);
        if (s!=null){
            if (s.isChecked()){
                return true;
            }
        }
        return false;
    }

    /**
     * Displays (or hides) the reference display, following what has been set in the preferences.
     */

    public void checkForReferenceDisplay(){
        TextView t = (TextView) findViewById(R.id.main_reference_text);
        if (t != null){
            Boolean display_reference = preferences.readPreference(preferences.PREF_DISPLAY_REFERENCE,preferences.PREF_DISPLAY_REFERENCE_DEFAULT);
            if (display_reference) {
                t.setVisibility(View.VISIBLE);
                t.invalidate();
            } else {
                t.setVisibility(View.INVISIBLE);
                t.invalidate();
            }
        }
    }

    /**
     * Registers to location updates. This is only of importance if the location changes and the
     * app is in the foreground.
     */

    private void registerToLocationUpdates(){
        if (lm == null) {
            loc_listener = new FeinstaubLocationListener(this){
                @Override
                public void onLocationChanged(Location location) {
                        super.onLocationChanged(location);
                        //updateSensorData();
                        localLastKnownLocation = location;
                        displayLocationServiceStatus();
                        updateDataDisplays();
                }
                @Override
                public void onProviderEnabled(String s) {
                    displayLocationServiceStatus();
                    requestSingleLocationUpdate();
                }
                @Override
                public void onProviderDisabled(String s) {
                    displayLocationServiceStatus();
                }
            };
            loc_listener.setLocationContext(context);
            loc_listener.setLocationVariable(localLastKnownLocation);
            lm = new FeinstaubLocationManager(this);
            if (hasLocationPermission(false)) {
                try {
                    lm.requestLocationUpdates(loc_listener,preferences);
                } catch (Exception e){
                    // nothing to do; no suitable permissions granted to get the location.
                }
            } else {
                /*
                 * Set 1st sensor as location if this is the desired behavior.
                 */
                if (preferences == null)
                    readPreferences();
                if (preferences.use_local_sensor_location_as_fallback){
                    if (localLastKnownLocation == null){
                        localLastKnownLocation = lm.getLastKnownLocation(preferences);
                    }
                }
            }
        }
    }

    /**
     * This is a convenience method that checks if the variable last_known_location has a valid
     * location.
     *
     * If not, it tries to get a "last known location" from the system. It tries to get this
     * from the GPS_PROVIDER first. If such a location is not known, it tries to get the
     * location from the PASSIVE_PROVIDER. This is likely to be less accurate.
     *
     * @param c Context
     * @return last known location
     */

    private Location getLastKnownLocationIfNotKnown(Context c){
        if (lm == null){
            registerToLocationUpdates();
        }
        if (lm != null){
            if (localLastKnownLocation == null) {
                localLastKnownLocation = lm.getLastKnownLocation(preferences);
            }
        }
        return localLastKnownLocation;
    }


    /**
     * Requests a single location update. This is triggered manually.
     */

    public void requestSingleLocationUpdate(){
        FeinstaubLocationListener feinstaubLocationListener = new FeinstaubLocationListener(this){
            @Override
            public void onLocationChanged(Location location) {
                if (location!=null){
                    updateSensorData();
                }
            }
        };
        if (lm == null){
            lm = new FeinstaubLocationManager(context);
        }
        if (lm != null){
            try {
                lm.requestSingleUpdate(feinstaubLocationListener,executor,preferences);
            } catch (Exception e){
                /*
                 * Location update failed. We do nothing here.
                 */
            }
        }
    }

    /**
     * Reads the last visible selector (mobile or local) from the preferences. Returns the value.
     *
     * @return
     */

    private int getCardSelectorValue(){
        return preferences.readPreference(FeinstaubPreferences.PREF_CARDSELECTORVALUE,FeinstaubPreferences.PREF_CARDSELECTORVALUE_DEFAULT);
    }

    /**
     * Stores the selector i in the preferences.
     *
     * @param i the visible selector to store.
     */

    private void storeCardSelectorValue(int i){
        preferences.commitPreference(FeinstaubPreferences.PREF_CARDSELECTORVALUE,i);
    }

    /**
     * Reads the last visible data selector position (fine particulate matter or temperature) from the
     * preferences.
     * Returns the value.
     *
     * @return
     */

    private Boolean getDataSelectorPosition(){
        return preferences.readPreference(FeinstaubPreferences.PREF_DATASWITCHPOSITION,FeinstaubPreferences.PREF_DATASWITCHPOSITION_DEFAULT);
    }

    /**
     * Stores the last visible data selector position.
     */

    private void storeDataSelectorPosition(Boolean position){
        preferences.commitPreference(FeinstaubPreferences.PREF_DATASWITCHPOSITION,position);
    }

    /**
     * Reads the preferences.
     */

    private void readPreferences() {
        preferences = new FeinstaubPreferences(this);
    }

    /**
     * Displays the intro dialog.
     */

    public void showIntroDialog(){
        introDialog = new Dialog(this);
        introDialog.getWindow().setBackgroundDrawableResource(R.mipmap.datasheet_background);
        introDialog.setContentView(R.layout.aboutdialog);
        introDialog.setTitle(getResources().getString(R.string.app_name));
        introDialog.setCancelable(true);
        String versionname = "-";
        String versioncode = "-";
        PackageManager pm = this.getPackageManager();
        try {
            versionname = this.getPackageManager().getPackageInfo(this.getPackageName(),0).versionName;
            versioncode = String.valueOf(this.getPackageManager().getPackageInfo(this.getPackageName(),0).versionCode);
        } catch (Exception e){
            // do nothing
        }
        String versionstring = versionname + " (build "+versioncode+")";
        TextView heading = (TextView) introDialog.findViewById(R.id.text_intro_headtext);
        heading.setText(getResources().getString(R.string.intro_headtext_text)+ System.getProperty("line.separator")+"version "+versionstring);
        ImageView iv = (ImageView) introDialog.findViewById(R.id.intro_headimage);
        iv.setImageResource(R.mipmap.ic_launcher);
        Button contbutton = (Button) introDialog.findViewById(R.id.intro_button);
        contbutton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v){
                introDialog.dismiss();
            }
        });
        introDialog.show();
    }

    /**
     * Returns the bitmap to share, which is derived from the visible views.
     *
     * @param data_source this should be MainActivity.CARDSELECTOR_LOCAL or MainActivity.CARDSELECTOR_MOBILE
     * @return
     */

    public Bitmap getShareDataBitmap(int data_source){
        LinearLayout sourceLinearLayout = null;
        Bitmap resultBitmap = null;
        if (data_source==MainActivity.CARDSELECTOR_LOCAL) {
            sourceLinearLayout = (LinearLayout) findViewById(R.id.main_local_container);
        }
        if (data_source==MainActivity.CARDSELECTOR_MOBILE) {
            sourceLinearLayout = (LinearLayout) findViewById(R.id.main_nextsensors_displaycontainer);
        }
        if (sourceLinearLayout!=null){
            Bitmap layoutBitmap = Bitmap.createBitmap(sourceLinearLayout.getWidth(),sourceLinearLayout.getHeight(), Bitmap.Config.ARGB_8888);
            Canvas c = new Canvas(layoutBitmap);
            sourceLinearLayout.draw(c);
            DisplayManager displayManager = new DisplayManager(this);
            resultBitmap = displayManager.attachReferenceLayerToBitmap(layoutBitmap);
        }
        return resultBitmap;
    }

    private String getShareFileName(){
        final SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH-mm-ss");
        return FeinstaubPreferences.getPrefFilename(this) + " " + simpleDateFormat.format(new Date(Calendar.getInstance().getTimeInMillis())) + ".jpeg";
    }

    private boolean deleteDir(File file){
        if (file.isDirectory()){
            String[] files = file.list();
            for (int i=0; i<files.length; i++){
                if (!deleteDir(new File(file,files[i]))){
                    return false;
                }
            }
        }
        return file.delete();
    }

    private void saveOrShareVisibleData(int action){
        deleteDir(getFilesDir());
        Bitmap bitmap = getShareDataBitmap(visible_cardselector);
        String filename = getShareFileName();
        File outFile = new File(getFilesDir(),filename);
        try {
            FileOutputStream fileOutputStream = new FileOutputStream(outFile);
            bitmap.compress(Bitmap.CompressFormat.JPEG,80,fileOutputStream);
            fileOutputStream.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
        Intent intent = new Intent(Intent.ACTION_SEND);
        Uri uri = FileContentProvider.getUriForFile("pollutionprovider",FileContentProvider.TAG_FILES,outFile,null);
        intent.setDataAndType(uri,"image/jpeg");
        intent.putExtra(Intent.EXTRA_STREAM, uri);
        intent.setFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
        Intent chooser = Intent.createChooser(intent, getResources().getString(R.string.share_choosertitle));
        sharedFileUri = uri;
        if (intent.resolveActivity(getPackageManager())!=null){
            startActivity(chooser);
        } else {
            // sharing failed, no target app
            Toast.makeText(this,getResources().getString(R.string.share_failed),Toast.LENGTH_LONG).show();
        }
    }

    private final int ACTION_SAVE = 1;
    private final int ACTION_SHARE = 2;

    private void displaySaveAndShareAlertDialog(final int action){
        View sharehintView = View.inflate(this,R.layout.sharehint,null);
        CheckBox do_not_show_again = (CheckBox) sharehintView.findViewById(R.id.sharehint_checkbutton);
        do_not_show_again.setChecked(false);
        do_not_show_again.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                preferences.do_not_show_sharehint_again = b;
                preferences.applyPreference(FeinstaubPreferences.PREF_DO_NOT_SHOW_SHAREHINT_AGAIN,preferences.do_not_show_sharehint_again);
            }
        });
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this, 0);
        alertDialogBuilder.setTitle(getResources().getString(R.string.sharehint_title));
        if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            alertDialogBuilder.setIcon(getResources().getDrawable(R.mipmap.ic_share_white_24dp,this.getTheme()));
        }
        alertDialogBuilder.setView(sharehintView);
        alertDialogBuilder.setCancelable(false);
        alertDialogBuilder.setPositiveButton(getResources().getString(R.string.sharehint_button_continue_text), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                if (action==ACTION_SHARE){
                    saveOrShareVisibleData(ACTION_SHARE);
                }
            }
        });
        alertDialogBuilder.setNegativeButton(getResources().getString(R.string.sharehint_button_cancel_text), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                dialogInterface.cancel();
            }
        });
        shareHint = alertDialogBuilder.show();
    }

    /**
     * Save or share visible data.
     *
     * @param action should be ACTION_SAVE or ACTION_SHARE.
     */

    private void shareCurrentVisibleData(int action){
        // this blocks saving/sharing while the views get updated with new data.
        if (!updateCloseSensors_is_running){
            if (preferences.do_not_show_sharehint_again){
                if (action==ACTION_SHARE){
                    saveOrShareVisibleData(action);
                }
            } else {
                displaySaveAndShareAlertDialog(action);
            }
        }
    }

}
