/*

 This file is part of FeinstaubWidget.

 Copyright (c) 2018, 2019, 2020, 2021, 2022, 2023, 2024 Pawel Dube

 FeinstaubWidget is free software: you can redistribute it and/or modify it
 under the terms of the GNU General Public License as published by the
 Free Software Foundation, either version 3 of the License, or (at
 your option) any later version.

 FeinstaubWidget is distributed in the hope that it will be useful, but
 WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with FeinstaubWidget. If not, see <http://www.gnu.org/licenses/>.

 */

package de.kaffeemitkoffein.feinstaubwidget;


import android.Manifest;
import android.app.PendingIntent;
import android.appwidget.AppWidgetManager;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.BitmapFactory;
import android.location.Location;
import android.os.Bundle;
import android.view.View;
import android.widget.RemoteViews;

import java.net.URL;
import java.util.ArrayList;

/**
 * This class defines the widget with the close sensors.
 *
 * Please note that is extends the StaubWidget class; for all the basic widget functionality, see
 * this class.
 *
 */

public class WidgetClose extends StaubWidget {

    ArrayList<SensorDataSet> sensors;
    ArrayList<LocationDataSet> locations;
    ArrayList<LocationDataSet> raw_locations;
    Location last_known_location;
    FeinstaubLocationManager feinstaubLocationManager;

    private static final int WIDGET_SENSORNUMBERS = 6;

    private static final int PI_TOUCH = 100;
    private static final int PI_LOCATION = 101;

    private static final String VERBOSEID = "WIDGET-CLOSE";

    int datafillcount = 0;

    private boolean hasLocationPermission(Context c){
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.M) {
            // handle runtime permissions only if android is >= Marshmellow api 23
            if (c.checkSelfPermission(Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED)
            {
                // permission not granted
                return false;
            }
            else
            {
                // permission is granted, ok
                return true;
            }
        }
        else
        {
            // before api 23, permissions are always granted, so everything is ok
            return true;
        }
    }

    /**
     * Registers a specific widget instance for an update triggered by touching the widget.
     *
     * Calls putDataIntoIntent(intent) to encapsulate the following variables within the pending
     * intent:
     * - last_known_location
     * - locations (arraylist with close sensors)
     * - sensors (arraylist with sensor data)
     *
     * This is done as the whole widget is not a service and therefore may be garbage collected when
     * inactive.
     *
     * The values are restored when the WIDGET_CUSTOM_UPDATE_ACTION is received by the widget.
     *
     * @param c context
     * @param rv remote views of the widget instance
     */

    private void registerWidgetForRefreshByTouch(Context c, RemoteViews rv){
        if (rv == null){
            rv = new RemoteViews(c.getPackageName(), R.layout.widget_layout_close);
        }
        Intent intent = new Intent();
        intent.setClass(c, getClass());
        intent.setAction(WIDGET_CUSTOM_UPDATE_ACTION);
        putDataIntoIntent(intent);
        PendingIntent pi;
        if (android.os.Build.VERSION.SDK_INT >= 23){
            pi = PendingIntent.getBroadcast(c, PI_TOUCH, intent, PendingIntent.FLAG_UPDATE_CURRENT|PendingIntent.FLAG_IMMUTABLE);
        } else {
            pi = PendingIntent.getBroadcast(c, PI_TOUCH, intent, PendingIntent.FLAG_UPDATE_CURRENT);
        }
        rv.setOnClickPendingIntent(R.id.widget_widget_close_relative, pi);
    }

    /**
     * Generates a PendingIntent that will be returned to the widget once location changed.
     *
     * @param c
     * @return
     */

    private PendingIntent getPendingIntentForLocationUpdate(Context c){
        readPreferences();
        Intent intent = new Intent();
        intent.setClass(c,getClass());
        intent.setAction(WIDGET_CUSTOM_NEWLOCATION_ACTION);
        PendingIntent pi;
        if (android.os.Build.VERSION.SDK_INT >= 23){
            pi = PendingIntent.getBroadcast(c,PI_LOCATION,intent,PendingIntent.FLAG_UPDATE_CURRENT|PendingIntent.FLAG_IMMUTABLE);
        } else {
            pi = PendingIntent.getBroadcast(c,PI_LOCATION,intent,PendingIntent.FLAG_UPDATE_CURRENT);
        }
        return pi;
    }

    /**
     * Registers a PendingIntent to the LocationManager that will be returned to the widget once
     * location changed.
     *
     * Please note: this respects settings from the main app concerning update frequency and distance.
     *
     * @param c
     */

    private void registerPendingIntentForLocationUpdate(Context c){
        readPreferences();
        if (feinstaubLocationManager == null){
            feinstaubLocationManager = new FeinstaubLocationManager(c);
        }
        if (feinstaubLocationManager.hasLocationPermission()){
            feinstaubLocationManager.requestLocationUpdates(getPendingIntentForLocationUpdate(c),preferences);
        }
    }

    /**
     * Removes the PendingIntent that listens to location changes for this widget.
     */

    private void unregisterLocationUpdates(Context c){
        Intent intent = new Intent();
        intent.setClass(c,getClass());
        intent.setAction(WIDGET_CUSTOM_NEWLOCATION_ACTION);
        if (android.os.Build.VERSION.SDK_INT >= 23){
            PendingIntent.getBroadcast(c,PI_LOCATION,intent,PendingIntent.FLAG_UPDATE_CURRENT|PendingIntent.FLAG_IMMUTABLE).cancel();
        } else {
            PendingIntent.getBroadcast(c,PI_LOCATION,intent,PendingIntent.FLAG_UPDATE_CURRENT).cancel();
        }



    }

    /**
     * Here, the type of the widget is defined: particulate matter.
     */

    private int[] getSensorTypesToDisplay(){
        int[] result = {SensorDataSet.PARTICULATE_SENSOR};
        return result;
    }

    /**
     * This is a convenience sub to determine if the sensor (parameter) is
     * found in the sensortypes array.
     *
     * @param sensor
     * @return true if "sensor" is in "sensortypes" array, otherwise false.
     */
    private Boolean isValidSensorType(int sensor){
        int[] sensortypes = getSensorTypesToDisplay();
        for (int i=0; i<sensortypes.length; i++){
            if (sensor == sensortypes[i])
                return true;
        }
        return false;
    }

    /**
     * This is called when the last widget gets disabled.
     *
     * @param c
     */

    @Override
    public void onDisabled(Context c){
        super.onDisabled(c);
        /*
         * Clean up: remove the location updates listener, as all instances of this widget have
         * been removed and we do not need to listen for location changes here any more.
         */
        unregisterLocationUpdates(c);
    }

    /**
     * Updates the widget display of all widget instances of this widget-type.
     *
     * @param c Context
     * @param awm AppWidgetManager
     * @param widget_instances widget instances
     */

    @Override
    public void updateWidgetDisplay(Context c, AppWidgetManager awm, int[] widget_instances) {
        if (last_known_location == null){
            displayMissingLocationIcon(c,awm,widget_instances);
        } else {
            if ((sensors != null) && (locations != null)){
                if (sensors.size()>0){
                    readPreferences();
                    for (int i = 0; i < widget_instances.length; i++) {
                        RemoteViews rv = new RemoteViews(c.getPackageName(), R.layout.widget_layout_close);
                        // Register the widget view as a "button" for refresh
                        registerWidgetForRefreshByTouch(c,rv);
                        // this is needed to show the right views again if permissions are present
                        displayMissingPermissionsInfoIfNeeded(c,rv);
                        // get the source char from the last known location
                        if (feinstaubLocationManager == null)
                            feinstaubLocationManager = new FeinstaubLocationManager(c);
                        String last_known_location_source = feinstaubLocationManager.getLocationSourceChar(last_known_location);
                        // update the widget
                        DisplayManager dsm = new DisplayManager(last_known_location);
                        dsm.setTheme(preferences.widget_theme);
                        WidgetDimensionManager wdm = new WidgetDimensionManager(context,awm,widget_instances[i]);
                        rv = dsm.displayCloseWidget(wdm,
                                rv,
                                R.id.widget_close_line1,
                                R.id.widget_close_line2,
                                R.id.widget_close_line3,
                                R.id.widget_close_maxvalue10,
                                R.id.widget_close_maxvalue25,
                                R.id.widget_close_graph,
                                preferences.display_only_two_lines,
                                last_known_location,
                                last_known_location_source,
                                sensors,
                                preferences.widget_opacity,
                                preferences.display_reference,
                                preferences.large_circles);
                        if (preferences.display_only_two_lines) {
                            rv.setViewVisibility(R.id.widget_close_line3, View.GONE);
                        }
                        //rv.setImageViewResource(R.id.widget_close_graph,R.drawable.ic_launcher_background);
                        awm.updateAppWidget(widget_instances[i], rv);
                    }
                }
            }
        }
    }

    /**
     * Displays a message that permissions are missing.
     *
     * @param c
     * @param rv
     */

    private void displayMissingPermissionsInfoIfNeeded(Context c, RemoteViews rv){
        if (!hasWidgetPrerequisites(c)){
            rv.setViewVisibility(R.id.widget_close_missingprerequisites, View.VISIBLE);
            rv.setViewVisibility(R.id.widget_close_linear_container, View.GONE);
        } else {
            rv.setViewVisibility(R.id.widget_close_missingprerequisites, View.GONE);
            rv.setViewVisibility(R.id.widget_close_linear_container, View.VISIBLE);
        }
    }

    /**
     * Displays the missing location icon and text in all widget instances
     * @param c
     * @param awm
     * @param widget_instances
     */

    private void displayMissingLocationIcon(Context c, AppWidgetManager awm, int[] widget_instances){
        RemoteViews rv = new RemoteViews(c.getPackageName(), R.layout.widget_layout_close);
        rv.setImageViewBitmap(R.id.widget_close_graph, BitmapFactory.decodeResource(c.getResources(),R.mipmap.ic_location_off_white_24dp));
        rv.setTextViewText(R.id.widget_close_line1,c.getResources().getString(R.string.widget_close_nolocation_notice));
        rv.setTextViewText(R.id.widget_close_line2,"");
        rv.setTextViewText(R.id.widget_close_line3,"");
        rv.setTextViewText(R.id.widget_close_maxvalue10,"");
        rv.setTextViewText(R.id.widget_close_maxvalue25,"");
        registerWidgetForRefreshByTouch(c,rv);
        for (int i = 0; i < widget_instances.length; i++){
            awm.updateAppWidget(widget_instances[i], rv);
        }
    }

    private Boolean hasWidgetPrerequisites(Context c){
        readPreferences();
        Boolean hasAtLeastOneLocationService = false;
        if ((preferences.use_location_provider_PASSIVE) || (preferences.use_location_provider_GPS) || (preferences.use_location_provider_NETWORK) || (preferences.use_local_sensor_location_as_fallback))
            hasAtLeastOneLocationService = true;
        return ((hasAtLeastOneLocationService) && (preferences.lastlocationlistupdatetime != 0));
    }

    @Override
    public void widgetRefreshAction(Context c, Intent i){
        if (getDataFromIntent(i)){
            if ((locations == null) || (sensors == null)){
                /*
                 * The widget got a new location with the intent, but the local data of sensors
                 * and locations was invalided because they do not fit to the new location.
                 *
                 * This is virtually the
                 */
                widgetNewLocationAction(c,i);
            } else {
                /*
                 * The widget got a new location with the intent, and also the corresponding data.
                 * Therefore, all that needs to be done is to update the widget instances.
                 */
                AppWidgetManager awm = AppWidgetManager.getInstance(c);
                int[] wi = awm.getAppWidgetIds(new ComponentName(c,this.getClass().getName()));
                if (wi.length>0){
                    updateWidgetDisplay(c,awm,wi);
                }
            }
        } else {
            AppWidgetManager awm = AppWidgetManager.getInstance(c);
            int[] wi = awm.getAppWidgetIds(new ComponentName(c,this.getClass().getName()));
            fetchSensorData(c,awm,wi);
        }
    }

    @Override
    public void widgetUpdateAction(Context c, Intent i){
        AppWidgetManager awm = AppWidgetManager.getInstance(c);
        int[] wi = awm.getAppWidgetIds(new ComponentName(c,this.getClass().getName()));
        if (getDataFromIntent(i)){
            fetchSensorData(c,awm,wi);
        }
        if (hasWidgetPrerequisites(c)){
            if (last_known_location == null) {
                displayMissingLocationIcon(c, awm, wi);
            }
            askForNewLocation(c);
        }
    }

    @Override
    public void widgetNewLocationAction(final Context c, final Intent i) {
        if (i != null) {
            Bundle bundle = i.getExtras();
            if (bundle != null) {
                Location location = bundle.getParcelable(FeinstaubLocationManager.KEY_LOCATION_CHANGED);
                if (location != null) {
                    last_known_location = location;
                    loadNewDataFromAPI(c,i);
                }
            }
        }
    }

    /**
     * Restores the essential widget variables from an intent if the intent has them:
     * - a location
     * - locations (arraylist with close sensors)
     * - sensors (arraylist with sensor data)
     *
     * The intent may have location data only.
     *
     * @param i
     * @return true if the local variables were updated/resored, otherwise false. Returns
     * true if at least the location was accepted.
     *
     * Please note that locations & sensors may remain not updated as the intent may
     * have location data only.
     */

    public Boolean getDataFromIntent(Intent i){
        Bundle b = i.getExtras();
        Location new_location = null;
        ArrayList<SensorDataSet> new_sensorlist = null;
        ArrayList<LocationDataSet> new_locationlist = null;
        if (b != null){
            new_location     = b.getParcelable(FeinstaubLocationManager.KEY_LOCATION_CHANGED);
            new_locationlist = b.getParcelableArrayList(MainActivity.NEXTSENSORS_LOCATION);
            new_sensorlist   = b.getParcelableArrayList(MainActivity.NEXTSENSORS_DATA);
        }
        if (new_location != null){
            /*
             * There is at least a location in the intent extras.
             */
            if (last_known_location == null) {
                /*
                 * The widget does not have any location. We take it from the intent.
                 */
                last_known_location = new_location;
                if ((new_sensorlist != null) && (new_locationlist != null)){
                    /*
                     * There is a list of close sensors and their values. The widget
                     * takes them, too, if they correspond to the widget type.
                     */
                    if (new_sensorlist.size()>0){
                        if (isValidSensorType(new_sensorlist.get(0).getSensorType())){
                            sensors = new_sensorlist;
                            locations = new_locationlist;
                        } else {
                            /*
                             * the locations and sensor data is invalidated because it does
                             * not match to the new location any more.
                             */
                            sensors = null;
                            locations = null;
                        }
                    }
                }
                return true;
            } else {
                /*
                 * The widget has a location and the intent has a location. Therefore,
                 * the intent now checks which location is newer. The location from the
                 * intent is only taken if it is newer than the known one.
                 */
                if (new_location.getTime()>last_known_location.getTime()){
                    /*
                     * The new location from the intent is newer that the known one.
                     * => The widget accepts the location from the intent.
                     */
                    last_known_location = new_location;
                    /*
                     * Check if there is more data present.
                     */
                    if ((new_sensorlist != null) && (new_locationlist != null)){
                        /*
                         * There is a list of close sensors and their values. The widget
                         * takes them, too, if they correspond to the widget type.
                         */
                        if (new_sensorlist.size()>0){
                            if (isValidSensorType(new_sensorlist.get(0).getSensorType())){
                                sensors = new_sensorlist;
                                locations = new_locationlist;
                            } else {
                                /*
                                 * the locations and sensor data is invalidated because it does
                                 * not match to the new location any more.
                                 */
                                sensors = null;
                                locations = null;
                            }
                        }
                    }
                    return true;
                }
            }
        }
        return false;
    }

    /**
     *  Puts the essential variables from the widget into an intent:
     *  - last_known_location
     *  - locations (arraylist of close sensors)
     *  - sensors (arraylist with sensor data)
     *
     *  The location gets is assigned with the key from the LocationManager indicating that
     *  this intent has a new location.
     *
     *  The arraylists are only put into the intent when the location is known, otherwise
     *  there is no sense in storing this data.
     *
     * @param i Intent
     * @return
     */
    public Intent putDataIntoIntent(Intent i){
        if (i==null){
            i = new Intent();
        }
        if (last_known_location != null){
            Bundle b = new Bundle();
            b.putParcelable(FeinstaubLocationManager.KEY_LOCATION_CHANGED,last_known_location);
            b.putParcelableArrayList(MainActivity.NEXTSENSORS_LOCATION,locations);
            b.putParcelableArrayList(MainActivity.NEXTSENSORS_DATA,sensors);
            i.putExtras(b);
        }
        return i;
    }


    /**
     * Helper to get the latest SensorDataSet from an arraylist of FullSensorDataSets for the
     * specified sensorid.
     *
     * @param data
     * @param sensorid
     * @return
     */

    private SensorDataSet getNewestDataset(ArrayList<FullSensorDataSet> data, String sensorid){
        FullSensorDataSet result = null;
        for (int i=0; i<data.size(); i++){
            if (data.get(i).sensorId.equals(sensorid)){
                if (result==null){
                    result = data.get(i);
                } else {
                    if (result.getTimestampLocalMillis()<data.get(i).getTimestampLocalMillis()){
                        result = data.get(i);
                    }
                }
            }
        }
        if (result != null){
            return result.toSensorDataSet();
        } else {
            return null;
        }
    }

    private void loadNewDataFromAPI(final Context c, final Intent i) {
        if (last_known_location==null) {
            getLastKnownLocation(c);
        }
        if (last_known_location==null) {
            return;
        }
        SensorlocationHandler sensorlocationHandler = new SensorlocationHandler(c);

        raw_locations = sensorlocationHandler.getNextSensors(last_known_location, 20, getSensorTypesToDisplay());
        if (raw_locations != null) {
            if (raw_locations.size() > 0) {
                final AppWidgetManager awm = AppWidgetManager.getInstance(c);
                final int[] wi = awm.getAppWidgetIds(new ComponentName(c, this.getClass().getName()));
                final ArrayList<FullSensorDataSet> fullsensors = new ArrayList<>();
                LuftDatenAPIStreamReader streamReader = new LuftDatenAPIStreamReader();
                streamReader.setContext(c);
                streamReader.setUrls(getRawLocationsUrls());
                streamReader.setResultDataList(fullsensors);
                streamReader.setNumberOfValidSensorsToRead(WIDGET_SENSORNUMBERS);
                streamReader.isCalledFromWidget(true);
                streamReader.setSensortypes(getSensorTypesToDisplay());
                streamReader.setRunOnSuccess(new Runnable() {
                    @Override
                    public void run() {
                        sensors = new ArrayList<SensorDataSet>();
                        locations = new ArrayList<LocationDataSet>();
                        // add the sensor data to the local database
                        CardHandler cardHandler = new CardHandler(c);
                        for (int i = 0; i < fullsensors.size(); i++) {
                            cardHandler.addSensorDataSetIfNew(fullsensors.get(i));
                        }
                        /*
                         * Create the corresponding arraylists with locations & sensordata from the
                         * streamReader output. This is needed to get the latest values and to ignore
                         * the older values that are also in the "fullsensors" arraylist.
                         */
                        for (int i=0; i<raw_locations.size(); i++){
                            SensorDataSet sensorDataSet = getNewestDataset(fullsensors,raw_locations.get(i).number);
                            if (sensorDataSet!=null){
                                sensors.add(sensorDataSet);
                                locations.add(raw_locations.get(i));
                            }
                        }
                        /*
                         * Update all widget displays.
                         */
                        updateWidgetDisplay(c, awm, wi);
                    }
                });
                /*
                 * If an intent is known, this intent is used to call the location action again, as it failed.
                 */
                /*
                if (i != null){
                    streamReader.setRunOnError(new Runnable() {
                        @Override
                        public void run() {
                            Looper.prepare();
                            widgetNewLocationAction(c, i);
                            Handler h = new Handler();
                            h.removeCallbacks(this);
                            h.postDelayed(this, WIDGET_CONN_RETRY_DELAY);
                        }
                    });
                }
                */
                streamReader.run();
            }
        }
    }

    @Override
    public void fetchSensorData(final Context c, final AppWidgetManager awm, final int[] widget_instances) {
        readPreferences();
        /*
         * Checks if:
         * - permissions are present
         * - a database of known sensors exisis.
         * If not, a message is displayed and no further action is done.
         */
        if (!hasWidgetPrerequisites(c)) {
            for (int i = 0; i < widget_instances.length; i++) {
                RemoteViews rv = new RemoteViews(c.getPackageName(), R.layout.widget_layout_close);
                displayMissingPermissionsInfoIfNeeded(c, rv);
                awm.updateAppWidget(widget_instances[i], rv);
            }
            return;
        }
        /*
         * Retrieves the last known location.
         */
        getLastKnownLocation(c);
        /*
         * Checks if the widget has a list of known close sensors. If not, this list is generated from
         * the last known location.
         */
        if (locations == null){
            if (last_known_location != null){
                SensorlocationHandler sensorlocationHandler = new SensorlocationHandler(c);
                raw_locations = sensorlocationHandler.getNextSensors(last_known_location, 20, getSensorTypesToDisplay());
                getCloseDataSet(0);
            }
        }
        /*
         * Update the display with known data, if there is any.
         */
        if ((locations != null) && (sensors != null)){
            if ((locations.size()>0) && (sensors.size()>0)){
                updateWidgetDisplay(c,awm,widget_instances);
            }
        }
        /*
         * Display missing location info if no location is known
         */
        if (last_known_location == null){
            displayMissingLocationIcon(c,awm,widget_instances);
        }
        /*
         * Finally, register for new location data.
         */
        registerPendingIntentForLocationUpdate(c);
        /*
         * Then, read new data from the API for the known sensors.
         */
        loadNewDataFromAPI(c,null);
    }

    /**
     * This is a convenience method to retrieve the last known
     * location.
     *
     * @param c Context
     * @return last known location
     */


    private Location getLastKnownLocation(Context c){
        Location l = null;
        FeinstaubLocationManager feinstaubLocationManager = new FeinstaubLocationManager(c);
        l = feinstaubLocationManager.getLastKnownLocation(preferences);
        if (l != null) {
            last_known_location = l;
        }
        return last_known_location;
    }


    private URL[] getCloseSensorUrls(){
        URL[] UrlArray = new URL[sensors.size()];
        CardHandler ch = new CardHandler(context);
        for (int i=0; i<sensors.size(); i++){
            UrlArray[i] = ch.getSensorAPIURL(sensors.get(i).sensorId);
        }
        return UrlArray;
    }

    private URL[] getRawLocationsUrls(){
        URL[] UrlArray = new URL[raw_locations.size()];
        CardHandler ch = new CardHandler(context);
        for (int i=0; i<raw_locations.size(); i++){
            UrlArray[i] = ch.getSensorAPIURL(raw_locations.get(i).number);
        }
        return UrlArray;
    }


    private void getCloseDataSet(int location_count_index) {
        if (raw_locations == null)
            return;
        if (raw_locations.size()==0)
            return;
        LocationDataSet l = raw_locations.get(location_count_index);
        CardHandler ch = new CardHandler(context);
        SensorDataSet s = ch.getLatestDataSet(l.number);
        if ((sensors == null) || (locations == null)){
            sensors = new ArrayList<SensorDataSet>();
            locations = new ArrayList<LocationDataSet>();
        }
        if (s != null) {
            if ((!s.sensordataValue[0].equals("")) && (!s.sensordataValue[1].equals(""))) {
                sensors.add(s);
                locations.add(l);
                datafillcount = datafillcount + 1;
            }
        }
        if ((datafillcount < WIDGET_SENSORNUMBERS) && (location_count_index + 1 < raw_locations.size())) {
            getCloseDataSet(location_count_index + 1);
        }
    }


    public void askForNewLocation(Context c){
        FeinstaubLocationManager feinstaubLocationManager = new FeinstaubLocationManager(c);
        feinstaubLocationManager.requestSingleUpdate(getPendingIntentForLocationUpdate(c),preferences);
    }
}

