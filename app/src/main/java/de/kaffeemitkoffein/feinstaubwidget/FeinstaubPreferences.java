/*

 This file is part of FeinstaubWidget.

 Copyright (c) 2018, 2019, 2020, 2021, 2022, 2023, 2024 Pawel Dube

 FeinstaubWidget is free software: you can redistribute it and/or modify it
 under the terms of the GNU General Public License as published by the
 Free Software Foundation, either version 3 of the License, or (at
 your option) any later version.

 FeinstaubWidget is distributed in the hope that it will be useful, but
 WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with FeinstaubWidget. If not, see <http://www.gnu.org/licenses/>.

 */

package de.kaffeemitkoffein.feinstaubwidget;

import android.content.Context;
import android.content.SharedPreferences;
import android.location.LocationManager;
import android.preference.PreferenceManager;

/**
 * This class defines the preferences of this app.
 */

public class FeinstaubPreferences {

    /**
     * Preference names to share between activities.
     * PREF_SENSOR1_NUMBER              local sensor #1
     * PREF_SENSOR2_NUMBER              local sensor #2
     * PREF_DRAWLINES                   connect the values with lines: true=yes, false= no
     * PREF_THEME                       use dark or bright widget theme
     * PREF_OPACITY                     opacity of the widget
     * PREF_LOCATIONUPDATE              last time of update of the list of known sensors in milliseconds UTC time.
     *                                  A value of 0 indicates that there is no list of known sensors or that the
     *                                  present list is invalid and should not be used.
     * PREF_CARDSELECTORVALUE           The last known visible main view inside the app (mobile or local), used
     *                                  to return to the last visible view after the app gets closed/terminated.
     * PREF_LOCATION_UPDATE_INTERVAL    time in milliseconds between location updates from the device
     * PREF_LOCATION_UPDATE_DISTANCE    distance in metres; as long as the device returns locations within this
     *                                  radius from the last position where a data update was made, this is
     *                                  considered to be "the same" position.
     *                                  50m seems to be a reasonable default regarding the density of
     *                                  existing sensors.
     *                                  This value prevents too frequent queries from the API because of a location
     *                                  change.
     * PREF_DISPLAY_REFERENCE:          Determines if to display the reference in the views with data ("data (c)
     *                                  Luftdaten.Info and contributors": true=yes (default), false=no
     * PREF_USE_LEGACY_JSONPARSER       Use the older parser of this app to generate the list of sensors available. This
     *                                  parser needs more RAM and tends to crash more often; this should be used only
     *                                  as a fallback option when the new parser fails.
     * PREF_USE_LENIENT_JSONPARSER      Makes the json parser lenient regarding the json syntax.
     * PREF_AUTOGUESS_TEMPSENSOR        Determines if, based on the 1st sensor number, the temperature sensor should
     *                                  be determined automatically in the local setup-activity.
     * PREF_DO_NOT_SHOW_SHAREHINT_AGAIN if the share hint should be ever shown again.
     */

    /**
     * Dataswitch position constants
     */

    public final static Boolean DATASWITCHPOSITION_PARTICULATEMATTER = false;
    public final static Boolean DATASWITCHPOSITION_TEMPERATURE = true;

    /**
     * Preference constants
     */

    static final String PREF_SENSOR1_NUMBER = "PREF_SENSOR1_NUMBER";
    static final String PREF_SENSOR2_NUMBER = "PREF_SENSOR2_NUMBER";
    static final String PREF_DRAWLINES = "PREF_DRAWLINES";
    static final String PREF_TWOLINES = "PREF_TWOLINES";
    static final String PREF_THEME = "PREF_THEME";
    static final String PREF_OPACITY = "PREF_OPACITY";
    static final String PREF_LOCATIONUPDATE = "PREF_LOCATIONUPDATE";
    static final String PREF_CARDSELECTORVALUE = "PREF_CARDSELECTORVALUE";
    static final String PREF_DATASWITCHPOSITION = "PREF_DATASWITCHPOSITION";
    static final String PREF_LOCATION_UPDATE_INTERVAL = "PREF_LOCATION_UPDATE_INTERVAL";
    static final String PREF_LOCATION_UPDATE_DISTANCE = "PREF_LOCATION_UPDATE_DISTANCE";
    static final String PREF_DISPLAY_REFERENCE = "PREF_DISPLAY_REFERENCE";
    static final String PREF_USE_LENIENT_JSONPARSER = "PREF_USE_LENIENT_JSONPARSER";
    static final String PREF_AUTOGUESS_TEMPSENSOR = "PREF_AUTOGUESS_TEMPSENSOR";
    static final String PREF_LARGE_CIRCLES = "PREF_LARGE_CIRCLES";
    static final String PREF_DO_NOT_SHOW_SHAREHINT_AGAIN = "PREF_DO_NOT_SHOW_SHAREHINT_AGAIN";
    static final String PREF_FILENAME = "PREF_FILENAME";
    static final String PREF_LOCATION_ENERGYSAVEMODE = "PREF_LOCATION_ENERGYSAVEMODE";
    static final String PREF_USE_LOCAL_SENSOR_LOCATION_AS_FALLBACK = "PREF_USE_LOCAL_SENSOR_LOCATION_AS_FALLBACK";
    static final String PREF_USE_LOCATION_PROVIDER_GPS = "PREF_USE_LOCATION_PROVIDER_GPS";
    static final String PREF_USE_LOCATION_PROVIDER_NETWORK = "PREF_USE_LOCATION_PROVIDER_NETWORK";
    static final String PREF_USE_LOCATION_PROVIDER_PASSIVE = "PREF_USE_LOCATION_PROVIDER_PASSIVE";

    /**
     * Default values for the preferences.
     */

    static final String PREF_SENSOR1_NUMBER_DEFAULT = "";
    static final String PREF_SENSOR2_NUMBER_DEFAULT = "";
    static final Boolean PREF_DRAWLINES_DEFAULT = false;
    static final Boolean PREF_TWOLINES_DEFAULT = false;
    static final int PREF_THEME_DEFAULT = DisplayManager.WIDGET_COLOR_THEME_LIGHT;
    static final int PREF_OPACITY_DEFAULT = DisplayManager.WIDGET_DEFAULT_OPACITY;
    static final Long PREF_LOCATIONUPDATE_DEFAULT = new Long(0);
    static final int PREF_CARDSELECTORVALUE_DEFAULT = MainActivity.CARDSELECTOR_MOBILE;
    static final Boolean PREF_DATASWITCHPOSITION_DEFAULT = DATASWITCHPOSITION_PARTICULATEMATTER;
    static final int PREF_LOCATION_UPDATE_INTERVAL_DEFAULT = 30; // 30 minutes update time
    static final int PREF_LOCATION_UPDATE_DISTANCE_DEFAULT = 1000;
    static final Boolean PREF_DISPLAY_REFERENCE_DEFAULT = true;
    static final Boolean PREF_USE_LENIENT_JSONPARSER_DEFAULT = false;
    static final Boolean PREF_AUTOGUESS_TEMPSENSOR_DEFAULT = true;
    static final Boolean PREF_LARGE_CIRCLES_DEFAULT = false;
    static final Boolean PREF_DO_NOT_SHOW_SHAREHINT_AGAIN_DEFAULT = false;
    static final String PREF_FILENAME_DEFAULT = "pollution";
    static final Boolean PREF_LOCATION_ENERGYSAVEMODE_DEFAULT = true;
    static final Boolean PREF_USE_LOCAL_SENSOR_LOCATION_AS_FALLBACK_DEFAULT = false;
    static final Boolean PREF_USE_LOCATION_PROVIDER_GPS_DEFAULT = true;
    static final Boolean PREF_USE_LOCATION_PROVIDER_NETWORK_DEFAULT = true;
    static final Boolean PREF_USE_LOCATION_PROVIDER_PASSIVE_DEFAULT = true;
    /**
     * Local variables holding the current settings.
     */

    String sensor1_number = PREF_SENSOR1_NUMBER_DEFAULT;
    String sensor2_number = PREF_SENSOR2_NUMBER_DEFAULT;
    Boolean drawlines = PREF_DRAWLINES_DEFAULT;
    Boolean display_only_two_lines = PREF_TWOLINES_DEFAULT;
    int widget_theme = PREF_THEME_DEFAULT;
    int widget_opacity = PREF_OPACITY_DEFAULT;
    Long lastlocationlistupdatetime = PREF_LOCATIONUPDATE_DEFAULT;
    int visible_cardselector = PREF_CARDSELECTORVALUE_DEFAULT;
    Boolean dataswitchposition = PREF_DATASWITCHPOSITION_DEFAULT;
    int location_update_timespan = PREF_LOCATION_UPDATE_INTERVAL_DEFAULT;
    int location_update_distance = PREF_LOCATION_UPDATE_DISTANCE_DEFAULT;
    Boolean display_reference = PREF_DISPLAY_REFERENCE_DEFAULT;
    Boolean use_lenient_jsonparser = PREF_USE_LENIENT_JSONPARSER_DEFAULT;
    Boolean autoguess_tempsensor = PREF_AUTOGUESS_TEMPSENSOR_DEFAULT;
    Boolean large_circles = PREF_LARGE_CIRCLES_DEFAULT;
    Boolean do_not_show_sharehint_again = PREF_DO_NOT_SHOW_SHAREHINT_AGAIN_DEFAULT;
    String filename = PREF_FILENAME_DEFAULT;
    Boolean energysavemode = PREF_LOCATION_ENERGYSAVEMODE_DEFAULT;
    Boolean use_local_sensor_location_as_fallback = PREF_USE_LOCAL_SENSOR_LOCATION_AS_FALLBACK_DEFAULT;
    Boolean use_location_provider_GPS = PREF_USE_LOCATION_PROVIDER_GPS_DEFAULT;
    Boolean use_location_provider_NETWORK = PREF_USE_LOCATION_PROVIDER_NETWORK_DEFAULT;
    Boolean use_location_provider_PASSIVE = PREF_USE_LOCATION_PROVIDER_PASSIVE_DEFAULT;


    private Context context;
    SharedPreferences sharedPreferences;

    public FeinstaubPreferences(Context context){
        this.context = context;
        sharedPreferences = PreferenceManager.getDefaultSharedPreferences(this.context);
        readPreferences();
    }

    public void readPreferences() {
        sensor1_number             = readPreference(PREF_SENSOR1_NUMBER,PREF_SENSOR1_NUMBER_DEFAULT);
        sensor2_number             = readPreference(PREF_SENSOR2_NUMBER,PREF_SENSOR2_NUMBER_DEFAULT);
        drawlines                  = readPreference(PREF_DRAWLINES,PREF_DRAWLINES_DEFAULT);
        display_only_two_lines     = readPreference(PREF_TWOLINES,PREF_TWOLINES_DEFAULT);
        widget_theme               = readPreference(PREF_THEME,PREF_THEME_DEFAULT);
        widget_opacity             = readPreference(PREF_OPACITY,PREF_OPACITY_DEFAULT);
        lastlocationlistupdatetime = readPreference(PREF_LOCATIONUPDATE,PREF_LOCATIONUPDATE_DEFAULT);
        visible_cardselector       = readPreference(PREF_CARDSELECTORVALUE,PREF_CARDSELECTORVALUE_DEFAULT);
        dataswitchposition         = readPreference(PREF_DATASWITCHPOSITION,PREF_DATASWITCHPOSITION_DEFAULT);
        try {
            location_update_timespan   = Integer.valueOf(readPreference(PREF_LOCATION_UPDATE_INTERVAL,String.valueOf(PREF_LOCATION_UPDATE_INTERVAL_DEFAULT)));
        } catch (NumberFormatException e){
            location_update_timespan = PREF_LOCATION_UPDATE_INTERVAL_DEFAULT;
        }
        try {
            location_update_distance   = Integer.valueOf(readPreference(PREF_LOCATION_UPDATE_DISTANCE,String.valueOf(PREF_LOCATION_UPDATE_DISTANCE_DEFAULT)));
        } catch (NumberFormatException e){
            location_update_distance = PREF_LOCATION_UPDATE_DISTANCE_DEFAULT;
        }
        display_reference                     = readPreference(PREF_DISPLAY_REFERENCE,PREF_DISPLAY_REFERENCE_DEFAULT);
        use_lenient_jsonparser                = readPreference(PREF_USE_LENIENT_JSONPARSER,PREF_USE_LENIENT_JSONPARSER_DEFAULT);
        autoguess_tempsensor                  = readPreference(PREF_AUTOGUESS_TEMPSENSOR,PREF_AUTOGUESS_TEMPSENSOR_DEFAULT);
        large_circles                         = readPreference(PREF_LARGE_CIRCLES,PREF_LARGE_CIRCLES_DEFAULT);
        do_not_show_sharehint_again           = readPreference(PREF_DO_NOT_SHOW_SHAREHINT_AGAIN,PREF_DO_NOT_SHOW_SHAREHINT_AGAIN_DEFAULT);
        filename                              = readPreference(PREF_FILENAME,PREF_FILENAME_DEFAULT);
        energysavemode                        = readPreference(PREF_LOCATION_ENERGYSAVEMODE,PREF_LOCATION_ENERGYSAVEMODE_DEFAULT);
        use_local_sensor_location_as_fallback = readPreference(PREF_USE_LOCAL_SENSOR_LOCATION_AS_FALLBACK,PREF_USE_LOCAL_SENSOR_LOCATION_AS_FALLBACK_DEFAULT);
        use_location_provider_GPS             = readPreference(PREF_USE_LOCATION_PROVIDER_GPS,PREF_USE_LOCATION_PROVIDER_GPS_DEFAULT);
        use_location_provider_NETWORK         = readPreference(PREF_USE_LOCATION_PROVIDER_NETWORK,PREF_USE_LOCATION_PROVIDER_NETWORK_DEFAULT);
        use_location_provider_GPS             = readPreference(PREF_USE_LOCATION_PROVIDER_PASSIVE,PREF_USE_LOCATION_PROVIDER_PASSIVE_DEFAULT);
    }

    public String readPreference(String preference, String defaultvalue) {
        return sharedPreferences.getString(preference, defaultvalue);
    }

    public Boolean readPreference(String preference, Boolean defaultvalue) {
        return sharedPreferences.getBoolean(preference, defaultvalue);
    }

    public int readPreference(String preference, int defaultvalue) {
        return sharedPreferences.getInt(preference, defaultvalue);
    }

    public Long readPreference(String preference, Long defaultvalue) {
        return sharedPreferences.getLong(preference, defaultvalue);
    }

    public void commitPreference(String preference, String value) {
        SharedPreferences.Editor pref_editor = sharedPreferences.edit();
        pref_editor.putString(preference, value);
        pref_editor.commit();
    }

    public void commitPreference(String preference, int value) {
        SharedPreferences.Editor pref_editor = sharedPreferences.edit();
        pref_editor.putInt(preference, value);
        pref_editor.commit();
    }

    public void commitPreference(String preference, Boolean value) {
        SharedPreferences.Editor pref_editor = sharedPreferences.edit();
        pref_editor.putBoolean(preference, value);
        pref_editor.commit();
    }

    public void commitPreference(String preference, Long value) {
        SharedPreferences.Editor pref_editor = sharedPreferences.edit();
        pref_editor.putLong(preference, value);
        pref_editor.commit();
    }

    public void commitAllPreferences() {
        commitPreference(PREF_SENSOR1_NUMBER, sensor1_number);
        commitPreference(PREF_SENSOR2_NUMBER, sensor2_number);
        commitPreference(PREF_DRAWLINES, drawlines);
        commitPreference(PREF_TWOLINES, display_only_two_lines);
        commitPreference(PREF_THEME, widget_theme);
        commitPreference(PREF_OPACITY, widget_opacity);
        commitPreference(PREF_LOCATIONUPDATE, lastlocationlistupdatetime);
        commitPreference(PREF_CARDSELECTORVALUE, visible_cardselector);
        commitPreference(PREF_DATASWITCHPOSITION,dataswitchposition);
        commitPreference(PREF_LOCATION_UPDATE_INTERVAL, location_update_timespan);
        commitPreference(PREF_LOCATION_UPDATE_DISTANCE, location_update_distance);
        commitPreference(PREF_DISPLAY_REFERENCE, display_reference);
        commitPreference(PREF_USE_LENIENT_JSONPARSER, use_lenient_jsonparser);
        commitPreference(PREF_AUTOGUESS_TEMPSENSOR,autoguess_tempsensor);
        commitPreference(PREF_LARGE_CIRCLES,large_circles);
        commitPreference(PREF_DO_NOT_SHOW_SHAREHINT_AGAIN,do_not_show_sharehint_again);
        commitPreference(PREF_FILENAME,filename);
        commitPreference(PREF_LOCATION_ENERGYSAVEMODE,energysavemode);
        commitPreference(PREF_USE_LOCAL_SENSOR_LOCATION_AS_FALLBACK,use_local_sensor_location_as_fallback);
        commitPreference(PREF_USE_LOCATION_PROVIDER_GPS,use_location_provider_GPS);
        commitPreference(PREF_USE_LOCATION_PROVIDER_NETWORK,use_location_provider_NETWORK);
        commitPreference(PREF_USE_LOCATION_PROVIDER_PASSIVE,use_location_provider_PASSIVE);
    }

    public void applyPreference(String preference, String value) {
        SharedPreferences.Editor pref_editor = sharedPreferences.edit();
        pref_editor.putString(preference, value);
        pref_editor.apply();
    }

    public void applyPreference(String preference, int value) {
        SharedPreferences.Editor pref_editor = sharedPreferences.edit();
        pref_editor.putInt(preference, value);
        pref_editor.apply();
    }

    public void applyPreference(String preference, Boolean value) {
        SharedPreferences.Editor pref_editor = sharedPreferences.edit();
        pref_editor.putBoolean(preference, value);
        pref_editor.apply();
    }

    public void applyPreference(String preference, Long value) {
        SharedPreferences.Editor pref_editor = sharedPreferences.edit();
        pref_editor.putLong(preference, value);
        pref_editor.apply();
    }

    public void applyAllPreferences() {
        applyPreference(PREF_SENSOR1_NUMBER, sensor1_number);
        applyPreference(PREF_SENSOR2_NUMBER, sensor2_number);
        applyPreference(PREF_DRAWLINES, drawlines);
        applyPreference(PREF_TWOLINES, display_only_two_lines);
        applyPreference(PREF_THEME, widget_theme);
        applyPreference(PREF_OPACITY, widget_opacity);
        applyPreference(PREF_LOCATIONUPDATE, lastlocationlistupdatetime);
        applyPreference(PREF_CARDSELECTORVALUE, visible_cardselector);
        applyPreference(PREF_DATASWITCHPOSITION,dataswitchposition);
        applyPreference(PREF_LOCATION_UPDATE_INTERVAL, location_update_timespan);
        applyPreference(PREF_LOCATION_UPDATE_DISTANCE, location_update_distance);
        applyPreference(PREF_DISPLAY_REFERENCE, display_reference);
        applyPreference(PREF_USE_LENIENT_JSONPARSER, use_lenient_jsonparser);
        applyPreference(PREF_AUTOGUESS_TEMPSENSOR,autoguess_tempsensor);
        applyPreference(PREF_LARGE_CIRCLES,large_circles);
        applyPreference(PREF_DO_NOT_SHOW_SHAREHINT_AGAIN,do_not_show_sharehint_again);
        applyPreference(PREF_FILENAME,filename);
        applyPreference(PREF_LOCATION_ENERGYSAVEMODE,energysavemode);
        applyPreference(PREF_USE_LOCAL_SENSOR_LOCATION_AS_FALLBACK,use_local_sensor_location_as_fallback);
        applyPreference(PREF_USE_LOCATION_PROVIDER_GPS,use_location_provider_GPS);
        applyPreference(PREF_USE_LOCATION_PROVIDER_NETWORK,use_location_provider_NETWORK);
        applyPreference(PREF_USE_LOCATION_PROVIDER_PASSIVE,use_location_provider_PASSIVE);
    }

    public long getLocationUpdateIntervalInMillis(){
        return location_update_timespan * 60 * 1000;
    }

    public String getLocationProviderPreference(){
        if (energysavemode){
            return LocationManager.PASSIVE_PROVIDER;
        } else {
            return LocationManager.GPS_PROVIDER;
        }
    }

    public static String getPrefFilename(Context context){
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
        return sharedPreferences.getString(PREF_FILENAME,PREF_FILENAME_DEFAULT);
    }
}
