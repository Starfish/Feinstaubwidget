/**

 This file is part of FeinstaubWidget.

 Copyright (c) 2018, 2019, 2020, 2021, 2022, 2023, 2024 Pawel Dube

 FeinstaubWidget is free software: you can redistribute it and/or modify it
 under the terms of the GNU General Public License as published by the
 Free Software Foundation, either version 3 of the License, or (at
 your option) any later version.

 FeinstaubWidget is distributed in the hope that it will be useful, but
 WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with FeinstaubWidget. If not, see <http://www.gnu.org/licenses/>.

 */

package de.kaffeemitkoffein.feinstaubwidget;

import android.Manifest;
import android.annotation.SuppressLint;
import android.content.Context;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.location.LocationProvider;
import android.os.Build;
import android.os.Bundle;

public class FeinstaubLocationListener implements LocationListener {

    Location localLastKnownLocation = null;
    Context context;

    public FeinstaubLocationListener(Context c) {
        this.context = c;
    }

    public void setLocationVariable(Location l) {
        this.localLastKnownLocation = l;
    }

    public void setLocationContext(Context c) {
        this.context = c;
    }

    @Override
    public void onLocationChanged(Location location) {
        localLastKnownLocation = location;
    }

    @Override
    @SuppressLint("MissingPermission")
    public void onStatusChanged(String s, int status, Bundle bundle) {
        // override only on api below 29, never called above
        if (android.os.Build.VERSION.SDK_INT <  android.os.Build.VERSION_CODES.Q) {
            if (FeinstaubLocationManager.hasLocationPermission(context)){
                LocationManager lm = (LocationManager) context.getSystemService(Context.LOCATION_SERVICE);
                Location l = lm.getLastKnownLocation(LocationManager.GPS_PROVIDER);
                if (l!=null){
                    onLocationChanged(l);
                }
            }
        }
    }

    @Override
    @SuppressLint("MissingPermission")
    public void onProviderEnabled(String s) {
        if (FeinstaubLocationManager.hasLocationPermission(context)){
            LocationManager lm = (LocationManager) context.getSystemService(Context.LOCATION_SERVICE);
            Location l = lm.getLastKnownLocation(LocationManager.GPS_PROVIDER);
            if (l!=null){
                localLastKnownLocation = l;
                onLocationChanged(l);
            }
        }
    }

    @Override
    public void onProviderDisabled(String s) {

    }

}
