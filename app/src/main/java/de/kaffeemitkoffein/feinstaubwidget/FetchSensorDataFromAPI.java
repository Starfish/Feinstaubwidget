/*

 This file is part of FeinstaubWidget.

 Copyright (c) 2018, 2019, 2020, 2021, 2022, 2023, 2024 Pawel Dube

 FeinstaubWidget is free software: you can redistribute it and/or modify it
 under the terms of the GNU General Public License as published by the
 Free Software Foundation, either version 3 of the License, or (at
 your option) any later version.

 FeinstaubWidget is distributed in the hope that it will be useful, but
 WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with FeinstaubWidget. If not, see <http://www.gnu.org/licenses/>.

 */

package de.kaffeemitkoffein.feinstaubwidget;

import android.content.Context;
import android.os.AsyncTask;
import java.net.URL;
import java.util.ArrayList;

/**
 * This class fetches the current data from the Luftdaten.info API.
 *
 * Use:
 * 1) Obtain an instance: FetchSendorDataFromAPI fsd = new FetchSensorDataFromAPI(context);
 * 2) Launch the task: fsd.execute(Urls...);
 *
 * This class is capable of reading multiple Urls (sensors) at once. Any data successfully obtained is written to the app database.
 *
 * To get the Urls by sensor number, use CardHandler.getSensorAPIURL(Sensornumber).
 *
 * Please see below how to override certain routines to customize the behaviour.
 */


public class FetchSensorDataFromAPI implements Runnable {

    // extends AsyncTask<URL,Void,ArrayList<FullSensorDataSet>>

    public Context context;
    private URL[] urls;

    public FetchSensorDataFromAPI(Context c, URL[] urls){
        this.context = c;
        this.urls = urls;
    }


    private ArrayList<FullSensorDataSet> doInBackground(URL[] urls) {
        final ArrayList<FullSensorDataSet> content = new ArrayList<FullSensorDataSet>();
        LuftDatenAPIStreamReader streamReader = new LuftDatenAPIStreamReader();
        streamReader.setUrls(urls);
        streamReader.setContext(context);
        streamReader.setResultDataList(content);
        streamReader.run();
        return content;
    }

    /**
     * Override this routine to define what to do if obtaining data failed.
     */

    public void onNegativeResult(){
        // do nothing at the moment.
    }

    /**
     * Override this routine to define what to do if obtaining data succeeded.
     *
     * Remember: at this point, the new data is already written to the database and can be
     * accessed via the CardHandler class.
     */

    public void onPositiveResult(){
        // do nothing at the moment.
    }

    public void onPositiveResult(ArrayList<Integer> sensortypes){
        onPositiveResult();
        // do nothing at the moment.
    }

    protected void onPostExecute(ArrayList<FullSensorDataSet> content) {
        if (content == null) {
           onNegativeResult();
        } else {
            ArrayList<Integer> sensortypes = new ArrayList<>();
            CardHandler ch = new CardHandler(context);
            for (int i=0; i<content.size(); i++){
                ch.addSensorDataSetIfNew(content.get(i));
                sensortypes.add(content.get(i).getSensorType());
            }
            onPositiveResult(sensortypes);
        }
    }

    @Override
    public void run() {
        onPostExecute(doInBackground(urls));
    }
}
